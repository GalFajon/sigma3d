/**
 * Adapted from: Bjorn Sandvik / http://thematicmapping.org/
 */

import * as THREE from '../three.module.js';

export class WcsTerrainLoader {
	constructor() {
		this.manager = ( this.manager !== undefined ) ? this.manager : THREE.DefaultLoadingManager;
	}

	setCrossOrigin(value) {
		this.crossOrigin = value;
	}

	load(url, onLoad, onProgress, onError) {
		var scope = this;
		var request = new XMLHttpRequest();

		if ( onLoad !== undefined ) {
			request.addEventListener( 'load', function ( event ) {
				onLoad( event.target.response.split( "\n" ).map( function( value ) {
					return parseFloat( value.split( ' ' )[2]);
				} ) );

				scope.manager.itemEnd( url );
			}, false );
		}

		if ( onProgress !== undefined ) {
			request.addEventListener( 'progress', function ( event ) {
				onProgress( event );
			}, false );
		}

		if ( onError !== undefined ) {
			request.addEventListener( 'error', function ( event ) {
				onError( event );
			}, false );
		}

		if ( this.crossOrigin !== undefined ) request.crossOrigin = this.crossOrigin;

		request.open( 'GET', url, true );
		request.send( null );

		scope.manager.itemStart( url );
	}
}