# Sigma3D #
A library that imitates OpenLayers in 3D space. Allows for the display of pointclouds, .obj models, measurements and more...

- Author: Gal Fajon
- Version: 0.9 
- Copyright: 2022 

## How to build: ##
Use the following command in the project folder: `npm run initialize`

## Are there any examples? ##
Check the `./examples/tests/` directory.

## How to generate documentation (in case changes were made): ##
- Use the following command in the project folder: `npm run docs`
- Enter the `./docs` directory and open index.html.
