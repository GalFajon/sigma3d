/**
 * A cache for WMS images.
 * 
 * @param {int} limit - The amount of images a single zoom level can hold.
 * @param {array} zoomLevels - An array of zoomLevels [1,2,3,4...].
 * 
 * @property {Object} Images - An object that contains a map for each provided zoom level. The keys of the map are the images of the urls.
 * 
 * @category Terrain
 * @class
 */
export class WMSImageCache {
    constructor(limit, zoomlevels) {
        this.Images = {}

        for (const zoomLevel in zoomlevels) {
            this.Images[zoomLevel] = new Map();
        }

        this.Limit = limit; // limit of images
    }

    /**
   * Adds an image to the cache.
   * @param {int} zoomLevel - The zoomlevel of the image.
   * @param {string} url - The url of the image.
   * @param {Image} imageObject - The Image() object to add.
   * @private
   * @function
   */
    add(zoomLevel, url, imageObject) {
        if (this.Images[zoomLevel].size >= this.Limit) {
            const [firstKey] = this.Images[zoomLevel].keys();
            this.Images[zoomLevel].delete(firstKey);
        }

        this.Images[zoomLevel].set(url, imageObject);
    }

    /**
   * Checks if the cache has an image.
   * @param {int} zoomLevel - The zoomlevel of the image.
   * @param {string} url - The url of the image.
   * 
   * @returns {boolean} hasImage - True if the cache contains the requested image.
   * 
   * @private
   * @function
   */
    has(zoomLevel,url) {
        if (this.Images[zoomLevel] !== undefined) {
            return this.Images[zoomLevel].has(url);
        }
        else return false;
    }

    /**
   * Gets an image from the cache.
   * @param {int} zoomLevel - The zoomlevel of the image.
   * @param {string} url - The url of the image.
   * 
   * @returns {Image} image - The requested image.
   * 
   * @private
   * @function
   */
    get(zoomLevel,url) {
        return this.Images[zoomLevel].get(url);
    }

    /**
   * Removes an image from the cache.
   * @param {int} zoomLevel - The zoomlevel of the image.
   * @param {string} url - The url of the image.
   * 
   * @private
   * @function
   */
    remove(zoomLevel,url) {
        this.Images[zoomLevel].delete(url);
    }
}