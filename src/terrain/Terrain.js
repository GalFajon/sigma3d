import { Tile } from "./Tile.js";
import * as THREE from '../../libs/threejs/three.module.js';
import { Utilities } from "../utilities/Utilities.js";

/**
 * An object used by TerrainSource to display terrain. Handles dividing the images provided to the source into tiles.
 * 
 * @param {Object} config - Configures the source.
 * @param {string} config.tiff - The url of the GeoTIFF file containing the heights of the terrain.
 * @param {string} config.texture - The url of the texture to draw over the terrain.
 * @param {int} config.tileWidth - The width of the tiles the terrain will be divided into (in pixels).
 * @param {int} config.tileHeight - The height of the tiles the terrain will be divided into (in pixels).
 * @param {Array} config.heights - The heights of the GeoTIFF file, contained in an array. ([x][y])
 * @param {Array} config.boundingBox - The bounding box of the provided GeoTIFF.
 * 
 * @property {string} Tiff - The url of the GeoTIFF file containing the heights of the terrain.
 * @property {string} Texture - The url of the texture to draw over the terrain.
 * @property {int} TileWidth - The width of the tiles the terrain will be divided into (in pixels).
 * @property {int} TileHeight - The height of the tiles the terrain will be divided into (in pixels).
 * @property {Array} Heights - The heights of the GeoTIFF file, contained in an array. ([x][y])
 * @property {Array} BoundingBox - The bounding box of the provided GeoTIFF.
 * @property {Array} Tiles - An array of Tile objects used by the terrain.
 * @property {THREE.Object3D} model - A THREE node that groups together the tiles of the terrain.
 * 
 * @category Terrain
 * @class
 */
export class Terrain {
    constructor(config) {
        if (config) {
            if (config.tiff) this.Tiff = config.tiff;
            if (config.texture) this.Texture = config.texture;
            if (config.tileWidth) this.TileWidth = config.tileWidth;
            if (config.tileHeight) this.TileHeight = config.tileHeight;
            
            if (config.heights) this.Heights = config.heights; // heights[x][y]
            if (config.boundingBox) this.BoundingBox = config.boundingBox;

            this.model = new THREE.Object3D();
        }
    }

    /**
     * Initializes the terrain and generates the terrain tiles.
     * @private
     * @function
     */
    async initialize() {
        this.Tiles = await this.generateTiles();
        
        for (let tile of this.Tiles) {
            this.model.add(tile.model);
        }
    }

    /**
     * Generates the tiles of the terrain.
     * 
     * @returns {Array} tiles - The tiles of the terrain.
     * 
     * @private
     * @function
     */
    async generateTiles() {
        let tiles = [];

        for (let x=0; (x+this.TileWidth) < this.Heights.length; x+=this.TileWidth) {
            for (let y=0; (y+this.TileHeight) < this.Heights[0].length; y+=this.TileHeight) {   
                let width = this.TileWidth;
                let height = this.TileHeight;
                
                if ((x+this.TileWidth + 1) < this.Heights.length - 1) width += 1;
                if ((y+this.TileHeight + 1) < this.Heights[0].length - 1) height += 1;

                let texture = await this.generateTexture(x,(x+width),(y),(y+height));

                tiles.push( new Tile(
                {
                    detail: 0.1,
                    heights: this.getHeightsInArea(x, x+width, y, y+height),
                    texture: texture,
                    position: [this.BoundingBox[0] + x, this.BoundingBox[1] + (height / 2) + y]    
                }));
            }
        }

        return tiles;
    }

    /**
     * Crops the texture of the terrain based on the provided boundaries.
     * 
     * @param {int} leftx - The left x coordinate of the tile.
     * @param {int} rightx - The right x coordinate of the tile.
     * @param {int} topy - The top y coordinate of the tile.
     * @param {int} bottomy - The bottom y coordinate of the tile.
     * 
     * @private
     * @function
     */
    async generateTexture(leftx,rightx,topy,bottomy) {
        return new Promise((resolve,reject) => {
            let canvas = document.createElement('canvas');
            let ctx = canvas.getContext('2d');

            ctx.canvas.width = rightx-leftx;
            ctx.canvas.height = bottomy-topy;
            
            ctx.fillStyle = Utilities.PotreeViewer.background;
            ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            
            let img = new Image();

            if (this.Texture) {
                img.crossOrigin = "anonymous";
                img.src = this.Texture;

                img.onload = () => { 
                    ctx.drawImage(img,-leftx,-topy, img.width + 1,img.height);
                    resolve(new THREE.CanvasTexture(canvas));
                }
            }
            else resolve(new THREE.CanvasTexture(canvas));
        })
    }

     /**
     * Gets the heights of a square tile defined by the provided parameters.
     * 
     * @param {int} leftx - The left x coordinate of the tile.
     * @param {int} rightx - The right x coordinate of the tile.
     * @param {int} topy - The top y coordinate of the tile.
     * @param {int} bottomy - The bottom y coordinate of the tile.
     * 
     * @private
     * @function
     */
    getHeightsInArea(leftx,rightx,topy,bottomy) {
        let lastDefinedHeight = 0;
        let heights = [];

        for (let x=0; x <= (rightx-leftx); x++) {
            heights[x] = [];

            for (let y=0; y <= (bottomy-topy); y++) {
                heights[x][y] = 0;
            }
        }

        let relativeX = 0;
        let relativeY = 0;

        for (let x=leftx; x <= rightx; x++) {
            relativeY = 0;

            for (let y=topy; y <= bottomy; y++) {
                if (this.Heights[x] !== undefined && this.Heights[x][y] !== undefined) {
                    lastDefinedHeight = this.Heights[x][y];
                    heights[relativeX][relativeY] = this.Heights[x][y];
                }
                else
                {
                    heights[relativeX][relativeY] = lastDefinedHeight;
                }

                relativeY += 1;
            }

            relativeX += 1;
        }

        return heights;
    }

    /**
     * Removes the terrain from the scene.
     * 
     * @private
     * @function
     */
    remove() {
        for (let tile of this.Tiles) {
            this.model.remove(tile.model);
            tile.model.geometry.dispose();
            tile.model.material.dispose();
        }
    }

    /**
     * Updates the detail level of the terrains tiles based on their distance from the camera.
     * @param {THREE.Vector3} cameraPosition - A Vector3 containing the position of the camera.
     * 
     * @private
     * @function
     */
    updateTileDetailAccordingToCameraDistance(cameraPosition) {
        for (let tile of this.Tiles) {
            let distance = tile.model.position.distanceTo(cameraPosition);
            let detail = 0.1;

            if (distance <= 30) detail = 1;
            if (distance > 50 && distance <= 300) detail = 0.2;
            if (distance > 300) detail = 0.1;

            tile.updateGeometry(detail);
        }
    }

    /**
     * Offsets the tiles to be within the terrain bounding box.
     * @private
     * @function
     */
    updateTilePositions() {
        let tileIndex = 0;

        if (this.Tiles) {
            for (let x=0; (x+this.TileWidth) < this.Heights.length; x+=this.TileWidth) {
                for (let y=0; (y+this.TileHeight) < this.Heights[0].length; y+=this.TileHeight) {   
                    let width = this.TileWidth;
                    let height = this.TileHeight;
                    
                    if ((x+this.TileWidth + 1) < this.Heights.length - 1) width += 1;
                    if ((y+this.TileHeight + 1) < this.Heights[0].length - 1) height += 1;

                    if (this.Tiles[tileIndex]) {
                        this.Tiles[tileIndex].model.position.set(this.BoundingBox[0] + x, this.BoundingBox[1] + y,0);
                    }

                    tileIndex += 1;
                }
            }
        }
    }

    /**
     * Updates the heights of the terrains tiles.
     * @private
     * @function
     */
    updateHeights() {
        if (this.Tiles) {
            let tileIndex = 0;

            for (let x=0; (x+this.TileWidth) < this.Heights.length; x+=this.TileWidth) {
                for (let y=0; (y+this.TileHeight) < this.Heights[0].length; y+=this.TileHeight) {   
                    let width = this.TileWidth;
                    let height = this.TileHeight;
                    
                    if ((x+this.TileWidth + 1) < this.Heights.length - 1) width += 1;
                    if ((y+this.TileHeight + 1) < this.Heights[0].length - 1) height += 1;
                    
                    if (this.Tiles[tileIndex]) {
                        this.Tiles[tileIndex].Heights = this.getHeightsInArea(x, x+width, y, y+height);
                    }

                    tileIndex += 1;
                }
            }

        }
    }

    
    /**
     * Generates textures for each tile.
     * @private
     * @function
     */
    async updateTexture() {
        if (this.Tiles) {
            let tileIndex = 0;

            for (let x=0; (x+this.TileWidth) < this.Heights.length; x+=this.TileWidth) {
                for (let y=0; (y+this.TileHeight) < this.Heights[0].length; y+=this.TileHeight) {   
                    let width = this.TileWidth;
                    let height = this.TileHeight;
                    
                    if ((x+this.TileWidth + 1) < this.Heights.length - 1) width += 1;
                    if ((y+this.TileHeight + 1) < this.Heights[0].length - 1) height += 1;
                    
                    if (this.Tiles[tileIndex]) {
                        this.Tiles[tileIndex].model.material.map = await this.generateTexture(x,(x+width),(y),(y+height));
                        this.Tiles[tileIndex].model.material.needsUpdate = true;
                    }

                    tileIndex += 1;
                }
            }

        }
    }
}