import * as THREE from '../../libs/threejs/three.module.js';

/**
 * An object used by TerrainSource to display terrain. Handles dividing the images provided to the source into tiles.
 * 
 * @param {Object} config - Configures the source.
 * @param {string} config.texture - The url of the texture to draw over the tile.
 * @param {Array} config.heights - The heights of the tile, contained in an array. ([x][y])
 * @param {Array} config.detail - The detail level of the tile, ranging from 0.1 to 1.
 * @param {Array} config.position - The position of the tile.
 * 
 * @property {string} Texture - The url of the texture to draw over the tile.
 * @property {Array} Heights - The heights of the tile, contained in an array. ([x][y])
 * @property {Array} Detail - The detail level of the tile, ranging from 0.1 to 1.
 * @property {Array} Position - The position of the tile.
 * @property {THREE.Object3D} model - The THREE.Mesh of the tile.
 * 
 * @category Terrain
 * @class
 */
export class Tile {
  constructor(config) {
    if (config) {
      if (config.detail) this.Detail = config.detail;
      if (config.heights) this.Heights = config.heights; // heights[x][y]
      if (config.texture) this.Texture = config.texture;
      if (config.position) this.Position = config.position; // [x,y]
    }

    this.model = this.createModel();
  }

  /**
   * Generates the model of the tile.
   * @private
   * @function
   */
  createModel() {
    let material = new THREE.MeshBasicMaterial({ map: this.Texture, side: THREE.DoubleSide});
    let geometry = this.updateGeometry(0.1);

    let model = new THREE.Mesh(geometry,material)
    model.position.set(this.Position[0], this.Position[1], 0);
    model.userData = this;

    return model;
  }

    /**
   * Updates the geometry of the tile.
   * @param {float} detail - The detail level of the tile, ranging from 0.1 to 1.
   * @returns {THREE.Geometry} geometry - The geometry of the tile.
   * @private
   * @function
   */
  updateGeometry(detail) {
    this.Detail = detail;
    let geometry = new THREE.BufferGeometry();
  
    const width = this.Heights.length;
    const height = this.Heights[0].length;

    const widthSegments = Math.floor((width - 1) * detail);
    const heightSegments = Math.floor((height - 1) * detail);

    const width_half = width / 2;
    const height_half = height / 2;

    const gridX = widthSegments;
    const gridY = heightSegments;

    const gridX1 = gridX + 1;
    const gridY1 = gridY + 1;

    const segment_width = width / gridX;
    const segment_height = height / gridY;

    const indices = [];
    const vertices = [];
    const normals = [];
    const uvs = [];

    let whichX = 0;
    let whichY = 0;

    for ( let iy = 0; iy < gridY1; iy ++ ) {
      whichX = 0;

      const y = iy * segment_height - height_half;

      for ( let ix = 0; ix < gridX1; ix ++ ) {
        const x = ix * segment_width;

        vertices.push( x, y, this.Heights[whichX][whichY] );
        normals.push( 0, 0, 1 );
        uvs.push( ix / gridX );
        uvs.push( 1 - ( iy / gridY ) );

        whichX += Math.round(1/detail);
      }

      whichY += Math.round(1/detail);
    }

    for ( let iy = 0; iy < gridY; iy ++ ) {
      for ( let ix = 0; ix < gridX; ix ++ ) {
        const a = ix + gridX1 * iy;
        const b = ix + gridX1 * ( iy + 1 );
        const c = ( ix + 1 ) + gridX1 * ( iy + 1 );
        const d = ( ix + 1 ) + gridX1 * iy;

        indices.push( a, b, d );
        indices.push( b, c, d );
      }
    }

    geometry.setIndex( indices );
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
    geometry.setAttribute( 'normal', new THREE.Float32BufferAttribute( normals, 3 ) );
    geometry.setAttribute( 'uv', new THREE.Float32BufferAttribute( uvs, 2 ) );
  
    if(this.model) {
      this.model.geometry.dispose();
      this.model.geometry = geometry;
    }

    return geometry;
  }
}