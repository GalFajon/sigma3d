import { Utilities } from "../utilities/Utilities.js";
import { Source } from "./Source.js";

import * as THREE from '../../libs/threejs/three.module.js';


export class OverlaySource extends Source {
    constructor(config) {
        super();

        this.Overlays = [];
        this.Models = [];

        this.ThreeScene = Utilities.PotreeViewer.scene.scene;
        this.UseVisibilityDistance = true;
        this.Type = "OverlaySource";

        if (config) {
            if (config.overlays) this.Overlays = config.overlays;
            if (config.scene) this.ThreeScene = scene;
        }
    }

    async load() {
        for (let overlay of this.Overlays) {
            await this.add(overlay, false)
        }
    }

    async add(overlay, addToArray = true) {
        if (addToArray) this.Overlays.push(overlay);
        this.Models.push(overlay.model);
        overlay.attachToScene(); 
    }

    remove(overlay) {
        overlay.removeFromScene();
        if (this.Overlays.indexOf(overlay) != -1) this.Overlays.splice(this.Overlays.indexOf(overlay), 1);
        if (this.Models.indexOf(overlay.model) != -1) this.Models.splice(this.Models.indexOf(overlay.model), 1);
    }

    removeFromScene() {
        for (let overlay of this.Overlays) this.remove(overlay); 
        this.Overlays = [];
    }

    getBbox() {
        let bbox = new THREE.Box3();

        for (let overlay of this.Overlays) {
            for (let vector of overlay.Vectors) {
                bbox.expandByPoint(new THREE.Vector3(...vector));
            }
        }

        return bbox;
    }
}