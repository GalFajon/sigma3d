import { Utilities } from "../utilities/Utilities.js";
import { Source } from "./Source.js";
import { NexusObject } from "../../libs/nexus/nexus_three.js";

import * as THREE from '../../libs/threejs/three.module.js';

/**
 * A source used for displaying ".nxs" files. (http://vcg.isti.cnr.it/nexus/)
 * 
 * @param {Object} config - Configures the source.
 * @param {Array} config.urls - An array of urls pointing to the ".nxs" models.
 * @param {Object} config.scene - A THREE.Scene object to which all models are added.
 * 
 * @property {Array} Urls - The urls of the 360 degree images the source contains.
 * @property {THREE.Scene} ThreeScene - The THREE.Scene object all models are added to.
 * @property {string} Type - A string that specifies the type of source (e.g. "GeometrySource").
 * 
 * @category Source
 * @class
 */
export class NexusSource extends Source {
    
    constructor(config) {
        super();

        this.ThreeScene = Utilities.PotreeViewer.scene.scene;
        this.Urls = [];

        if (config) {
            if (config.urls) this.Urls = config.urls
            if (config.scene) this.ThreeScene = config.scene;
        }

        this.Type = "NexusSource";
    }

    /**
     * Internal function used for loading the source when initializing a map.
     * @private
     * @function
     */
    async load() {
        for (let url of this.Urls) {
            await this.add(url, false)
        }
    }

    /**
     * Used for adding a new model to the source.
     * @param {String} url - The url of the ".nxs" model to add.
     * @param {boolean} addToArray - True by default, if false the url will not be added to the Urls array.
     * @function
     */
    async add(url, addToArray = true) {
        return new Promise((resolve,reject) => {
            if (addToArray) this.Urls.push(url);
            let model = new NexusObject(url, function() { resolve(); }, function() {}, Utilities.PotreeViewer.renderer, false);
            
            this.ThreeScene.add(model);
            this.Models.push(model)
        })
    }

    /**
     * Used for removing a model from the source.
     * @param {NexusObject} imageSet - The nexus object to remove.
     * @function
     */
    remove(model) {
        this.ThreeScene.remove(model);

        if (this.Models.indexOf(model) > -1) {
            this.Models.splice( this.Models.indexOf(model), 1);
        }
    }

    /**
     * Removes all models from the scene.
     * @private
     * @function
     */
    removeFromScene() {
        for (let model of this.Models) {
            this.ThreeScene.remove(model);
        }

        this.Urls = [];
        this.Models = [];
    }

    /**
     * Returns the bounding box of the source.
     * @returns {THREE.Box3} bbox - The bounding box of the source. (made to contain every model)
     * @private
     * @function
     */
    getBbox() {
        let bbox = new THREE.Box3();

        for (let model of this.Models) {
            bbox.union(model.geometry.boundingBox);
        }

        return bbox;
    }
}