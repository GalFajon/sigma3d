import { Utilities } from "../utilities/Utilities.js";
import { Source } from "./Source.js";

import * as THREE from '../../libs/threejs/three.module.js';

export class PlySource extends Source {
    
    constructor(config) {
        super();

        this.Sources = [];
        this.Models = [];

        this.Type = "PlySource";
        this.ThreeScene = Utilities.PotreeViewer.scene.scene;

        if (config) {
            if (config.urls) this.Urls = config.urls;
        }
    }

    async load() {
        return new Promise( async(resolve, reject) => {
            if (this.Urls && this.Urls.length > 0) {
                for (let url of this.Urls) {
                    await this.add(url, false);
                }
            }

            resolve();
        });
    }

    async add(url, addToArray = true) {
        return new Promise((resolve,reject) => {
            Utilities.PLYLoader.load(
                url.ply,
                function (geometry) {
                    geometry.computeVertexNormals()                    
                    let mesh = new THREE.Mesh(geometry, new THREE.MeshNormalMaterial({ map: THREE.TextureLoader().load(url.texture) }));
                    this.Models.push(mesh);
                    this.ThreeScene.add(mesh)
                    if (addToArray) this.Urls.push(url);
                    resolve();
                }.bind(this),
                (xhr) => { },
                (error) => {
                    console.log(error)
                    reject();
                }
            )
        })
    }

    remove(model) {
        this.ThreeScene.remove(model);
        
        if (this.Models.indexOf(model) > -1){
            this.Models.splice( this.Models.indexOf(model), 1);
        }
    }

    removeFromScene() {
        for (let model of this.Models) {
            this.ThreeScene.remove(model);
        }
        this.Models = [];
    }

    getBbox() {
        let bbox = new THREE.Box3();

        for (let model of this.Models) {
            bbox.expandByObject(model);
        }
        
        return bbox;
    }
}