import { Utilities } from "../utilities/Utilities.js";
import { Source } from "./Source.js";

import * as THREE from '../../libs/threejs/three.module.js';

/**
 * A source made for displaying measurements that usually display physical distances, coordinates and other real world properties. Taken from the Potree library.
 * 
 * @param {Object} config - Configures the source.
 * @param {Array} config.measurements - An array of PotreeMeasurement objects to display.
 * @param {Array} config.volumes - An array of PotreeBoxVolume objects to display.
 * @param {Array} config.heightProfiles - An array of PotreeHeightProfiles to display.
 * @param {Object} config.scene - The Potree scene object that contains the measurements.
 * 
 * @property {Array} Measurements - An array of PotreeMeasurement objects belonging to the source.
 * @property {Array} Volumes - An array of PotreeBoxVolume objects belonging to the source.
 * @property {Array} HeightProfiles - An array of PotreeHeightProfiles belonging to the source.
 * @property {Array} Models - An array of THREE.Mesh objects tied to each measurement.
 * @property {PotreeScene} PotreeScene - The Potree scene object that contains the measurements.
 * @property {string} Type - A string that specifies the type of source (e.g. "GeometrySource").
 * 
 * @fires PotreeMeasurementSource#added 
 * @category Source
 * @class
 */

/**
 * @event PotreeMeasurementSource#added
 * @returns {Object} event.detail - Returns the measurement that was added to the source.
 */
export class PotreeMeasurementSource extends Source {
    
    constructor(config) {
        super();

        this.Measurements = [];
        this.Volumes = [];
        this.HeightProfiles = [];
        this.Models = [];

        this.PotreeScene = Utilities.PotreeViewer.scene;

        if (config) {
            if (config.measurements) this.Measurements = config.measurements;
            if (config.volumes) this.Volumes = config.volumes;
            if (config.heightProfiles) this.HeightProfiles = config.heightProfiles;
            if (config.scene) this.PotreeScene = config.scene;
        }

        this.Type = "PotreeMeasurementSource";
    }

    /**
     * Internal function used for loading the source when initializing a map.
     * @private
     * @function
     */
    async load() {
        for (let measurement of this.Measurements) await this.addMeasurement(measurement, false);
        for (let volume of this.Volumes) await this.addVolume(volume, false);
        for (let profile of this.HeightProfiles) await this.addProfile(profile, false);
    }

    /**
     * Removes the measurements of the source from the Potree scene.
     * @private
     * @function
     */
    removeFromScene() {
        for (let measurement of this.Measurements) this.removeMeasurement(measurement, false);
        for (let volume of this.Volumes) this.removeVolume(volume, false);
        for (let profile of this.HeightProfiles) this.removeProfile(profile, false); 

        this.Measurements = [];
        this.Volumes = [];
        this.HeightProfiles = [];
        this.Models = [];
    }

    /**
     * Used for adding a new potree measurement to the source.
     * @param {PotreeMeasurement} measurement - The PotreeMeasurement to be added to the source.
     * @param {boolean} addToArray - True by default, if false the measurement will not be added to the Measurements array.
     * @function
     */
    async addMeasurement(measurement, addToArray = true) {
        this.PotreeScene.addMeasurement(measurement);
        if (addToArray) this.Measurements.push(measurement);

        for (let sphere of measurement.spheres) {
            sphere.index = (this.Models.length - 1);
            this.Models.push(sphere);

            sphere.removeEventListener('drag', sphere.drag);
            sphere.removeEventListener('drop', sphere.drop);
            sphere.removeEventListener('mouseover', sphere.mouseover);
            sphere.removeEventListener('mouseleave', sphere.mouseleave);
        }

        const customEvent = new CustomEvent('added', { detail: { measurement: measurement } });
        this.dispatchEvent(customEvent);
    }

    /**
     * Used for removing a potree measurement from the source.
     * @param {PotreeMeasurement} measurement - The PotreeMeasurement to be removed.
     * @function
     */
    removeMeasurement(measurement) {
        this.PotreeScene.removeMeasurement(measurement);

        if (this.Measurements.indexOf(measurement) > -1) {
            this.Measurements.splice( this.Measurements.indexOf(measurement), 1);
        }

        for (let sphere of measurement.spheres) {
            if (this.Models.indexOf(sphere) > -1) {
                this.Models.splice( this.Models.indexOf(sphere), 1);
            }
        }

        const customEvent = new CustomEvent('removed', { detail: { measurement: measurement } });
        this.dispatchEvent(customEvent);
    }

    /**
     * Used for adding a new potree measurement to the source.
     * @param {PotreeBoxVolume} volume - The PotreeBoxVolume to be added to the source.
     * @param {boolean} addToArray - True by default, if false the volume will not be added to the Volumes array.
     * @function
     */
    async addVolume(volume, addToArray = true) {
        volume.index = (this.Volumes.length - 1);
        volume.box.index = (this.Models.length - 1);

        this.PotreeScene.addVolume(volume);
        
        if (addToArray) this.Volumes.push(volume);

        this.Models.push(volume);

        Utilities.PotreeViewer.inputHandler.blacklist.add(volume);

        const customEvent = new CustomEvent('added', { detail: { volume: volume } });
        this.dispatchEvent(customEvent);
    }

    /**
     * Used for removing a potree volume from the source.
     * @param {PotreeBoxVolume} volume - The PotreeBoxVolume to be removed from the source.
     * @function
     */
    removeVolume(volume) {
        this.PotreeScene.removeVolume(volume);

        if (this.Volumes.indexOf(volume) > -1) {
            this.Volumes.splice(this.Volumes.indexOf(volume), 1);
        }

        if (this.Models.indexOf(volume) > -1) {
            this.Models.splice(this.Models.indexOf(volume), 1);
        }

        const customEvent = new CustomEvent('removed', { detail: { volume: volume } });
        this.dispatchEvent(customEvent);
    }

    /**
     * Used for adding a new potree height profile to the source.
     * @param {PotreeBoxVolume} volume - The PotreeHeightProfile to be added to the source.
     * @param {boolean} addToArray - True by default, if false the profile will not be added to the Profiles array.
     * @function
     */
    async addProfile(profile, addToArray = true) {
        this.PotreeScene.addProfile(profile);
        if (addToArray) this.HeightProfiles.push(profile);

        for (let sphere of profile.spheres) {
            sphere.index = this.Models.length;
            this.Models.push(sphere);

            sphere.removeEventListener('drag', sphere.drag);
            sphere.removeEventListener('drop', sphere.drop);
            sphere.removeEventListener('mouseover', sphere.mouseover);
            sphere.removeEventListener('mouseleave', sphere.mouseleave);
        }

        const customEvent = new CustomEvent('added', { detail: { source: this, profile: profile } });
        this.EventNode.dispatchEvent(customEvent);
    }

    /**
     * Used for removing a potree height profile from the source.
     * @param {PotreeHeightProfile} profile - The PotreeHeightProfile to be removed from the source.
     * @function
     */
    removeProfile(profile) {
        this.PotreeScene.removeProfile(profile);

        if (this.HeightProfiles.indexOf(profile) > -1) {
            this.HeightProfiles.splice( this.HeightProfiles.indexOf(profile), 1);
        }

        for (let sphere of profile.spheres) {
            if (this.Models.indexOf(sphere) > -1) {
                this.Models.splice(this.Models.indexOf(sphere), 1);
            }
        }

        const customEvent = new CustomEvent('removed', { detail: { source: this, profile: profile } });
        this.EventNode.dispatchEvent(customEvent);
    }

    /**
     * Returns the bounding box of the source.
     * @returns {THREE.Box3} bbox - The bounding box of the source. (made to contain every model)
     * @private
     * @function
     */
    getBbox() {
        let bbox = new THREE.Box3();

        for (let model of this.Models) {
            bbox.expandByObject(model);
        }

        return bbox;
    }
}