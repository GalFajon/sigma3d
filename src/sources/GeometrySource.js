/**
 * A source used for drawing geometries in 3D space. (see the geometries section for more details)
 * 
 * @param {Object} config - Configures the source.
 * @param {Object} config.geometries - An array of geometries to add to the layer.
 * @param {Object} config.scene - A THREE.Scene object to which all geometries are added.
 * 
 * @param {Sigma3D.Source} config.source - The source the layer is bound to.
 * 
 * @property {Array} Geometries - An array of the geometries the source contains.
 * @property {Array} Models - An array of the THREE models the source contains.
 * @property {string} Type - A string that specifies the type of source (e.g. "GeometrySource").
 * @property {THREE.Scene} ThreeScene - The THREE.Scene objects all geometries are added to.
 * 
 * @property {string} Type - Indicates the layer type.
 * @fires GeometrySource#added 
 * @category Source
 * @class
 */

/**
 * @event GeometrySource#added
 * @returns {Object} event.detail - Returns the source and the geometry that was added to it.
 */

import { Utilities } from "../utilities/Utilities.js";
import { Source } from "./Source.js";

import * as THREE from '../../libs/threejs/three.module.js';

import { 
    LineMaterial,
    PointMaterial,
    PolygonMaterial,
    HeightPointMaterial,

    MeshSelectionMaterial, LineSelectionMaterial, PointSelectionMaterial, SymbolMaterialCache 
} from "../utilities/Materials.js";

export class GeometrySource extends Source {
    
    constructor(config) {
        super();

        this.Geometries = [];

        this.Points = new Map();
        this.Points.set('default', []);
        this.Points.set('height', []);
        this.Points.set('highlighted', []);

        this.PointsCloud = new Map();
        this.PointsCloud.set('default', new THREE.Points(new THREE.BufferGeometry(), PointMaterial));
        this.PointsCloud.set('height', new THREE.Points(new THREE.BufferGeometry(), HeightPointMaterial));
        this.PointsCloud.set('highlighted', new THREE.Points(new THREE.BufferGeometry(), PointSelectionMaterial));

        this.PointVertices = new Map();
        this.PointVertices.set('default', []);
        this.PointVertices.set('height', []);
        this.PointVertices.set('highlighted', []);

        this.Models = [];
        this.ThreeScene = Utilities.PotreeViewer.scene.scene;

        this.HidingPolygonSurroundingLines = false;

        this.Type = "GeometrySource";

        if (config) {
            if (config.geometries) this.Geometries = config.geometries;
            if (config.scene) this.ThreeScene = scene;
        }
    }

    /**
     * Internal function used for loading the source when initializing a map.
     * @private
     * @function
     */
    async load() {
        for (let geometry of this.Geometries) await this.add(geometry, false)
        this.updatePoints();
    }

    /**
     * Used for adding a new geometry to the source.
     * @param {Sigma3D.HeightPoint | Sigma3D.Polygon | Sigma3D.Line | Sigma3D.Point | Sigma3D.Symbol} geometry - The geometry to add.
     * @param {boolean} addToArray - True by default, if false the geometry will not be added to the Geometries array.
     * @function
     */
    async add(geometries, addToArray = true) {
        if (!Array.isArray(geometries)) geometries = [ geometries ];

        for (let geometry of geometries) {
            if (addToArray) this.Geometries.push(geometry);

            if (geometry.Type == 'Polygon' && this.HidingPolygonSurroundingLines) geometry.toggleSurroundingLine();
            if (geometry.model) {
                this.Models.push( geometry.model );
                this.ThreeScene.add(geometry.model); 
            }

            const customEvent = new CustomEvent('added', { detail: { source: this, geometry: geometry } });
            this.dispatchEvent(customEvent);
        }

        this.updatePoints();
    }

    updatePoints() {
        this.PointVertices.clear();
        this.PointVertices.set('default', []);
        this.PointVertices.set('highlighted', []);
        this.PointVertices.set('height', []);
        
        this.Points.clear();
        this.Points.set('default', []);
        this.Points.set('highlighted', []);
        this.Points.set('height', []);
        
        let rootCoords = [];
        let first = true;

        for (let geometry of this.Geometries) {
            if (geometry.Type == 'Point' || geometry.Type == 'HeightPoint') {
                if (first == true) {
                    rootCoords = geometry.Vectors[0];
                    first = false;
                }

                let relativeCoords = [ geometry.Vectors[0][0] - rootCoords[0], geometry.Vectors[0][1] - rootCoords[1], geometry.Vectors[0][2] - rootCoords[2] ];

                if (geometry.highlighted) {
                    this.Points.get('highlighted').push(geometry);
                    this.PointVertices.get('highlighted').push(...relativeCoords);
                }
                else if (geometry.Type == 'HeightPoint') {
                    this.Points.get('height').push(geometry);
                    this.PointVertices.get('height').push(...relativeCoords);
                }
                else if (geometry.Symbol != undefined) {
                    if (!SymbolMaterialCache.has(geometry.Symbol)) SymbolMaterialCache.set(geometry.Symbol, new THREE.PointsMaterial({ transparent: true, size: 7, map: new THREE.TextureLoader().load(geometry.Symbol) }));
                    if (!this.PointsCloud.has(geometry.Symbol)) this.PointsCloud.set(geometry.Symbol, new THREE.Points(new THREE.BufferGeometry(), SymbolMaterialCache.get(geometry.Symbol)));
                    if (!this.PointVertices.has(geometry.Symbol)) this.PointVertices.set(geometry.Symbol, []);
                    if (!this.Points.has(geometry.Symbol)) this.Points.set(geometry.Symbol,[]);

                    this.PointVertices.get(geometry.Symbol).push(...relativeCoords);
                    this.Points.get(geometry.Symbol).push(geometry);
                }
                else {
                    this.Points.get('default').push(geometry);
                    this.PointVertices.get('default').push(...relativeCoords);    
                }
            }
        }

        for (const [key, value] of this.PointsCloud) {
            this.ThreeScene.remove(value);
            
            if (this.PointVertices.get(key)) {
                value.geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( this.PointVertices.get(key), 3 ) );
                value.geometry.setDrawRange( 0, this.PointVertices.get(key).length );
                value.geometry.verticesNeedUpdate = true;
                value.geometry.computeBoundingSphere();

                value.position.set(...rootCoords);
                this.ThreeScene.add(value);
            }
            else {
                value.geometry.dispose();
                value.material.dispose();
                this.PointsCloud.delete(key);
            }
        }
    }

    /**
     * Toggles the lines that surround polygons.
     * @function
     */
    togglePolygonSurroundingLines() {
        this.HidingPolygonSurroundingLines = !this.HidingPolygonSurroundingLines;
        for (let geometry of this.Geometries) if (geometry.Type == "Polygon") geometry.toggleSurroundingLine();
    }
    
    /**
     * Used for removing a geometry from the source.
     * @param {Sigma3D.HeightPoint | Sigma3D.Polygon | Sigma3D.Line | Sigma3D.Point | Sigma3D.Symbol} geometry - Geometry to remove.
     * @function
     */
    remove(geometry) {
        if (geometry.Type == 'Line' || geometry.Type == 'Polygon') {
            geometry.model.geometry.dispose();
            this.ThreeScene.remove(geometry.model);
            
            if (this.Models.indexOf(geometry.model) > -1){
                this.Models.splice( this.Models.indexOf(geometry.model), 1);
            }
        }        

        if (this.Geometries.indexOf(geometry) > -1) this.Geometries.splice( this.Geometries.indexOf(geometry), 1);
        this.updatePoints();
        
        const customEvent = new CustomEvent('removed', { detail: { source: this, geometry: geometry } });
        this.dispatchEvent(customEvent);
    }

    /**
     * Removes geometry models from the THREE.Scene.
     * @private
     * @function
     */
    removeFromScene() {
        for (let model of this.Models) {
            this.ThreeScene.remove(model);

            if (model.userData.Type == 'Line' || model.userData.Type == 'Polygon') {
                model.geometry.dispose();
            }
        }

        this.Geometries = [];
        this.Models = [];
    }

    setPointSymbol(geometry, symbol) {
        if (symbol == undefined) { geometry.Symbol = 'default'; this.updatePoints(); return; }
        if (symbol == 'height') { console.warn("Cannot set point to heightpoint via symbols."); return; }
        if (symbol == 'selected') { console.warn("Cannot set point to selected via symbols."); return; }

        geometry.Symbol = symbol;
        this.updatePoints();
    }

    addMaterial(name, material) {
        SymbolMaterialCache.set(name, material);
        this.PointVertices.set(name, []);
        this.Points.set(name, []);
        this.PointsCloud.set(name, new THREE.Points(new THREE.BufferGeometry(), material));
    }

    /**
     * Highlights a geometry, used by the selectInteraction.
     * @param {Sigma3D.HeightPoint | Sigma3D.Polygon | Sigma3D.Line | Sigma3D.Point | Sigma3D.Symbol} geometry - Geometry to highlight.
     * @private
     * @function
     */
    highlightGeometry(geometry) {
        if (geometry.Type == 'Line') {
            geometry.model.material = LineSelectionMaterial;
        }
        else if(geometry.Type == 'Point' || geometry.Type == 'HeightPoint') {
            geometry.highlighted = true;
            this.updatePoints();
        }
        else if (geometry.Type == 'Polygon') geometry.model.material = MeshSelectionMaterial;
    }

    /**
     * Removes highlight from a geometry. Used by the selectInteraction.
     * @param {Sigma3D.HeightPoint | Sigma3D.Polygon | Sigma3D.Line | Sigma3D.Point | Sigma3D.Symbol} geometry - Geometry to remove highlight from.
     * @private
     * @function
     */
    removeGeometryHighlight(geometry) {
        if (geometry) {
            if (geometry.CustomMaterial) {
                geometry.model.material = geometry.CustomMaterial;
            }
            else if (geometry.Type == 'Line') {
                geometry.model.material = LineMaterial;
            }
            else if (geometry.Type == 'Point' || geometry.Type == 'HeightPoint') {
                geometry.highlighted = false;
                this.updatePoints();
            }
            else if (geometry.Type == 'Polygon') {
                geometry.model.material = PolygonMaterial;
            }
        }
    }

    /**
     * Returns the bounding box of the source.
     * @returns {THREE.Box3} bbox - The bounding box of the source. (made to contain every model)
     * @private
     * @function
     */
    getBbox() {
        let bbox = new THREE.Box3();

        for (let geometry of this.Geometries) {
            for (let vector of geometry.Vectors) {
                bbox.expandByPoint(new THREE.Vector3(...vector));
            }
        }

        return bbox;
    }
}