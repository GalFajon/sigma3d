import { Utilities } from "../utilities/Utilities.js";
import { Source } from "./Source.js";

import { WMSTile } from "../terrain/WMSTile.js";
import { WMSImageCache } from "../terrain/WMSImageCache.js";

import { ImageGenerator } from "../utilities/ImageGenerator.js";

import * as THREE from '../../libs/threejs/three.module.js';

/**
 * @class WMSTerrainSource
 * 
 * @param {Object} config - Configures the source.
 * @param {Array} config.topLeftCorner - An array containing the coordinates of the top left corner of the map. (e.g. [373217.6542445395,194661.54622136615])
 * @param {Object} config.zoomLevels - An object that defines the zoom levels of the source. Returned by the "parseZoomLevelsFromGeoserverXML()" method.
 * @param {int} config.tileMetres - The width and height of individual tiles in metres.
 * @param {int} config.tilePixels - The width and height of individual tiles in pixels.
 * @param {int} config.bottomZoomBoundary - The bottom boundary of the "zoomLevels property".
 * @param {int} config.topZoomBoundary - The top boundary of the "zoomLevels property".
 * @param {Object} config.source - The source from which to draw the heights and textures of terrain.
 * @param {Array} config.source.wms - An array of urls pointing to geoserver WMS that provide textures and a boolean that determines whether or not the layer is hidden. ({url: '...', hidden: true/false}) 
 * @param {string} config.source.tif - A url pointing to a folder containing the GeoTIFF images used to determine tile heights. (stored as: baseFolder/zoomLevel/row_column.tif)
 * @param {string} config.source.srs - The coordinate system of the tilegrid. (e.g. "EPSG3794")
 * 
 * @property {Object} Source - The source object provided in the configuration.
 * @property {Object} ZoomLevels - The zoomLevels object provided in the configuration.
 * @property {Array} TopLeftCorner - The array provided in the configuration.
 * @property {int} TileMetres - The value provided in the configuration.
 * @property {int} TilePixels - The value provided in the configuration.
 * @property {int} BottomZoomBoundary - The value provided in the configuration.
 * @property {int} TopZoomBoundary - The value provided in the configuration.
 * 
 * @property {map} Tiles - A map of tiles used by the terrain, the key is a string in the format (row+'_'+column+'_'+zoom).
 * @property {map} RemovedTiles - A map of tiles to be removed when regenerating the terrain, the key is a string in the format (row+'_'+column+'_'+zoom).
 * @property {map} NeededTiles - A map of tiles to be generated when regenerating the terrain, the key is a string in the format (row+'_'+column+'_'+zoom).
 * @property {map} NeededTileHeights - A map of heights used by NeededTiles, the key is a string in the format (row+'_'+column+'_'+zoom).
 * 
 * @property {int} LowestZoomLevel - The lowest zoom level in the configuration file.
 * @property {int} HighestZoomLevel - The highest zoom level in the configuration file.
 * @property {int} InitialZoomLevel - The first zoom level that has defined tiles.
 * 
 * @property {Array} Resolutions - The width of a tile in metres at a given zoom level.
 * @property {Array} MatrixIds - An array of zoom matrix ids. (format: "coordinateSystem:zoomLevel", e.g. "EPSG:3794:18")      
 * @property {Array} TileGrid - An OpenLayers tilegrid object (https://geoadmin.github.io/ol3/apidoc/ol.tilegrid.TileGrid.html) used for calculating terrain division.
 * 
 * @property {Array} PreviousGenPosition - The previous position where the terrain was regenerated.
 * @property {Array} CurrentGenPosition - The position where the terrain is currently being regenerated.
 * @property {Array} TerrainBoundaries - An array containing the boundaries of the terrain. ([leftX,rightX,topY,bottomY])
 * 
 * @property {boolean} CanUpdateGenPosition - Determines whether or not the terrain generation position is updated to the camera position of the view.
 * @property {boolean} FirstGeneration - Determines whether or not the terrain is being generated for the first time.
 * @property {boolean} Generating - Determines whether or not the terrain is being regenerated.
 * @property {Array} Models - The models of the WMSTiles used by the source.
 * 
 * @property {THREE.Scene} ThreeScene - The THREE.Scene objects all geometries are added to.
 * @property {string} Type - A string that specifies the type of source (e.g. "GeometrySource").
 * 
 * @extends {Source}
 */

export class WMSTerrainSource extends Source {
    
    constructor(config) {
        super();
        
        this.Source = undefined;
        this.Heights = [];

        this.Type = "WMSTerrainSource";
        this.ThreeScene = Utilities.PotreeViewer.scene.scene;
        
        this.CacheSize = 30;

        if (config) {
            if (config.source) this.Source = config.source;
            if (config.topLeftCorner) this.TopLeftCorner = config.topLeftCorner;
            if (config.tileMetres) this.TileMetres = config.tileMetres;
            if (config.tilePixels) this.TilePixels = config.tilePixels;
            if (config.zoomLevels) this.ZoomLevels = config.zoomLevels;
            if (config.regenerationInterval) this.RegenerationInterval = config.regenerationInterval;
            if (config.cacheSize) this.CacheSize = config.cacheSize;
            if (config.bottomZoomBoundary) this.BottomZoomBoundary = config.bottomZoomBoundary;
            if (config.topZoomBoundary) this.TopZoomBoundary = config.topZoomBoundary;
        }

        this.Tiles = new Map();

        this.NeededTiles = new Map();
        this.RemovedTiles = new Map();
        
        this.NeededTileHeights = new Map();

        this.Models = [];

        this.LowestZoomLevel;
        this.HighestZoomLevel;
        this.InitialZoomLevel;

        this.PreviousGenPosition = [0,0,0];
        this.CurrentGenPosition = [0,0,0];

        this.calculateZoomBoundaries();
        this.calculateTileGrid();
        this.calculateTerrainBoundaries();
        
        this.WMSImageCache = new WMSImageCache(this.CacheSize, this.ZoomLevels);
        this.WCSImageCache = new WMSImageCache(this.CacheSize, this.ZoomLevels);

        this.CanUpdateGenPosition = true;
        this.FirstGeneration = true;

        this.Generating = false;

        Utilities.PotreeViewer.addEventListener('360image_focused',  (e) => { 
            this.CanUpdateGenPosition = false;
            this.CurrentGenPosition = e.detail.image.position;
        });
    
        Utilities.PotreeViewer.addEventListener('360image_unfocused',  (e) => { 
            this.CanUpdateGenPosition = true;
        });

        this.camPos = undefined;
        this.prevCamPos = undefined;
        this.HighestZoomBoundaryGenerated = this.TopZoomBoundary;
    }

    /**
     * Internal function used for loading the source when initializing a map.
     * @private
     * @function
     */
    load() {
        this.camPos = Utilities.PotreeViewer.scene.getActiveCamera().position.clone();
        if (!this.prevCamPos) this.prevCamPos = this.camPos;

        setInterval(this.regenerate.bind(this), this.RegenerationInterval);

        if (!this.InitialZoomLevel) {
            this.InitialZoomLevel = 0;

            for (const zoomLevel in this.ZoomLevels) {
                if (this.ZoomLevels[zoomLevel].minTileCol != this.ZoomLevels[zoomLevel].maxTileCol || 
                    this.ZoomLevels[zoomLevel].minTileRow != this.ZoomLevels[zoomLevel].maxTileRow) {
                    if (!this.InitialZoomLevel) this.InitialZoomLevel = zoomLevel;
                    break;
                }
            }  
        }

        let width = (this.TerrainBoundaries[1] - this.TerrainBoundaries[0]) / 2;
        let height = (this.TerrainBoundaries[2] - this.TerrainBoundaries[3]) / 2;

        this.generateTerrain([this.TerrainBoundaries[0] + width,this.TerrainBoundaries[3] + height,0], this.BottomZoomBoundary, this.BottomZoomBoundary + 1,true);
    }
    
    /**
     * Internal function used for calculating the resolutions and matrix ids of the tilegrid and finally adding the tilegrid object.
     * @private
     * @function
     */
    calculateTileGrid() {
        this.Resolutions = new Array(this.HighestZoomLevel);
        this.MatrixIds = new Array(this.HighestZoomLevel);      

        for (let z = 0; z <= this.HighestZoomLevel; ++z) {
            this.Resolutions[z] = this.TileMetres / Math.pow(2, z);
            this.MatrixIds[z] = "EPSG:3794:" + z;
         }

        this.TileGrid = new ol.tilegrid.WMTS({
            origin: this.TopLeftCorner,
            resolutions: this.Resolutions,
            matrixIds: this.MatrixIds,
        })
    }

    /**
     * Internal function used for calculating the zoom boundaries of the source.
     * @private
     * @function
     */
    calculateZoomBoundaries() {
        let i=0;
        if (this.ZoomLevels) while (this.ZoomLevels[i + 1]) {i += 1;}

        this.LowestZoomLevel = 0;
        this.HighestZoomLevel = i;
    }

    /**
     * Internal function used for calculating the boundaries of the terrain, displayed by the source.
     * @private
     * @function
     */
    calculateTerrainBoundaries() {
        let leftX = this.TileGrid.getTileCoordExtent([this.BottomZoomBoundary, this.ZoomLevels[this.BottomZoomBoundary].minTileCol, -this.ZoomLevels[this.BottomZoomBoundary].minTileRow])[0];
        let rightX = this.TileGrid.getTileCoordExtent([this.BottomZoomBoundary, this.ZoomLevels[this.BottomZoomBoundary].maxTileCol, -this.ZoomLevels[this.BottomZoomBoundary].maxTileRow-1])[2];
        let topY = this.TileGrid.getTileCoordExtent([this.BottomZoomBoundary, this.ZoomLevels[this.BottomZoomBoundary].minTileCol, -this.ZoomLevels[this.BottomZoomBoundary].minTileRow])[1]
        let bottomY = this.TileGrid.getTileCoordExtent([this.BottomZoomBoundary, this.ZoomLevels[this.BottomZoomBoundary].maxTileCol, -this.ZoomLevels[this.BottomZoomBoundary].maxTileRow-1])[1];

        this.TerrainBoundaries = [leftX,rightX,topY,bottomY];
    }

    /**
     * Internal function used for getting the heights of a tile from the filesystem.
     * @param {int} zoom - The zoom level of the tile.
     * @param {int} row - The row of the tile.
     * @param {int} column - The column of the tile.
     * @private
     * @function
     */
    async getHeights(zoom,row,column) {
        return new Promise( async (resolve,reject) => {
            try {
                const response = await fetch(`${this.Source.tif}${zoom}/${row}_${column}.tif`)
                .then(async (response) => {
                    const rawTiff = await Utilities.GeoTIFF.fromArrayBuffer(await response.arrayBuffer());
                    const tifImage = await rawTiff.getImage();
                    const heights = await tifImage.readRasters({ interleave: true });
                    
                    this.WCSImageCache.add(zoom,heights);

                    resolve(heights);
                })
                .catch((error) => {
                    let heights = [];
    
                    for (let i=0; i < this.TilePixels*this.TilePixels; i++) heights.push(0);
        
                    resolve(heights);
                })
            }
            catch (error) {
                let heights = [];
    
                for (let i=0; i < this.TilePixels*this.TilePixels; i++) heights.push(0);
    
                resolve(heights);
            }
        })
    }

    /*removeTileSeams() {
        for (let [key,value] of this.NeededTiles.entries()) {
            const [row,column,zoom] = key.split('_');
            let heights = this.NeededTileHeights.get(row+'_'+(column)+'_'+zoom);

            let prevCol = Math.floor(column / 2);
            let prevRow = Math.floor(row / 2);
            
            let right = false
            let bottom = false

            if (column % 2 == 1)  right = true;
            if (row % 2 == -1) bottom = true;

            if (this.NeededTileHeights.has(row+'_'+(column-1)+'_'+zoom)) {
                const edgeHeights = this.NeededTileHeights.get(row+'_'+(column-1)+'_'+zoom);
                for (let y=0; y <= this.TilePixels;y++) if (edgeHeights[255+y*this.TilePixels]) heights[0+y*this.TilePixels] = edgeHeights[255+y*this.TilePixels];
            }
            else if (!right) {
                if (this.NeededTileHeights.has(prevRow+'_'+(prevCol-1)+'_'+(zoom-1))) {
                    const edgeHeights = this.NeededTileHeights.get(prevRow+'_'+(prevCol-1)+'_'+(zoom-1));

                    let i = 0;
                    if (bottom == false) i = this.TilePixels/2;

                    for (;i <= (this.TilePixels/2) + ((!bottom)*this.TilePixels/2);i++) {
                        if (edgeHeights[255+i*this.TilePixels]) {
                            heights[0+Math.floor(i*2)*this.TilePixels] = edgeHeights[255+i*this.TilePixels];
                        }
                    }
                }
            }

            if (this.NeededTileHeights.has(row+'_'+(column+1)+'_'+zoom)) {
                const edgeHeights = this.NeededTileHeights.get(row+'_'+(column+1)+'_'+zoom);
                for (let y=0; y <= this.TilePixels;y++) if (edgeHeights[0+y*this.TilePixels]) heights[255+y*this.TilePixels] = edgeHeights[0+y*this.TilePixels];
            }
            else if(right) {
                if (this.NeededTileHeights.has(prevRow+'_'+(prevCol+1)+'_'+(zoom-1))) {
                    const edgeHeights = this.NeededTileHeights.get(prevRow+'_'+(prevCol+1)+'_'+(zoom-1));

                    let i = 0;
                    if (bottom == false) i = this.TilePixels/2;

                    for (;i <= (this.TilePixels/2) + ((!bottom)*this.TilePixels/2);i++) {
                        if (edgeHeights[0+i*this.TilePixels]) {
                            heights[255+Math.floor(i*2)*this.TilePixels] = edgeHeights[0+i*this.TilePixels];
                        }
                    }
                }
            }

            if (this.NeededTileHeights.has((row-1)+'_'+column+'_'+zoom)) {
                const edgeHeights = this.NeededTileHeights.get((row-1)+'_'+column+'_'+zoom);
                for (let x=0; x <= this.TilePixels;x++) if (edgeHeights[x]) heights[x+255*this.TilePixels] = edgeHeights[x];
            }
            else if(bottom) {
                if (this.NeededTileHeights.has((prevRow+1)+'_'+prevCol+'_'+(zoom-1))) {
                    const edgeHeights = this.NeededTileHeights.get((prevRow+1)+'_'+prevCol+'_'+(zoom-1));

                    let i = 0;
                    if (right == true) i = this.TilePixels/2;

                    for (;i <= (this.TilePixels/2) + ((right)*this.TilePixels/2);i++) {
                        if (edgeHeights[i+255*this.TilePixels]) heights[(i*2)] = edgeHeights[i+255*this.TilePixels];
                    }
                }
            }

            if (this.NeededTileHeights.has((row+1)+'_'+column+'_'+zoom)) {
                const edgeHeights = this.NeededTileHeights.get((row+1)+'_'+column+'_'+zoom);
                for (let x=0; x <= this.TilePixels;x++) if (edgeHeights[x+255*this.TilePixels]) heights[x+0*this.TilePixels] = edgeHeights[x+255*this.TilePixels];
            }
            else if(!bottom) {
                if (this.NeededTileHeights.has((prevRow-1)+'_'+prevCol+'_'+(zoom-1))) {
                    const edgeHeights = this.NeededTileHeights.get((prevRow-1)+'_'+prevCol+'_'+(zoom-1));

                    let i = 0;
                    if (right == true) i = this.TilePixels/2;

                    for (;i <= (this.TilePixels/2) + ((right)*this.TilePixels/2);i++) {
                        if (edgeHeights[i]) heights[i*2+255*this.TilePixels] = edgeHeights[i];
                    }
                }
            }


            this.NeededTileHeights.set(heights);
        }
    }*/

    /**
     * Internal function used for generating a WMSTile and adding it to the scene.
     * @param {int} zoom - The zoom level of the tile.
     * @param {int} row - The row of the tile.
     * @param {int} column - The column of the tile.
     * @returns {WMSTile} tile - The generated tile.
     * @private
     * @function
     */
    generateTile(zoom,row,column) {
        if (
            (Math.abs(row) <= (this.ZoomLevels[zoom].maxTileRow + 1) && Math.abs(row) >= (this.ZoomLevels[zoom].minTileRow)) && 
            (Math.abs(column) <= (this.ZoomLevels[zoom].maxTileCol + 1) && Math.abs(column) >= (this.ZoomLevels[zoom].minTileCol))
        ) {
            let width = ((this.ZoomLevels[zoom].tileWidth * this.ZoomLevels[zoom].scaleDenominator) * (0.00028));
            let height = ((this.ZoomLevels[zoom].tileHeight * this.ZoomLevels[zoom].scaleDenominator) * (0.00028));

            let extent = this.TileGrid.getTileCoordExtent([zoom, parseInt(column), parseInt(row)]);
            
            row = parseInt(row);
            column = parseInt(column);
            zoom = parseInt(zoom);
            
            let detail = 0.1;
            if (zoom >= 8) detail = 0.5;
            if (zoom >= 12) detail = 1;
    
            let tile = new WMSTile({
                zoomLevel: zoom,
                detail: detail,

                width: width,
                height: height,
                pixelWidth: this.TilePixels,
                pixelHeight: this.TilePixels,
                    
                position: [ extent[0], extent[1] ],
                row: Math.abs(row),
                column: column,

                parentSource: this
            });

            tile.initialize();

            this.ThreeScene.add(tile.model);
            this.Models.push(tile.model);
            
            return tile;
        }
    }
    
    /**
     * Internal function used for removing the tiles of the terrain.
     * @private
     * @function
     */
    clearTiles() {
        for (const [key,value] of this.RemovedTiles.entries()) {
            this.removeTile(value);
        }

        this.RemovedTiles.clear();
    }

    /**
     * Internal function used for generating the tiles surrounding a position.
     * @param {int} position - The position the tiles are generated around ([x,y,z]).
     * @param {int} bottomZoomBoundary - The lowest zoom boundary to generate from.
     * @param {int} topZoomBoundary - The highest zoom boundary to generate to.
     * @param {boolean} first - Used during recursion. Should always be true when called externally.
     * @param {int} extraSplits - The amount of additional tiles to draw surrounding the central four.
     * @private
     * @function
     */
    generateTiles(position,bottomZoomBoundary,topZoomBoundary,first,extraSplits) {
        let [zoom,column,row] = this.TileGrid.getTileCoordForCoordAndZ(position, bottomZoomBoundary);
        let extent = this.TileGrid.getTileCoordExtent([zoom,column,row]);

        let width = (this.ZoomLevels[zoom].tileWidth * this.ZoomLevels[zoom].scaleDenominator) * (0.00028);
        let height = (this.ZoomLevels[zoom].tileHeight * this.ZoomLevels[zoom].scaleDenominator) * (0.00028);

        if (first) {        
            for (let i=parseInt(this.ZoomLevels[bottomZoomBoundary].minTileCol); i <= parseInt(this.ZoomLevels[bottomZoomBoundary].maxTileCol); i++) {
                for (let j=parseInt(this.ZoomLevels[bottomZoomBoundary].minTileRow); j <= parseInt(this.ZoomLevels[bottomZoomBoundary].maxTileRow); j++) {
                    if ((j != Math.abs(row + 1) || i != column)) {
                        this.NeededTiles.set((-j)+"_"+i+"_"+bottomZoomBoundary)
                    } 
                }
            }
        }

        if (bottomZoomBoundary < topZoomBoundary) {
            let topLeftExtent = [extent[0],extent[1]];
            let topRightExtent = [extent[2],extent[1]];
            let bottomLeftExtent = [extent[0],extent[3]];
            let bottomRightExtent = [extent[2],extent[3]];

            let topLeftDistance = Math.sqrt((position[0] - topLeftExtent[0])*(position[0] - topLeftExtent[0]) + (position[1] - topLeftExtent[1])*(position[1] - topLeftExtent[1]));
            let topRightDistance = Math.sqrt((position[0] - topRightExtent[0])*(position[0] - topRightExtent[0]) + (position[1] - topRightExtent[1])*(position[1] - topRightExtent[1]));
            let bottomLeftDistance = Math.sqrt((position[0] - bottomLeftExtent[0])*(position[0] - bottomLeftExtent[0]) + (position[1] - bottomLeftExtent[1])*(position[1] - bottomLeftExtent[1]));
            let bottomRightDistance = Math.sqrt((position[0] - bottomRightExtent[0])*(position[0] - bottomRightExtent[0]) + (position[1] - bottomRightExtent[1])*(position[1] - bottomRightExtent[1]));

            let shortestDistance = Math.min(topLeftDistance,topRightDistance,bottomLeftDistance,bottomRightDistance);

            let smallestRow;
            let largestRow;
            let smallestCol;
            let largestCol;

            if (shortestDistance == topLeftDistance) { smallestRow=-1; largestRow=1; smallestCol=-1; largestCol=1; }
            if (shortestDistance == topRightDistance) { smallestRow=-1; largestRow=1; smallestCol=0; largestCol=2; }
            if (shortestDistance == bottomLeftDistance) { smallestRow=0; largestRow=2; smallestCol=-1; largestCol=1; }
            if (shortestDistance == bottomRightDistance) { smallestRow=0; largestRow=2; smallestCol=0; largestCol=2; }

            if (extraSplits) {
                smallestRow -= extraSplits;
                largestRow += extraSplits;
                smallestCol -= extraSplits;
                largestCol += extraSplits;
            }

            for (let j=smallestRow; j < largestRow; j++) {
                for (let i=smallestCol; i < largestCol; i++) {
                    let extent = this.TileGrid.getTileCoordExtent([zoom,column+i,row+j]);

                    let topLeft = this.TileGrid.getTileCoordForCoordAndZ([extent[0] + (width / 4), (extent[3]) - (height / 4)], zoom + 1);
                    let bottomLeft = this.TileGrid.getTileCoordForCoordAndZ([extent[0] + (width / 4), (extent[3]) - (height / 4) * 3], zoom + 1);
                    let topRight = this.TileGrid.getTileCoordForCoordAndZ([extent[0] + (width / 4) * 3, (extent[3]) - (height / 4)], zoom + 1);
                    let bottomRight = this.TileGrid.getTileCoordForCoordAndZ([extent[0] + (width / 4) * 3, (extent[3]) - (height / 4) * 3], zoom + 1);

                    this.NeededTiles.delete((row+j+1)+"_"+(column+i)+"_"+zoom);

                    this.NeededTiles.set(parseInt(topLeft[2]+1)+"_"+topLeft[1]+"_"+topLeft[0])
                    this.NeededTiles.set(parseInt(bottomLeft[2]+1)+"_"+bottomLeft[1]+"_"+bottomLeft[0])
                    this.NeededTiles.set(parseInt(topRight[2]+1)+"_"+topRight[1]+"_"+topRight[0])
                    this.NeededTiles.set(parseInt(bottomRight[2]+1)+"_"+bottomRight[1]+"_"+bottomRight[0])
                }
            }
            
            this.generateTiles(position, zoom + 1, topZoomBoundary, false, extraSplits);
        }
        else {
            for (const [key, value] of this.Tiles.entries()) {
                if (!this.NeededTiles.has(key)) {
                    this.Tiles.delete(key);
                    this.RemovedTiles.set(key, value);
                }
            }

            for (const [key,value] of this.NeededTiles.entries()) {
                if (!this.Tiles.has(key)) {
                    [row,column,zoom] = key.split('_');

                    const tile = this.generateTile(zoom,row,column)
                    if (tile) this.Tiles.set(row+"_"+column+"_"+zoom,tile);
                }
            }

            this.NeededTiles.clear();
            this.Generating = false;
        }
    }

    /**
     * Returns true if all tiles on the scenes are initialized.
     * @returns {boolean} initialized - True if all tiles are initialized.
     * @private
     * @function
     */
    allTilesInitialized() {
        for (let tile of this.Tiles.values()) {
            if (!tile.Initialized) return false;   
        }
        return true;
    }

    regenerate() {
        if (this.prevCamPos && Math.floor(this.camPos.x) == Math.floor(this.prevCamPos.x) && Math.floor(this.camPos.y) == Math.floor(this.prevCamPos.y) && Math.floor(this.camPos.z) == Math.floor(this.prevCamPos.z)) {
            if (this.CanUpdateGenPosition && !this.Generating) {
                let center = new THREE.Vector2(0,0);
                Utilities.Raycaster.setFromCamera( center, Utilities.PotreeViewer.scene.getActiveCamera() );
    
                let intersected = Utilities.Raycaster.intersectObjects(this.Models,true);
                intersected = intersected.sort((intersection1, intersection2) => (intersection1.distance > intersection2.distance) ? 1 : -1);    
    
                let vector = new THREE.Vector3();
                vector.set(center.x, center.y, 0.5);
                vector.unproject( Utilities.PotreeViewer.scene.getActiveCamera().clone() );
                vector.sub( Utilities.PotreeViewer.scene.getActiveCamera().clone().position ).normalize();
                
                let z = 0;
                if (intersected && intersected[0] && intersected[0].point.z) {
                    z = intersected[0].point.z;
                    let distance = (z - Utilities.PotreeViewer.scene.getActiveCamera().clone().position.z) / vector.z;
                    this.CurrentGenPosition = (Utilities.PotreeViewer.scene.getActiveCamera().clone().position).add( vector.multiplyScalar( distance )).toArray();
                }
                /*else if (this.PreviousGenPosition && this.PreviousGenPosition[2]) {
                    this.CurrentGenPosition = Utilities.PotreeViewer.scene.getActiveCamera().clone().position;
                    this.CurrentGenPosition.z = this.PreviousGenPosition[2];
                }*/
            }
            
            if (!this.Generating && this.allTilesInitialized()) {
                this.clearTiles();
                
                let upperBound = this.HighestZoomBoundaryGenerated;
                if ((upperBound + 1) <= this.HighestZoomLevel) upperBound += 1;

                if (!(new THREE.Vector3(this.CurrentGenPosition[0],this.CurrentGenPosition[1],this.CurrentGenPosition[2]).floor().equals(new THREE.Vector3(this.PreviousGenPosition[0],this.PreviousGenPosition[1],this.PreviousGenPosition[2]).floor()))) {
                    this.generateTerrain(this.CurrentGenPosition,this.BottomZoomBoundary,this.TopZoomBoundary);
                }
            }
        }

        this.prevCamPos = this.camPos;
    }

    /**
     * Internal function used for regenerating terrain.
     * @param {int} position - The position the tiles are generated around ([x,y,z]).
     * @param {int} bottomZoomBoundary - The lowest zoom boundary to generate from.
     * @param {int} topZoomBoundary - The highest zoom boundary to generate to.
     * @private
     * @function
     */
    generateTerrain(position, bottomZoomBoundary, topZoomBoundary, ignoreDistance) {
        this.Generating = true;
        this.PreviousGenPosition = position;

        let distanceFromPlane = Utilities.PotreeViewer.scene.getActiveCamera().position.distanceTo(new THREE.Vector3(position[0],position[1],position[2]));
        
        if (!ignoreDistance) {
            if (distanceFromPlane > 1000) topZoomBoundary = 9;
            if (distanceFromPlane < 1000) topZoomBoundary = 10;
            if (distanceFromPlane < 400) topZoomBoundary = 11;
            if (distanceFromPlane < 200) topZoomBoundary = 12;
            if (distanceFromPlane < 50) topZoomBoundary = 13;
            if (distanceFromPlane < 30) topZoomBoundary = 16;
        }

        if (topZoomBoundary > this.TopZoomBoundary) topZoomBoundary = this.TopZoomBoundary;
        
        this.HighestZoomBoundaryGenerated = topZoomBoundary;

        if (position[0] > this.TerrainBoundaries[0] && position[0] < this.TerrainBoundaries[1] && position[1] < this.TerrainBoundaries[2] && position[1] > this.TerrainBoundaries[3]) {
            this.generateTiles(position,bottomZoomBoundary,topZoomBoundary, true, 0);
        }
        else {
            this.Generating = false;
        }
    }

    /**
     * Used to get zoomLevels used by the WMS from geoserver.
     * @param {('http' || 'https')} protocol - The protocol used to access the geoserver.
     * @param {string} host - The host of the server.
     * @param {string} port - The port of the server. (usually not needed)
     * @param {string} zoomLayer - The name of the layer the grid belongs to.
     * @param {string} srs - The coordinate system of the tilegrid. (e.g. "EPSG3794")
     * 
     * @returns {Object} zoomLevels - The zoomLevels object to be provided to the configuration of the source.
     * 
     * @function
     */
    static async parseZoomLevelsFromGeoserverXML(protocol,host,port,zoomLayer, srs) {
        let zoomLevels = {};

        const response = await fetch(`${protocol}://${host}:${port}/geoserver/gwc/service/wmts?REQUEST=GetCapabilities`);
        const parser = new DOMParser();
        let xmlDoc = parser.parseFromString(await response.text(),"text/xml");

        for (let tileMatrixSet of xmlDoc.getElementsByTagName("TileMatrixSet")) {
            if (tileMatrixSet.getElementsByTagName("ows:Identifier")[0]) {
                let identifier = tileMatrixSet.getElementsByTagName("ows:Identifier")[0].textContent;

                if (identifier == srs) {
                    for (let matrix of tileMatrixSet.getElementsByTagName("TileMatrix")) {
                        let zoomLevel = matrix.getElementsByTagName("ows:Identifier")[0].textContent.split(':')[1];
                        let width = matrix.getElementsByTagName("MatrixWidth")[0].textContent;
                        let height = matrix.getElementsByTagName("MatrixHeight")[0].textContent;
                        let tileWidth = matrix.getElementsByTagName("TileWidth")[0].textContent
                        let tileHeight = matrix.getElementsByTagName("TileHeight")[0].textContent
                        let scaleDenominator = matrix.getElementsByTagName("ScaleDenominator")[0].textContent

                        zoomLevels[zoomLevel] = {
                            width: width,
                            height: height,
                            tileWidth: tileWidth,
                            tileHeight: tileHeight,
                            scaleDenominator: scaleDenominator
                        };
                    }
                }
            }
        }

        for (let layer of xmlDoc.getElementsByTagName("Layer")) {
            if (layer.getElementsByTagName("ows:Title")[0]) {
                let identifier = layer.getElementsByTagName("ows:Title")[0].textContent;

                if (identifier == zoomLayer) {
                    for (let matrix of layer.getElementsByTagName("TileMatrixLimits")) {
                        let zoomIdentifier =  matrix.getElementsByTagName("TileMatrix")[0].textContent.split(':')[0];
                        let zoomLevel = matrix.getElementsByTagName("TileMatrix")[0].textContent.split(':')[1];

                        if (zoomIdentifier == srs) {
                            let minTileRow = matrix.getElementsByTagName("MinTileRow")[0].textContent;
                            let minTileCol = matrix.getElementsByTagName("MinTileCol")[0].textContent;
                            let maxTileRow = matrix.getElementsByTagName("MaxTileRow")[0].textContent;
                            let maxTileCol = matrix.getElementsByTagName("MaxTileCol")[0].textContent;

                            zoomLevels[zoomLevel].minTileRow = minTileRow;
                            zoomLevels[zoomLevel].minTileCol = minTileCol;
                            zoomLevels[zoomLevel].maxTileRow = maxTileRow;
                            zoomLevels[zoomLevel].maxTileCol = maxTileCol;
                        }
                    }
                }
            }
        }

        return zoomLevels
    };

    /**
     * Toggles the visibility of a WMS layer.
     * @param {Object} wms - The wms object. (format: {url: '...', hidden: true/false})
     * 
     * @returns {Object} zoomLevels - The zoomLevels object to be provided to the configuration of the source.
     * 
     * @function
     */
    async toggleWMS(wms) {
        wms.hidden = !wms.hidden;
        await this.updateWMS();
    }

    /**
     * Updates tiles to reflect WMS visibility settings. (if the "hidden" property was changed)
     * @function
     */
    async updateWMS() {
        for (let tile of this.Tiles.values()) {
            tile.updateTexture();
        }
        for (let tile of this.RemovedTiles.values()) {
            tile.updateTexture();
        }
    }

    /**
     * Internal function for updating the terrain. Determines whether or not the terrain must be regenerated based on the camera position.
     * @private
     * @function
     */
    update() {
        this.camPos = Utilities.PotreeViewer.scene.getActiveCamera().position.clone();
    }

    /**
     * Removes a tile from the terrain.
     * @param {WMSTile} tile - The tile to remove.
     * @private
     * @function
     */
    removeTile(tile) {
        this.ThreeScene.remove(tile.model);
        if (this.Models.indexOf(tile.model) > -1) this.Models.splice(this.Models.indexOf(tile.model),1);
        tile.destroy();
    }

    /**
     * Returns the bounding box of the source.
     * @returns {THREE.Box3} bbox - The bounding box of the source. (made to contain every model)
     * @private
     * @function
     */
    getBbox() {
        let bbox = new THREE.Box3();

        bbox.expandByPoint(new THREE.Vector3(this.TerrainBoundaries[0], this.TerrainBoundaries[2], 0));
        bbox.expandByPoint(new THREE.Vector3(this.TerrainBoundaries[1], this.TerrainBoundaries[3], 0));

        return bbox;
    }
}