import { Utilities } from "../utilities/Utilities.js";
import { Source } from "./Source.js";

import * as THREE from '../../libs/threejs/three.module.js';

/**
 * A source used for displaying oriented images. Usually used in combination with a pointlcloud.
 * 
 * @param {Object} config - Configures the source.
 * @param {Array} config.urls - An array of urls pointing to the 360 images.
 * @param {string} config.cameraParameterUrl - A url that points to an xml file containing the details about how the camera that took the oriented images is configured.
 * @param {Object} config.scene - The Potree scene object that contains the images.
* 
 * @property {Array} Urls - The urls of the images the source contains.
 * @property {360ImageSet} ImageSets - Image set objects that contain the properties of the 3d models representing the images.
 * @property {PotreeScene} PotreeScene - The Potree scene object that contains the images.
 * @property {string} Type - A string that specifies the type of source (e.g. "GeometrySource").
 * 
 * @category Source
 * @class
 */
export class OrientedImageSource extends Source {
    
    constructor(config) {
        super();

        this.CameraParameterUrl = '';
        this.Params = null;
        this.Urls = [];

        this.ImageSets = [];
        this.PotreeScene = Utilities.PotreeViewer.scene;
        this.Type = "OrientedImageSource";
        
        this.Groups = {};

        if (config) {
            if (config.urls) this.Urls = config.urls;
            if (config.cameraParameterUrl) this.CameraParameterUrl = config.cameraParameterUrl;
            if (config.scene) this.PotreeScene = config.scene;
        }
    }

    /**
     * Internal function used for loading the source when initializing a map.
     * @private
     * @function
     */
    async load() {
        return new Promise( async(resolve, reject) => {
            this.Params = await Utilities.Potree.OrientedImageLoader.loadCameraParams(this.CameraParameterUrl);

            if (this.Urls && this.Urls.length > 0) {
                for (let url of this.Urls) {
                    await this.add(url, false);
                }
            }

            resolve();
        });

    }

    /**
     * Loads a set of images from a url.
     * @param {String} url - The url of the image set to load.
     * @returns {OrientedImageSet} images - The ImageSet object containing the orientedImages.
     * @private
     * @function
     */
    async loadImagesFromUrl(url) {
        return new Promise( (resolve, reject) => {
            Potree.OrientedImageLoader.load(this.CameraParameterUrl, url, Utilities.PotreeViewer).then( images => {
                resolve(images);
            });
        })
    }

    /**
     * Used for adding a new set of images to the source.
     * @param {String} url - The url of the image set to add.
     * @param {boolean} addToArray - True by default, if false the url will not be added to the Urls array.
     * @function
     */
    async add(input, addToArray = true) {
        if (typeof input === 'string') {
            if (this.addToArray) this.Urls.push(input);
            let images = await this.loadImagesFromUrl(input);    

            this.ImageSets.push(images);
            this.PotreeScene.addOrientedImages(images);
            Utilities.PotreeViewer.fitToScreen();
        }
    }

    /**
     * Used for removing a set of images from the source.
     * @param {OrientedImageSet} imageSet - The image set to remove.
     * @function
     */
    remove(imageSet) {
        this.PotreeScene.removeOrientedImages(imageSet);

        for (let child of imageSet.node.children) {
            child.orientedImage = undefined;
            this.PotreeScene.scene.remove(child);
        }

        if (this.ImageSets.indexOf(imageSet) > -1) {
            this.ImageSets.splice( this.ImageSets.indexOf(imageSet), 1);
        }
        
        this.PotreeScene.scene.remove(imageSet.node);
    }

    /**
     * Removes all oriented images from the scene.
     * @private
     * @function
     */
    removeFromScene() {
        for (let imageSet of this.ImageSets) {
            this.PotreeScene.removeOrientedImages(imageSet);
            this.PotreeScene.scene.remove(imageSet.node);
        }

        this.Urls = [];
        this.ImageSets = [];
    }

    /**
     * Returns the bounding box of the source.
     * @returns {THREE.Box3} bbox - The bounding box of the source. (made to contain every model)
     * @private
     * @function
     */
    getBbox() {
        let bbox = new THREE.Box3();

        for (let imageSet of this.ImageSets) {
            for (let image of imageSet.images) {
                bbox.expandByPoint(image.position);
            }
        }

        return bbox;
    }

    // group
    createImageGroup(name,fileNames) {
        if (this.Groups[name] != undefined) this.setImageGroupVisibility(name,true);

        this.Groups[name] = {
            images: [],
            visible: true
        }
        
        for (let set of this.ImageSets) {
            for (let image of set.images) {
                if (fileNames.includes(image.id)) {
                    this.Groups[name].images.push(image);
                    image.group = this.Groups[name];
                }
            }
        }

        this.setImageGroupVisibility(name,true);
    }

    setImageGroupVisibility(name,visible) {
        this.Groups[name].visible = visible;
        
        for (let image of this.Groups[name].images) {
            image.line.visible = visible;
            image.mesh.visible = visible;
            image.clickable = visible;
        }
    }
}