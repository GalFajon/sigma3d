import { Utilities } from "../utilities/Utilities.js";
import { Source } from "./Source.js";

import { Terrain } from "../terrain/Terrain.js";
import * as THREE from '../../libs/threejs/three.module.js';

/**
 * A source used for displaying terrain, generated from a texture and heightmap. Can only display a single terrain.
 * 
 * @param {Object} config - Configures the source.
 * @param {Object} config.source - An array of PotreeMeasurement objects to display.
 * @param {string} config.source.tiff - The url of the GeoTIFF file containing the heights of the terrain.
 * @param {string} config.source.texture - The url of the texture to draw over the terrain.
 * @param {int} config.tileWidth - The width of the tiles the terrain will be divided into (in pixels).
 * @param {int} config.tileHeight - The height of the tiles the terrain will be divided into (in pixels).
 * 
 * @property {Array} Heights - An array of heights, contained in the provided GeoTIFF file.
 * @property {Object} Source - The source object provided in the configuration.
 * @property {Terrain} Terrain - The Terrain object used by the source.
 * @property {Array} Dimensions - An object containing the width and height of the provided GeoTIFF.
 * @property {Array} BoundingBox - The bounding box of the provided GeoTIFF.
 * @property {THREE.Scene} ThreeScene - The THREE.Scene objects all geometries are added to.
 * @property {Array} Models - An array of the THREE models the source contains.
 * @property {string} Type - A string that specifies the type of source (e.g. "GeometrySource").
 * 
 * @category Source
 * @class
 */
export class TerrainSource extends Source {
    
    constructor(config) {
        super();

        this.Source = undefined;
        this.Terrain = undefined;

        this.Heights = [];
        this.Dimensions = { width: 0, height: 0};
        this.BoundingBox = [];

        this.Models = [];

        this.Type = "TerrainSource";
        this.ThreeScene = Utilities.PotreeViewer.scene.scene;

        if (config) {
            if (config.source) this.Source = config.source;
            if (config.tileWidth) this.TileWidth = config.tileWidth;
            if (config.tileHeight) this.TileHeight = config.tileHeight;
        }
    }

    /**
     * Internal function used for loading the source when initializing a map.
     * @private
     * @function
     */
    async load() {
        await this.add(this.Source);
    }

    /**
     * Adds a terrain to the source. If the source already has a terrain it replaces it with the new one.
     * @param {Object} source - The source object of the terrain, as specified in the constructor.
     * @function
     */
    async add(source) {
        if (this.Terrain != undefined) {
            this.remove();
        }

        this.Source = source;

        const rawTiff = await Utilities.GeoTIFF.fromUrl(source.tiff);
        const tifImage = await rawTiff.getImage();
        let image = {
            width: tifImage.getWidth(),
            height: tifImage.getHeight(),
        };

        const data = await tifImage.readRasters({ interleave: true });
        
        this.BoundingBox = tifImage.getBoundingBox();
        this.Dimensions = image;

        for (let i=0; i <= image.width; i++) {
            this.Heights.push([]);

            for (let j=0; j <= image.height; j++) {
                this.Heights[i][j] = data[i + (j * image.width)];
            }
        }

        this.Terrain = new Terrain({ 
            tiff: this.Source.tiff, 
            texture: this.Source.texture,
            tileWidth: this.TileWidth,
            tileHeight: this.TileHeight,
            heights: this.Heights,
            boundingBox: this.BoundingBox
        });

        await this.Terrain.initialize();
        
        for (let tile of this.Terrain.Tiles) this.Models.push(tile.model);
        this.ThreeScene.add(this.Terrain.model);
    }

    
    /**
     * Internal function for updating the terrain. Determines the level of detail on individual tiles.
     * @param {Array} position - The position used for calculating the level of detail of different tiles. ( [x, y, z] )
     * @private
     * @function
     */
    update(position) {
        this.Terrain.updateTileDetailAccordingToCameraDistance(new THREE.Vector3(position[0],position[1],position[2]));
    }

    /**
     * Removes the terrain from the scene.
     * @function
     */
    remove() {
        for (tile of this.Terrain.Tiles) {
            tile.Model.geometry.dispose();
            this.ThreeScene.remove(tile.Model);
        };

        this.Terrain = undefined;
    }

    /**
     * Returns the bounding box of the source.
     * @returns {THREE.Box3} bbox - The bounding box of the source. (made to contain every model)
     * @private
     * @function
     */
    getBbox() {
        let bbox = new THREE.Box3();

        for (let model of this.Models) {
            bbox.expandByObject(model);
        }

        return bbox;
    }
}