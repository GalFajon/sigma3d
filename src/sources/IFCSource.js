import { Utilities } from "../utilities/Utilities.js";
import { Source } from "./Source.js";

import { IFCOutlineMaterial, IFCSelectionMaterial } from '../utilities/Materials.js';


import * as THREE from '../../libs/threejs/three.module.js';

/**
 * A source used for displaying IFC models.
 * 
 * @param {Object} config - Configures the source.
 * @param {Array} config.urls - An array of urls pointing to ".ifc" models.
 * @param {Object} config.scene - A THREE.Scene object to which all geometries are added.
 * 
 * @property {Array} Urls - The urls of the ifc models the source contains.
 * @property {Array} Models - The urls of the ifc models the source contains.
 * @property {THREE.Scene} ThreeScene - The THREE.Scene object all geometries are added to.
 * @property {string} Type - A string that specifies the type of source (e.g. "GeometrySource").
 * 
 * @category Source
 * @class
 */
export class IFCSource extends Source {
    
    constructor(config) {
        super();

        this.Urls = [];
        
        this.Models = [];
        this.Outlines = [];

        this.Subsets = [];

        this.ThreeScene = Utilities.PotreeViewer.scene.scene;
        this.Type = "IFCSource";

        this.generateOutlines = false;
        this.correctWhites = false;

        if (config) {
            if (config.urls) this.Urls = config.urls;
            if (config.scene) this.ThreeScene = scene;
            if (config.generateOutlines != undefined) this.generateOutlines = config.generateOutlines;
            if (config.correctWhites != undefined) this.correctWhites = config.correctWhites;

        }
    }

    /**
     * Internal function used for loading the source when initializing a map.
     * @private
     * @function
     */
    async load() {
        return new Promise( async(resolve, reject) => {
            if (this.Urls && this.Urls.length > 0) {
                for (let url of this.Urls) {
                    await this.add(url, false);
                }
            }

            resolve();
        });
    }

    /**
     * Loads an IFC model from the provided URL and offsets it in 3D space.
     * @param {String} url - The url of the ".ifc" model to load.
     * @private
     * @function
     */
    async loadIFCFromUrl(url) {
        return new Promise( (resolve, reject) => {
            Utilities.IFCLoader.load( url, (model) => {
                this.offsetModel(model);
                resolve(model);
            });
        });
    }

    /**
     * Used for adding a new IFC model to the source.
     * @param {String} url - The url of the ".ifc" to add.
     * @param {boolean} addToArray - True by default, if false the url will not be added to the Urls array.
     * @function
     */
    async add(input, addToArray = true) {
        if (addToArray) this.Urls.push(input);
        let model = await this.loadIFCFromUrl(input);

        model.userData.url = input;
        
        if (this.correctWhites) {
            for (let material of model.material) {
                if (material.color.r == 1 && material.color.g == 1 && material.color.b == 1) {
                    material.color.r = 0.8;
                    material.color.g = 0.8;
                    material.color.b = 0.8;
                }
            }
        }

        this.Models.push(model); 
        this.ThreeScene.add(model);

        if (this.generateOutlines) {
            let outline = this.createOutline(model);
            this.ThreeScene.add(outline);
            this.Outlines.push(outline);
        }

        const customEvent = new CustomEvent('added', { detail: { source: this, model: model } });
        this.dispatchEvent(customEvent);
    }

    createOutline(model) {
        let geo = new THREE.EdgesGeometry( model.geometry );
        let outline = new THREE.LineSegments( geo, IFCOutlineMaterial );

        outline.rotation.x = Math.PI / 2;
        outline.position.copy(model.position);

        return outline;
    }

    /**
     * Removes an IFC model from the source.
     * @param {THREE.Mesh} model - The ifc model to remove.
     * @function
     */
    remove(model) {
        model.geometry.dispose();

        for (let material of model.material) {
            material.dispose();
        }

        this.ThreeScene.remove(model);

        if (this.Models.indexOf(model) > -1){
            this.Models.splice( this.Models.indexOf(model), 1);
        }

        if (model.userData.url && this.Urls.indexOf(model.userData.url) > -1) {
            this.Urls.splice(this.Urls.indexOf(model.userData.url), 1);
        }

        const customEvent = new CustomEvent('removed', { detail: { source: this, model: model } });
        this.dispatchEvent(customEvent);
    }

    /**
     * Removes all IFC models from the scene.
     * @private
     * @function
     */
    removeFromScene() {
        for (let model of this.Models) {
            model.geometry.dispose();

            for (let material of model.material) {
                material.dispose();
            }

            this.ThreeScene.remove(model);
        }

        this.Models = [];
        this.Urls = [];
    }

    /**
     * Offsets the IFC model to its actual coordinates in 3D space.
     * @private
     * @function
     */
    offsetModel(model) {
        let center = new THREE.Vector3();

        model.geometry.computeBoundingBox();
        model.geometry.boundingBox.getCenter(center);        
        model.geometry.center();
        model.position.set(center.x, -center.z, center.y);

        model.rotation.x = Math.PI / 2;
    }

    offsetSubset(subset,model) {
        subset.position.copy(model.position);
        subset.rotation.x = Math.PI / 2;
    }

    /**
     * Returns the bounding box of the source.
     * @returns {THREE.Box3} bbox - The bounding box of the source. (made to contain every model)
     * @private
     * @function
     */
    getBbox() {
        let bbox = new THREE.Box3();

        for (let model of this.Models) {
            bbox.expandByObject(model);
        }

        return bbox;
    }

    async getPropertySet(subset,set) {
        const props = await Utilities.IFCLoader.ifcManager.getPropertySets(subset.model.modelID,subset.properties.expressID,true);
        for (let set2 of props) {
            if (set2.Name.value == set) {
                return set2.HasProperties.map( (prop) => prop.Name.value);
            }
        }
    }

    async getProperty(subset,set,name) {
        const props = await Utilities.IFCLoader.ifcManager.getPropertySets(subset.model.modelID,subset.properties.expressID,true);

        for (let set2 of props) {
            if (set2.Name.value == set) {
                for (let property of set2.HasProperties) {
                    if (property.Name.value == name) {
                        return property.NominalValue.value;
                    }
                }
            }
        }
    }

    async getHierarchy() {
        let props = [];

        for (let model of this.Models) {
            props.push({ modelId: model.modelID, hierarchy: await Utilities.IFCLoader.ifcManager.getSpatialStructure(model.modelID, true)});
        }

        return props;
    }

    createSubset(model, ids) {
        let subset = Utilities.IFCLoader.ifcManager.createSubset({
            modelID: model.modelID,
            scene: Utilities.PotreeViewer.scene.scene,
            ids: ids,
            removePrevious: true,
        });

        this.offsetSubset(subset,model);

        if (this.Models.indexOf(model) > -1) { 
            if (this.createOutline) {
                let outline = this.createOutline(subset);

                this.ThreeScene.remove(this.Outlines[this.Models.indexOf(model)]);

                this.Outlines.splice(this.Models.indexOf(model), 1);
                this.Outlines.push(outline);

                this.ThreeScene.add(outline);
            }

            this.Models.splice(this.Models.indexOf(model),1);
            this.Models.push(subset);
        }

        Utilities.PotreeViewer.scene.scene.remove(model);
        Utilities.PotreeViewer.scene.scene.add(subset);

    }

}