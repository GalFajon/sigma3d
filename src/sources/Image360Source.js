import { Utilities } from "../utilities/Utilities.js";
import { Source } from "./Source.js";

import * as THREE from '../../libs/threejs/three.module.js';

/**
 * A source used for displaying 360 degree images (street view spheres). Usually used in combination with a pointlcloud.
 * 
 * @param {Object} config - Configures the source.
 * @param {Array} config.urls - An array of urls pointing to the 360 images.
 * @param {Object} config.scene - The Potree scene object that contains the spheres.
 * 
 * @property {Array} Urls - The urls of the 360 degree images the source contains.
 * @property {Array} Models - The urls of the ifc models the source contains.
 * @property {360ImageSet} ImageSets - Image set objects that contain the properties of the 3d models representing the spheres.
 * @property {PotreeScene} PotreeScene - The Potree scene object that contains the spheres.
 * @property {Image360} FocusedImage - The image currently being viewed by the user.
 * @property {string} Type - A string that specifies the type of source (e.g. "GeometrySource").
 * 
 * @category Source
 * @class
 */
export class Image360Source extends Source {
    
    constructor(config) {
        super();

        this.Urls = [];
        this.ImageSets = [];
        this.PotreeScene = Utilities.PotreeViewer.scene;
        this.Type = "Image360Source";
        
        if (config) {
            if (config.urls) this.Urls = config.urls;
            if (config.scene) this.PotreeScene = config.scene;
        }

        this.handleFocused = this.handleFocused.bind(this);
        this.handleUnfocused = this.handleUnfocused.bind(this);

        this.FocusedImage = undefined;
        this.Models = [];

        Utilities.PotreeViewer.addEventListener('360image_focused', this.handleFocused);
        Utilities.PotreeViewer.addEventListener('360image_unfocused', this.handleUnfocused);
    }

    /**
     * Triggered when an image is focused, used to swap between controls and store the focused image.
     * @private
     * @function
     */
    handleFocused(e) {
        if (this.DefaultControls == undefined) this.DefaultControls = Utilities.PotreeViewer.controls;
        this.FocusedImage = e.detail.image;
    }

    /**
     * Triggered when an image is unfocused, used to swap between controls and store the focused image.
     * @private
     * @function
     */
    handleUnfocused(e) {
        if (this.DefaultControls) Utilities.PotreeViewer.setControls(this.DefaultControls);
        this.FocusedImage = undefined;
    }

    /**
     * Internal function used for loading the source when initializing a map.
     * @private
     * @function
     */
    async load() {
        return new Promise( async(resolve, reject) => {
            if (this.Urls && this.Urls.length > 0) {
                for (let url of this.Urls) {
                    await this.add(url, false);
                }
            }

            resolve();
        });
    }

    /**
     * Loads a set of 360 images from the provided URL.
     * @param {String} url - The url of the image set to load.
     * @returns {Image360Set} images - The ImageSet object containing the 360 images.
     * @private
     * @function
     */
    async loadImagesFromUrl(url) {
        return new Promise( (resolve, reject) => {
            let loader = Utilities.Potree.Images360Loader.load( url, Utilities.PotreeViewer ).then( images => {
                resolve(images);
            });
        })
    }

    /**
     * Used for adding a new set of images to the source.
     * @param {String} url - The url of the image set to add.
     * @param {boolean} addToArray - True by default, if false the url will not be added to the Urls array.
     * @function
     */
    async add(input, addToArray = true) {
        if (typeof input === 'string') {
            if (this.addToArray) this.Urls.push(input);
            let images = await this.loadImagesFromUrl(input);    
            
            this.ImageSets.push(images);
            this.PotreeScene.add360Images(images);

            for (let image of images.images) {
                this.Models.push(image.mesh);
            }

            Utilities.PotreeViewer.fitToScreen();
        }
    }

    /**
     * Used for removing a set of images from the source.
     * @param {360ImageSet} imageSet - The image set to remove.
     * @function
     */
    remove(imageSet) {
        for (let image of imageSet.images) {
            if (this.Models.indexOf(image.mesh) > -1) this.Models.splice(this.Models.indexOf(image.mesh),1);
        }

        this.PotreeScene.remove360Images(imageSet);
        imageSet.selectingEnabled = false;
        this.PotreeScene.scene.remove(imageSet.node);

        if (this.ImageSets.indexOf(imageSet) > -1) {
            this.ImageSets.splice( this.ImageSets.indexOf(imageSet), 1);
        }
    }

    /**
     * Removes all 360 images from the scene.
     * @private
     * @function
     */
    removeFromScene() {
        for (let imageSet of this.ImageSets) {
            this.PotreeScene.remove360Images(imageSet);
            imageSet.selectingEnabled = false;
            this.PotreeScene.scene.remove(imageSet.node);
        }

        this.Models = [];
        this.Urls = [];
        this.ImageSets = [];
    }

    /**
     * Returns the bounding box of the source.
     * @returns {THREE.Box3} bbox - The bounding box of the source. (made to contain every model)
     * @private
     * @function
     */
    getBbox() {
        let bbox = new THREE.Box3();

        for (let model of this.Models) {
            bbox.expandByPoint(model.position);
        }

        return bbox;
    }


}