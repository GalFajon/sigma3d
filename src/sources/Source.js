import { EventDispatcher } from "../utilities/EventDispatcher.js";

/**
 * The class all other sources inherit from. Not to be used by itself.
 * 
 * @property {Array} Models - An array of THREE.Mesh objects tied to the things the source contains.
 * @category Source
 * @class
 */
export class Source extends EventDispatcher {
    constructor() {
        super();
        this.Models = [];
    }
}