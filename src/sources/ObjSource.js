 import { Utilities } from "../utilities/Utilities.js";
 import { Source } from "./Source.js";
 import * as THREE from '../../libs/threejs/three.module.js';
 
 export class ObjSource extends Source {
     
     constructor(config) {
         super();
 
         this.Models = [];
         this.ThreeScene = Utilities.PotreeViewer.scene.scene;

         this.Type = "ObjSource";
         this.Urls = [];
         
         if (config) {
             if (config.urls) this.Urls = config.urls;
             if (config.scene) this.ThreeScene = scene;
         }
     }
 
     async load() {
        for (let url of this.Urls) await this.add(url, false)
     }

     async add(url, addToArray = true) {
        return new Promise((resolve,reject) => {
            if (url.mtl) {
                Utilities.MTLLoader.setMaterialOptions({ side: THREE.DoubleSide })
                Utilities.MTLLoader.load(url.mtl, function (materials) {
                    materials.preload();

                    Utilities.OBJLoader.setMaterials(materials);
                    Utilities.OBJLoader.load(url.obj,
                        function ( object ) {
                            for (let model of object.children) {
                                this.Models.push(model);
                                this.moveToLocal(model);
                            }

                            this.ThreeScene.add(object);
                            if (addToArray) this.Urls.push(url);
                            resolve();
                        }.bind(this),
                        function ( xhr ) {},
                        function ( error ) { console.log( 'Error loading obj.' ); reject(); }
                    );
                }.bind(this))
            }
            else {
                Utilities.OBJLoader.load(url.obj,
                    function ( object ) {
                        for (let model of object.children) {
                            this.Models.push(model);
                            this.moveToLocal(model);
                        }

                        this.ThreeScene.add(object);
                        if (addToArray) this.Urls.push(url);
                        resolve();
                    }.bind(this),
                    function ( xhr ) {},
                    function ( error ) { console.log( 'Error loading obj.' ); reject(); }
                );
            }
        })
     }

     moveToLocal(object) {
        let newPositions = new Float32Array(object.geometry.attributes.position.array.length);
        
        let center = new THREE.Vector3();
        let bbox = new THREE.Box3().setFromObject(object);
        
        bbox.getCenter(center);
        
        let first = [];

        for (let i=0; i < object.geometry.attributes.position.array.length; i+=3) {
            if (i == 0) {
                first.push(
                    object.geometry.attributes.position.array[i],
                    object.geometry.attributes.position.array[i+1],
                    object.geometry.attributes.position.array[i+2]
                )

                newPositions[i] = 0;
                newPositions[i+1] = 0;
                newPositions[i+2] = 0;
            }
            else {
                newPositions[i] = object.geometry.attributes.position.array[i] - first[0];
                newPositions[i+1] = object.geometry.attributes.position.array[i+1] - first[1];
                newPositions[i+2] = object.geometry.attributes.position.array[i+2] - first[2];
            }
        }

        object.position.set(...first);

        object.geometry.setAttribute('position', new THREE.BufferAttribute( newPositions, 3));
        object.geometry.attributes.position.needsUpdate = true;
        object.geometry.computeBoundingBox();
        object.geometry.computeBoundingSphere();
     }
     
     remove(model) {
        this.ThreeScene.remove(model);

        if (this.Models.indexOf(model) > -1) {
            this.Models.splice( this.Models.indexOf(model), 1);
        }
     }
 
     removeFromScene() {
        for (let model of this.Models) this.ThreeScene.remove(model);
        this.Models = [];
     }

     getBbox() {
         let bbox = new THREE.Box3();

         for (let model of this.Models) bbox.expandByObject(model);

         return bbox;
     }
 }