import * as THREE from '../libs/threejs/three.module.js';
import {Utilities} from '../src/utilities/Utilities.js'

/**
 * Determines how the user views a map.
 * 
 * @param {Object} config - Configures the view.
 * @param {Array} config.position - Sets the initial position of the view (e.g. [0, 0, 0] or [3, 3, 3]). [0,0,0] by default.
 * @param {Sigma3D.DeviceOrientationControls | Sigma3D.EarthControls | Sigma3D.VRControls | Sigma3D.OrbitControls | Sigma3D.noCloudControls} config.controls - Decides what controls are used by the view.
 * 
 * @property {Array} Position - Contains the current position of the view ([x,y,z]). 
 * @example
 * await Sigma3D.initialize();
 *			
 * let map = new Sigma3D.Map({
 *      view: new Sigma3D.View({ position: [3,3,3], controls: Sigma3D.OrbitControls }),
 * });
 *
 * Sigma3D.setMap(map);
 * 
 * @category Viewer
 * 
 * @class
 */

export class View {
    constructor( config ) {
        this.Position = [0,0,0];      

        if (config) {
            if (config.position) this.Position = config.position;
            if (config.controls) {
                this.Controls = config.controls;
                Utilities.PotreeViewer.setControls( config.controls );
            }
        }
    }
    
    /**
     * Centers the view and its controls on a model (a Three.js object).
     * @param {THREE.Object3D} object - The object to zoom to.
     * @function
     */

    zoomToObject(object) {
        Utilities.PotreeViewer.zoomTo(object, 1, 0);
        this.Position = [object.position.x, object.position.y, object.position.z];

        Utilities.PotreeViewer.controls.stop();
    }

    /**
     * Centers the view and its controls on a set of coordinates.
     * @param {Array} position - The coordinates to zoom to. ([x,y,z])
     * @function
     */
    zoomToPosition( [x,y,z] ) {
        let pivot = new THREE.AxesHelper(6);
        pivot.position.set(x,y,z);
        this.Position = [x,y,z];

        Utilities.PotreeViewer.zoomTo(pivot, 1, 0);
        Utilities.PotreeViewer.controls.stop();
    }

    /**
     * Moves the view to a set of coordinates.
     * @param {Array} position - The coordinates to zoom to. ([x,y,z])
     * @function
     */
    goToPosition([x,y,z]) {
        Utilities.PotreeViewer.scene.view.position.set(x,y,z);
        Utilities.PotreeViewer.scene.view.lookAt(x,y,z - 10);
    }

    /**
     * Centers the view based on the bounding box of the entire scene.
     * @function
     */
    center(bbox) {
        if (bbox.min.x != Infinity && bbox.min.y != Infinity && bbox.min.z != Infinity && bbox.max.x != -Infinity && bbox.max.y != -Infinity && bbox.max.z != Infinity) {
            let center = new THREE.Vector3( bbox.min.x + (bbox.max.x - bbox.min.x) / 2, bbox.min.y + (bbox.max.y - bbox.min.y) / 2, bbox.min.z + (bbox.max.z - bbox.min.z) / 2 );
            this.Position = [center.x, center.y, center.z];

            const geometry = new THREE.BoxGeometry( bbox.max.x - bbox.min.x, bbox.max.y - bbox.min.y, bbox.max.z - bbox.min.z );
            const material = new THREE.MeshBasicMaterial( {color: 0x00ff00, wireframe:true} );
            const cube = new THREE.Mesh( geometry, material );

            cube.position.copy(center);        

            Utilities.PotreeViewer.zoomTo(cube, 1, 0);        
            Utilities.PotreeViewer.controls.stop();
            
            cube.geometry.dispose();
            cube.material.dispose();
        }
        else {
            this.zoomToPosition([0,0,0]);
        }
    }

    /**
     * Updates the Position property of the view based on the position of the camera in 3d space.
     * @private
     * @function
     */
    update(position) {
        this.Position = Utilities.PotreeViewer.scene.getActiveCamera().position.toArray();
    }
}