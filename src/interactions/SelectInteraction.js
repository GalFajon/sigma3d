import { Interaction } from './Interaction.js';

import { Utilities } from '../utilities/Utilities.js';
import { IFCSelectionMaterial, MeshSelectionMaterial } from '../utilities/Materials.js';

import * as THREE from '../../libs/threejs/three.module.js';

/**
 * The interaction used for selecting (returning the information of and visually highlighting) the element that has been clicked on a particular source.
 * 
 * @param {Object} config - Configures the interaction.
 * @param {Sigma3D.GeometrySource | Sigma3D.IFCSource | Sigma3D.PotreeMeasurementSource} config.source - The source to which the interaction is bound.
 * 
 * @property {THREE.Raycaster} Raycaster - An instance of THREE.Raycaster used for getting mouse position.
 * @property {Sigma3D.Source} ParentSource - The source to which the interaction is assigned. Interaction behavior changes depending on source.
 * @property {boolean} MovedMouse - Indicates whether or not the user moved the mouse between pressing the mouse down and up. (true means the user dragged the mouse, false means the user clicked without moving)
 * @property {Sigma3D.Geometry | IFCsubset} SelectedObject - The most recently selected object.
 * @property {string} Type - A string indicating the interaction type.
 * 
 * @fires SelectInteraction#selected
 * 
 * @example
 *			await Sigma3D.initialize();
 *
 *			let ifcSource = new Sigma3D.IFCSource({
 *               urls:  [
 *                   'https://projekti.lgb.si/WebPointCloud/IFC/20211110_Marina_Garage_RTC360/Marina_garage_pipes_DX460000_DY100000_DZ300.ifc'
 *               ]
 *           });
 *
 *           let geomSource = new Sigma3D.GeometrySource({
 *               geometries: [
 *                   new Sigma3D.Point( [0,0,0] ),
 *                   new Sigma3D.Point( [0,4,0] ),
 *                   new Sigma3D.Line([ [0,0,0], [0,4,0] ]),
 *                   new Sigma3D.Polygon(
 *                       [[
 *                           [0,0,0], [4,0,0], [4,5,0], [0,4,0]
 *                       ]]
 *                   )
 *               ]
 *           })
 *
 *           let ifcSelectInteraction = new Sigma3D.SelectInteraction({
 *              source:ifcSource
 *           });
 *
 *           let geomSelectInteraction = new Sigma3D.SelectInteraction({
 *               source:geomSource
 *           });
 *			
 *           let map = new Sigma3D.Map({
 *               view: new Sigma3D.View({}),
 *
 *               layers: [
 *                   new Sigma3D.GeometryLayer({
 *                       source: geomSource
 *                   }),
 *                   new Sigma3D.IFCLayer({
 *						source: ifcSource
 *					 }),
 *               ],
 *
 *               interactions: [
 *                   ifcSelectInteraction,
 *                   geomSelectInteraction
 *               ]
 *           });
 *
 *           ifcSelectInteraction.addEventListener('selected', (event) => {
 *               console.log(event.detail);
 *           });
 *
 *           geomSelectInteraction.addEventListener('selected', (event) => {
 *               console.log(event.detail);
 *           });
 *           
 *           Sigma3D.setMap(map);
 * 
 * @category Interaction
 * @class SelectInteraction
 */

/**
 * @event SelectInteraction#selected
 * @returns {Object} event.detail - Returns whatever measurement or geometry was just selected.
 */

export class SelectInteraction extends Interaction {
    
    constructor(config) {
        super(config);

        this.RayCaster = Utilities.Raycaster;
        this.Highlight = true;
        
        if (config) {
            if (config.source) this.ParentSource = config.source;
            if (config.button) this.Button = config.button;
            else this.Button = 0;
            if (config.highlight !== undefined) this.Highlight = config.highlight;
        }
        
        this.MovedMouse = false;
        this.SelectedObject = undefined;

        this.Type = "SelectInteraction";
        this.Active = true;
    }

    /**
     * Adds the event listeners.
     * @private
     * @function
     */
    addEventListeners() {
        if (this.ParentSource.Type == 'GeometrySource') {
            this.DomElement.addEventListener('pointerup', this.handleGeometrySourcePointerUp);
        }
        else if (this.ParentSource.Type == 'IFCSource') {
            this.DomElement.addEventListener('pointerup', this.handleIFCSourcePointerUp);
        }
        else if (this.ParentSource.Type == 'PotreeMeasurementSource') {
            this.DomElement.addEventListener('pointerup', this.handlePotreeMeasurementSourcePointerUp);
        }

        else {
            console.warn("The assigned source" + this.ParentSource.Type + " is incompatible with the interaction" + this.Type + ".")
        }
    }

    /**
     * Initializes the interaction.
     * @private
     * @function
     */
    async initialize() {
        this.handleGeometrySourcePointerUp = this.handleGeometrySourcePointerUp.bind(this);
        this.handleIFCSourcePointerUp = this.handleIFCSourcePointerUp.bind(this);
        this.handlePotreeMeasurementSourcePointerUp = this.handlePotreeMeasurementSourcePointerUp.bind(this);

        this.addEventListeners();
    }

    /**
     * Determines whether or not the interaction uses its event listeners.
     * @private
     * @function
     */
    setActive(active) {
        if (active == true) { this.Active = true;}
        else { 
            if (this.Highlight) this.ParentSource.removeGeometryHighlight(this.SelectedObject);
            this.SelectedObject = undefined; 
            this.Active = false; 
        }
    }
    
    /**
     * Handles the "pointerup" event for the corresponding source type.
     * @private
     * @function
     */
    handleGeometrySourcePointerUp(event) {    
        if (this.Active) {            
            if (event.button == this.Button) {
                if (Utilities.Cursor3D.MovedMouse == false) {
                    let intersect = this.getMouseIntersect(event);
                    
                    if (this.SelectedObject) {
                        if (this.Highlight) this.ParentSource.removeGeometryHighlight(this.SelectedObject);
                    }

                    if (intersect) {
                        this.SelectedObject = intersect.object.userData;
                        if (this.Highlight) this.ParentSource.highlightGeometry(this.SelectedObject);
                        const customEvent = new CustomEvent('selected', { detail: { Geometry: this.SelectedObject, ClickPosition: intersect.point.toArray() } });
                        this.dispatchEvent(customEvent); 
                    }
                    else {
                        if (Utilities.Cursor3D.SnappedObject && this.ParentSource.Geometries.includes(Utilities.Cursor3D.SnappedObject)) {
                            this.SelectedObject = Utilities.Cursor3D.SnappedObject
                            if (this.Highlight) this.ParentSource.highlightGeometry(this.SelectedObject);
                            const customEvent = new CustomEvent('selected', { detail: { Geometry: this.SelectedObject, ClickPosition: Utilities.Cursor3D.Position } });
                            this.dispatchEvent(customEvent); 
                        }
                    }
                }
            }
        }
    }

    /**
     * Handles the "pointerup" event for the corresponding source type.
     * @private
     * @function
     */
    async handleIFCSourcePointerUp(event) {
        if (this.Active) {
            if (event.button == this.Button) {
                if (Utilities.Cursor3D.MovedMouse == false) {
                    let intersect = this.getMouseIntersect(event, false);

                    if (intersect) {
                        let id = Utilities.IFCLoader.ifcManager.getExpressId( intersect.object.geometry, intersect.faceIndex );
                        let modelID = intersect.object.modelID;
                        let props = await Utilities.IFCLoader.ifcManager.getItemProperties( modelID, id, true );

                        if (!this.prevID || this.prevID != id) {
                            if (this.Highlight) {
                                let scene = Utilities.PotreeViewer.scene.scene;
                                let subset = Utilities.IFCLoader.ifcManager.createSubset( { modelID, ids: [ id ], scene, removePrevious: true, customID: 3 } );
                                
                                subset.material = IFCSelectionMaterial;

                                this.ParentSource.offsetSubset(subset, intersect.object);
                            }
                        }

                        this.SelectedObject = {
                            properties: props
                        }

                        this.prevID = id;
                        const customEvent = new CustomEvent('selected', { detail: { IFCSubset: this.SelectedObject,  ClickPosition: intersect.point.toArray() } });
                        this.dispatchEvent(customEvent);
                    }
                }
            }
        }
    }

    /**
     * Handles the "pointerup" event for the corresponding source type.
     * @private
     * @function
     */
    handlePotreeMeasurementSourcePointerUp(event) {
        if (this.Active) {
            if (event.button == this.Button) {
                if (Utilities.Cursor3D.MovedMouse == false) {
                    let intersect = this.getMouseIntersect(event);

                    if (intersect) {
                        if (this.SelectedObject && this.SelectedObject.spheres) {
                            for (let sphere of this.SelectedObject.spheres) {
                                if (this.Highlight) sphere.material.emissive.setHex(0x000000);
                            }
                        }

                        this.SelectedObject = intersect.object.parent;
                        
                        if (this.Highlight) if (intersect.object.material.emissive) intersect.object.material.emissive.setHex(0x00FF00);

                        const customEvent = new CustomEvent('selected', { detail: { PotreeMeasurement: this.SelectedObject, ClickPosition: intersect.point.toArray() } });
                        this.dispatchEvent(customEvent);
                    }
                }
            }
        }
    }

    /**
     * Gets models clicked by the mouse on the parent source.
     * @private
     * @function
     */
    getMouseIntersect(event, intersectChildren = true) {
        let intersects = [];
        let mouseCoords = new THREE.Vector2(
            ( event.layerX / this.DomElement.getBoundingClientRect().width) * 2 - 1, // x
            -( event.layerY / this.DomElement.getBoundingClientRect().height ) * 2 + 1 // y
        );

        let cursorPos = new THREE.Vector3(...Utilities.Cursor3D.Position);

        this.RayCaster.setFromCamera(mouseCoords, Utilities.PotreeViewer.scene.getActiveCamera());
        
        if (this.ParentSource.Models) {
            intersects.push(...this.RayCaster.intersectObjects( this.ParentSource.Models, intersectChildren ));
        }
        if (this.ParentSource.PointsCloud) {
            if (this.ParentSource.PointsCloud) for (const [key, value] of this.ParentSource.PointsCloud) {
                let pointIntersects = this.RayCaster.intersectObject(value, true);
                for (let intersect of pointIntersects) {
                    intersects.push({ object: { userData: this.ParentSource.Points.get(key)[intersect.index] }, point: intersect.point, distance: intersect.distance });
                }
            }
        }

        intersects.sort((first, second) => (cursorPos.distanceTo(new THREE.Vector3(...first.object.userData.Vectors[0])) > cursorPos.distanceTo(new THREE.Vector3(...second.object.userData.Vectors[0]))) ? 1 : -1)
        
        if (intersects.length > 0) return intersects[0];
    }

}