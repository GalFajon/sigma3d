import {Utilities} from '../utilities/Utilities.js'
import { EventDispatcher } from '../utilities/EventDispatcher.js'
import * as THREE from '../../libs/threejs/three.module.js';

/**
 * The class all other interaction types inherit from. Not to be used by itself.
 * 
 * @property {Sigma3D.Source} ParentSource - The source to which the interaction is assigned. Interaction behavior changes depending on source.
 * @property {DOMelement} DomElement - The DOM element used by the PotreeViewer renderer. Used to receive events for user interactions.
 * 
 * @category Interaction
 * @class
 */

export class Interaction extends EventDispatcher {
    
    constructor() {
        super();
        
        this.ParentSource = undefined; // This is assigned by the user when adding the interaction
        this.DomElement = Utilities.PotreeViewer.renderer.domElement; // The element from which we receive user events
    }
}