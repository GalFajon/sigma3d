import { Interaction } from './Interaction.js'
import { SnapLine } from './helpers/SnapLine.js'
import { Utilities } from '../utilities/Utilities.js';
import { SnapPointMaterial } from '../utilities/Materials.js';

import * as THREE from '../../libs/threejs/three.module.js';

/**
 * An interaction that snaps the mouse to the models of a given source.
 * 
 * @param {Object} config - Configures the interaction.
 * @param {Signa3D.GeometrySource | Sigma3D.PotreeMeasurementSource | Sigma3D.PointcloudSource} config.source - The parent source of the interaction.
 * 
 * @property {Sigma3D.SnapLine} SnapLines - An array of lines used for snapping to line-like objects (such as Line geometries).
 * @property {Sigma3D.SnapPoints} SnapPoints - An array of points used for snapping to specific coordinates (such as Point geometries or the vertices of IFC models).
 * @property {THREE.Mesh} Models - An array of models used for snapping.
 * @property {DOMelement} DomElement - The DOM element used by the PotreeViewer renderer. Used to receive events for user interactions.
 * @property {THREE.Raycaster} RayCaster - An instance of THREE.Raycaster used for getting mouse position.
 * @property {string} Type - A string indicating the interaction type.
 * 
 * @example
 * await Sigma3D.initialize();
 * 
 *   let geomSource = new Sigma3D.GeometrySource({
 *       geometries: [
 *           new Sigma3D.Point( [1,1,1] ),
 *           new Sigma3D.Point( [1,4,1] ),
 *           new Sigma3D.Line([ [1,1,1], [1,4,1] ])
 *       ]
 *   })
 *
 *   let ifcSource = new Sigma3D.IFCSource({
 *       urls:  [
 *           'https://projekti.lgb.si/WebPointCloud/IFC/20211110_Marina_Garage_RTC360/Marina_garage_pipes_DX460000_DY100000_DZ300.ifc'
 *       ]
 *   });
 *
 *   let ptcldSource = new Sigma3D.PointcloudSource({
 *       urls:  [
 *           'https://projekti.lgb.si/WebPointCloud/RawData/DataConverted/20211110_Marina_Garage_RTC360/metadata.json'
 *       ]
 *   });
 *
 *   let interaction2 = new Sigma3D.SnapInteraction({
 *       source: geomSource
 *   });
 *
 *   let map = new Sigma3D.Map({
 *       view: new Sigma3D.View({}),
 *
 *       layers: [
 *           new Sigma3D.GeometryLayer({
 *               source: geomSource
 *           }),
 *       ],
 *
 *       interactions: [
 *           interaction2
 *       ]
 *   });
 *
 *   Sigma3D.setMap(map);
 * 
 * @category Interaction
 * @class SnapInteraction
 */

export class SnapInteraction extends Interaction {
    constructor(config) {
        super(config);

        this.ParentSources = [];

        if (config) {
            if (config.sources) this.ParentSources = config.sources;
            if (config.target) this.Target = config.target;
        }

        this.SnapPoints = [];
        this.SnapPointCloud = new THREE.Points(new THREE.BufferGeometry(), SnapPointMaterial);
        this.SnapPotreePointclouds = [];
        this.SnapLines = [];

        this.Models = [];

        this.DomElement = Utilities.PotreeViewer.renderer.domElement;

        this.Type = "SnapInteraction";
        this.Active = true;
    }

    /**
     * Initializes the interaction.
     * @private
     * @function
     */
    initialize() {        
        this.generateSnaps2 = this.generateSnaps.bind(this);
        Utilities.PotreeViewer.scene.scene.add(this.SnapPointCloud);

        for (let source of this.ParentSources) {

            if (source.Type == 'GeometrySource') {
                source.addEventListener('modifyend', this.generateSnaps2);
                source.addEventListener('added', this.generateSnaps2);
                source.addEventListener('removed', this.generateSnaps2);
            }
            else if (source.Type == 'PotreeMeasurementSource') {
                source.addEventListener('modifyend', this.generateSnaps2);
                source.addEventListener('added', this.generateSnaps2);
                source.addEventListener('removed', this.generateSnaps2);
            }
        }

        this.generateSnaps();
    }

    /**
     * Removes event handlers for when the interaction is taken off a map.
     * @private
     * @function
     */
    remove() {
        this.clearSnapPoints();
        this.clearSnapLines();     
    }
    
    /**
     * Determines whether or not the interaction uses its event listeners.
     * @private
     * @function
     */
     setActive(active) {
        if (active == true) { this.Active = true; this.generateSnaps(); }
        else { this.Active = false; this.remove(); }
    }

    /**
     * Generates SnapPoints and SnapLines for models on the corresponding source type.
     * @private
     * @function
     */
    generateSnaps() {
        let measurements = [];
        let volumes = [];
        let heightProfiles = [];

        this.clearSnapLines();
        this.clearSnapPoints();

        let geometriesToSnapTo = [];

        if (this.Target) geometriesToSnapTo.push(this.Target);
        else {
            for (let source of this.ParentSources) {
                if (source.Type == 'GeometrySource') geometriesToSnapTo.push(...source.Geometries);
                if (source.Type == 'PotreeMeasurementSource') {
                    measurements.push(...source.Measurements);
                    volumes.push(...source.Volumes);
                    heightProfiles.push(...source.HeightProfiles);
                }
            }
        }

        for (let geometry of geometriesToSnapTo) {
            if (geometry.Type == 'Point' || geometry.Type == 'HeightPoint') this.createSnapPoint(geometry.Vectors[0], geometry)
            if (geometry.Type == 'Polygon') {
                for (let vector of geometry.Vectors) {
                    this.createSnapPoint(vector, geometry);
                }

                for (let hole of geometry.Holes) {
                    this.createSnapLine([...hole, hole[0]], geometry);
                    for(let holePosition of hole) {
                        this.createSnapPoint(holePosition, geometry);
                    }
                }
                
                this.createSnapLine([...geometry.Vectors, geometry.Vectors[0]], geometry);
            }
            if (geometry.Type == 'Line') {
                this.createSnapLine(geometry.Vectors, geometry);
                for (let vector of geometry.Vectors) {
                    this.createSnapPoint(vector, geometry);
                }
            }
        }

        for (let measurement of measurements) {
            let positions = [];

            for (let sphere of measurement.spheres) {
                positions.push([sphere.position.x, sphere.position.y, sphere.position.z]);
                this.createSnapPoint( [sphere.position.x, sphere.position.y, sphere.position.z], sphere);
            }
        }
        for (let volume of volumes) this.createSnapPoint( [volume.position.x, volume.position.y, volume.position.z], volume);
        for (let profile of heightProfiles) {
            let positions = [];

            for (let sphere of profile.spheres) {
                positions.push([sphere.position.x, sphere.position.y, sphere.position.z]);
                this.createSnapPoint( [sphere.position.x, sphere.position.y, sphere.position.z], sphere);
            }
        }

        this.updateSnapPointHelper();
    }

    /**
     * Removes all SnapPoints.
     * @private
     * @function
     */
    clearSnapPoints() {
        this.SnapPointCloud.geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( [], 3 ) );
        this.SnapPointCloud.geometry.setDrawRange( 0, 0 );
        this.SnapPointCloud.geometry.verticesNeedUpdate = true;
        this.SnapPointCloud.geometry.computeBoundingSphere();
        this.SnapPoints = [];
    }

    /**
     * Removes all SnapLines.
     * @private
     * @function
     */
    clearSnapLines() {
        for (let snapLine of this.SnapLines) {
            snapLine.removeFromScene();
            if (this.Models.indexOf(snapLine.model) > -1) this.Models.splice(this.Models.indexOf(snapLine.model), 1);
        }

        this.SnapLines = [];
    }

    /**
     * Creates a SnapPoint at the given coordinates.
     * @private
     * @function
     */
    createSnapPoint([x,y,z], referencedObject) {
        this.SnapPoints.push({ Coordinates: [x,y,z], RefersTo: referencedObject });
    }

    updateSnapPointHelper() {
        if (this.SnapPoints[0] && this.SnapPoints[0].Coordinates) {
            let root = this.SnapPoints[0].Coordinates;
            let flat = [];
            for (let point of this.SnapPoints) flat.push( point.Coordinates[0] - root[0], point.Coordinates[1] - root[1], point.Coordinates[2] - root[2] );

            this.SnapPointCloud.geometry.setAttribute( 'position', new THREE.Float32BufferAttribute(flat,3) );
            this.SnapPointCloud.geometry.setDrawRange( 0, this.SnapPoints.length );
            this.SnapPointCloud.geometry.verticesNeedUpdate = true;
            this.SnapPointCloud.geometry.computeBoundingSphere();
            this.SnapPointCloud.position.set(...root);
        }
    }

    /**
     * Creates a SnapLine at the given coordinates.
     * @private
     * @function
     */
    createSnapLine(positions, referencedObject) {
        let snapLine = new SnapLine(positions, referencedObject);
        snapLine.attachToScene();

        this.SnapLines.push(snapLine);
        this.Models.push(snapLine.model);
        return snapLine;
    }
}