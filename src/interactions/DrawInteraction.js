import { Interaction } from './Interaction.js';
import { Utilities } from '../utilities/Utilities.js';

import { DrawHelper } from './helpers/DrawHelper.js';
import { PotreeMeasurement } from '../potree_measurements/PotreeMeasurement.js';
import { PotreeBoxVolume } from '../potree_measurements/PotreeBoxVolume.js';
import { PotreeHeightProfile } from '../potree_measurements/PotreeHeightProfile.js';

import * as THREE from '../../libs/threejs/three.module.js';
import { Line } from '../geometries/Line.js';
import { Point } from '../geometries/Point.js';
import { Polygon } from '../geometries/Polygon.js';

/**
 * The interaction used for drawing on (adding elements to) a layer.
 * 
 * @param {Object} config - Configures the interaction.
 * @param {Sigma3D.GeometrySource | Sigma3D.PotreeMeasurementSource} config.source - The source to bind the interaction to.
 * @param {string} config.type - The type of measurement or geometry to draw, based on the source the layer is assigned to. (e.g. "Polygon", "Line", "Point", "HeightPoint", "Measurement", "Volume", "HeightProfile"...)
 * @param {Object} config.measurementConfig - In case a PotreeMeasurement is being drawn (config.type == "Measurement"), this decides the properties of that measurement. See the PotreeMeasurement constructor for more.
 * @param {Object} config.maxVertices - The largest amount of vertices that can be drawn onto the measurement.
* 
 * @property {THREE.Raycaster} Raycaster - An instance of THREE.Raycaster used for getting mouse position.
 * @property {Sigma3D.Source} ParentSource - The source to which the interaction is assigned. Interaction behavior changes depending on source.
 * @property {DOMelement} DomElement - The DOM element used by the PotreeViewer renderer. Used to receive events for user interactions.
 * @property {Object} MeasurementBeingDrawn - The object currently being drawn by the interaction.
 * @property {boolean} MovedMouse - Indicates whether or not the user moved the mouse between pressing the mouse down and up. (true means the user dragged the mouse, false means the user clicked without moving)
 * @property {Array} Vectors - An array of Vectors ([ [x1,y1,z1] [x2,y2,z2]... ]) indicating the positions the user clicked on.
 * @property {string} Type - A string indicating the interaction type.
 * @property {Sigma3D.DrawHelper} DrawHelper - An indicator of the geometry being drawn when the interaction is used with a GeometrySource.
 * @property {boolean} ViewingImage360 - Determines whether or not the user is viewing a 360 image.
 * @property {boolean} ViewingOrientedImage - Determines whether or not the user is viewing an orientedimage.
 * @property {integer} MaxVertices - The largest amount of vertices that can be drawn onto the measurement.
 * 
 * @fires DrawInteraction#drawend
 * @fires DrawInteraction#vertexadded
 * 
 * @example
 * 			await Sigma3D.initialize();
 *
 *           let geomSource = new Sigma3D.GeometrySource({
 *               geometries: [
 *                   new Sigma3D.Point( [0,0,325] ),
 *                   new Sigma3D.Point( [0,4,333] ),
 *                   new Sigma3D.Line([ [0,0,325], [0,4,333] ])
 *               ]
 *           })
 *
 *           let interaction = new Sigma3D.DrawInteraction({
 *               source: geomSource,
 *               type: "Line"
 *           });
 *			
 *           let map = new Sigma3D.Map({
 *               view: new Sigma3D.View({}),
 *
 *               layers: [
 *                   new Sigma3D.GeometryLayer({
 *                       source: geomSource
 *                   })
 *               ],
 *
 *               interactions: [
 *                   interaction
 *               ]
 *           });
 *           
 *           interaction.addEventListener('drawend', (event) => {
 *               console.log(event);
 *           });
 *
 *           Sigma3D.setMap(map);
 *
 *
 * 
 * @category Interaction
 * @class DrawInteraction
 * 
 */

/**
 * @event DrawInteraction#drawend
 * @returns {Array} event.detail - Returns an array of whatever measurements or geometries were last drawn by the interaction.
 */

/**
 * @event DrawInteraction#vertexadded
 * @returns {Array} event.detail.vertex - Returns whatever vertex was added by the last mouse click.
 */

export class DrawInteraction extends Interaction {
    
    constructor(config) {
        super(config);

        this.DrawWithoutSnap = true;

        if (config) {
            if (config.source) this.ParentSource = config.source;
            if (config.type) {
                this.DrawType = config.type;
                this.DrawHelper = new DrawHelper({ helperType: config.type });
            }

            if (config.drawWithoutSnap == false) this.DrawWithoutSnap = false;

            if (config.maxVertices) this.MaxVertices = config.maxVertices;
            else this.MaxVertices = Infinity;

            if (config.measurementConfig) this.MeasurementConfig = config.measurementConfig;
        }

        this.RayCaster = Utilities.Raycaster;

        this.MeasurementBeingDrawn = undefined;

        this.MovedMouse = false;
        this.Vectors = []; // [ [x,y,z], [x,y,z]... ];
        
        this.ViewingImage360 = false;
        this.ViewingOrientedImage = false;

        this.Type = "DrawInteraction";
        this.Active = true;
    }

    /**
     * Adds the event listeners.
     * @private
     * @function
     */
    addEventListeners() {
        Utilities.PotreeViewer.addEventListener('360image_focused', this.setViewingImage3602);
        Utilities.PotreeViewer.addEventListener('360image_unfocused', this.unsetViewingImage3602);
        
        Utilities.PotreeViewer.addEventListener('oriented_image_focused',this.setViewingOrientedImage2);
        Utilities.PotreeViewer.addEventListener('oriented_image_unfocused',this.unsetViewingOrientedImage2);
        
        if (this.ParentSource.Type == "GeometrySource") {
            this.DomElement.addEventListener('pointerup', this.handleGeometrySourcePointerUp2);		    
        }
        else if (this.ParentSource.Type == "PotreeMeasurementSource") {   
            this.DomElement.addEventListener('pointerup', this.handlePotreeMeasurementSourcePointerUp2);
        }
        else {
            console.warn("The assigned source " + this.ParentSource.Type + " is incompatible with the interaction " + this.Type + ".")
        }
    }

    /**
     * Initializes the interaction.
     * @private
     * @function
     */
    async initialize() {
        this.setViewingImage3602 = this.setViewingImage360.bind(this);
        this.unsetViewingImage3602 = this.unsetViewingImage360.bind(this);
        this.setViewingOrientedImage2 = this.setViewingOrientedImage.bind(this);
        this.unsetViewingOrientedImage2 = this.unsetViewingOrientedImage.bind(this);
        this.handlePotreeMeasurementSourcePointerUp2 = this.handlePotreeMeasurementSourcePointerUp.bind(this);
        this.handleGeometrySourcePointerUp2 = this.handleGeometrySourcePointerUp.bind(this);

        this.addEventListeners();
    }

    /**
     * Removes event handlers for when the interaction is taken off a map.
     * @private
     * @function
     */
    remove() {
        if (this.DrawHelper) this.DrawHelper.clear();

        if (this.ParentSource.Type == "PotreeMeasurementSource") {
            if (this.MeasurementBeingDrawn) {
                if (this.DrawType == 'Line' || this.DrawType == 'Measurement' || this.DrawType == 'Angle' || this.DrawType == 'Point' || this.DrawType == 'Distance' || this.DrawType == 'Height' || this.DrawType == 'Circle' || this.DrawType == 'Area' || this.DrawType == 'Azimuth') {
                    this.ParentSource.removeMeasurement(this.MeasurementBeingDrawn);
                    this.MeasurementBeingDrawn = undefined;
                }
                if (this.DrawType == "Volume") {
                    this.ParentSource.removeVolume(this.MeasurementBeingDrawn);
                    this.MeasurementBeingDrawn = undefined;
                }
                if (this.DrawType == "HeightProfile") {
                    this.ParentSource.removeProfile(this.MeasurementBeingDrawn);
                    this.MeasurementBeingDrawn = undefined;
                }
            }
        }
    }

    /**
     * Determines whether or not the interaction uses its event listeners.
     * @private
     * @function
     */
    setActive(active) {
        if (active == true) { this.Active = true; }
        else { this.Active = false; this.remove(); }
    }

    /**
     * Sets ViewingImage360 to true.
     * @private
     * @function
    */
    setViewingImage360() {
        this.ViewingImage360 = true;
    }

    /**
     * Sets ViewingImage360 to false.
     * @private
     * @function
     */
    unsetViewingImage360() {
        this.ViewingImage360 = false
    }


    /**
     * Sets ViewingOrientedImage to true.
     * @private
     * @function
     */
    setViewingOrientedImage() {
        this.ViewingOrientedImage = true;
    }

    /**
     * Sets ViewingOrientedImage to false.
     * @private
     * @function
     */
    unsetViewingOrientedImage() {
        this.ViewingOrientedImage = false;
    }

    /**
     * Handles the "pointerup" event for the corresponding source type.
     * @private
     * @function
     */
    handlePotreeMeasurementSourcePointerUp(event) {
        if (this.Active) {
            if (event.button == 0) { // left click
                let intersect = this.getMouseIntersect(event);

                if (
                    Utilities.Cursor3D.MovedMouse == false && 
                    (((!this.ViewingImage360 && (!intersect || !intersect.object.image360))) || (this.ViewingImage360)) && 
                    (((!this.ViewingOrientedImage) && (!intersect || intersect.object.parent.name != "oriented_images")) || (this.ViewingOrientedImage && intersect.object.parent.name == "oriented_images"))  ) {
                    this.Vectors.push( Utilities.Cursor3D.Position );
                    
                    if ((this.DrawWithoutSnap == false && intersect) || this.DrawWithoutSnap == true) {
                        if (this.MeasurementBeingDrawn) {
                            if (this.DrawType == 'Volume') {
                                if (this.MeasurementBeingDrawn) {
                                    this.MeasurementBeingDrawn.position.set(Utilities.Cursor3D.Position[0], Utilities.Cursor3D.Position[1], Utilities.Cursor3D.Position[2]);
                                }
                            }
                            else {
                                if (this.DrawType == "Line" && this.MeasurementBeingDrawn.spheres.length >= 2) return;
                                if (this.DrawType == "Point" && this.MeasurementBeingDrawn.spheres.length >= 1) return;
                                if (this.MeasurementBeingDrawn.spheres.length < this.MaxVertices) {
                                    this.MeasurementBeingDrawn.addMarker( new THREE.Vector3(Utilities.Cursor3D.Position[0], Utilities.Cursor3D.Position[1], Utilities.Cursor3D.Position[2]));
                                    this.dispatchVertexAdded({ vertex: JSON.parse(JSON.stringify(Utilities.Cursor3D.Position)), index: this.MeasurementBeingDrawn.spheres.length - 1 })
                                }
                            }
                        }
                        else if (!this.MeasurementBeingDrawn){
                            if (this.DrawType == 'Line' || this.DrawType == 'Measurement' || this.DrawType == 'Angle' || this.DrawType == 'Point' || this.DrawType == 'Distance' || this.DrawType == 'Height' || this.DrawType == 'Circle' || this.DrawType == 'Area' || this.DrawType == 'Azimuth') {
                                if (!this.MeasurementConfig) this.MeasurementConfig = {};
                                this.MeasurementConfig.type = this.DrawType;

                                const newMeasurement = new PotreeMeasurement([Utilities.Cursor3D.Position], this.MeasurementConfig);

                                this.ParentSource.addMeasurement(newMeasurement);
                                this.MeasurementBeingDrawn = newMeasurement;
                            }
                            else if (this.DrawType == 'Volume') {                        
                                const newVolume = new PotreeBoxVolume([Utilities.Cursor3D.Position], this.MeasurementConfig);
                                this.ParentSource.addVolume(newVolume);
                                this.MeasurementBeingDrawn = newVolume;
                            }
                            else if (this.DrawType == 'HeightProfile') {
                                const newProfile = new PotreeHeightProfile([Utilities.Cursor3D.Position], this.MeasurementConfig);
                                this.ParentSource.addProfile(newProfile);
                                this.MeasurementBeingDrawn = newProfile;
                            }
                        }
                    }
                }
            }
            else if (event.button == 2) { // right click
                if (this.MeasurementBeingDrawn && !Utilities.Cursor3D.MovedMouse) {
                    if (this.DrawType == 'Line' || this.DrawType == 'Measurement' || this.DrawType == 'Angle' || this.DrawType == 'Point' || this.DrawType == 'Distance' || this.DrawType == 'Height' || this.DrawType == 'Circle' || this.DrawType == 'Area' || this.DrawType == 'Azimuth') {
                        if (this.DrawType == "Line" && (this.MeasurementBeingDrawn.spheres.length < 2)) return;

                        this.ParentSource.removeMeasurement(this.MeasurementBeingDrawn);
                        this.dispatchDrawEnd(this.MeasurementBeingDrawn);
                        this.MeasurementBeingDrawn = undefined;
                    }
                    if (this.DrawType == "Volume") {
                        this.ParentSource.removeVolume(this.MeasurementBeingDrawn);
                        this.dispatchDrawEnd(this.MeasurementBeingDrawn);
                        this.MeasurementBeingDrawn = undefined;
                    }
                    if (this.DrawType == "HeightProfile") {
                        this.ParentSource.removeProfile(this.MeasurementBeingDrawn);
                        this.dispatchDrawEnd(this.MeasurementBeingDrawn);
                        this.MeasurementBeingDrawn = undefined;
                    }
                }
            }
        }
    }

    /**
     * Handles the "pointerup" event for the corresponding source type.
     * @private
     * @function
     */
    handleGeometrySourcePointerUp(event) {
        if (this.Active) {
            if (event.button == 0) { // left click
                let intersect = this.getMouseIntersect(event);

                if (
                        Utilities.Cursor3D.MovedMouse == false && 
                        (((!this.ViewingImage360 && (!intersect || !intersect.object.image360))) || (this.ViewingImage360)) && 
                        (((!this.ViewingOrientedImage) && (!intersect || intersect.object.parent.name != "oriented_images")) || (this.ViewingOrientedImage && intersect.object.parent.name == "oriented_images")) &&
                        this.Vectors.length < this.MaxVertices
                    ) {

                    if (this.DrawHelper) {
                        if ((this.DrawWithoutSnap == false && intersect) || this.DrawWithoutSnap == true) {
                            this.Vectors.push( Utilities.Cursor3D.Position );
                            this.DrawHelper.addVector( Utilities.Cursor3D.Position );
                            this.dispatchVertexAdded({ vertex: JSON.parse(JSON.stringify(Utilities.Cursor3D.Position)), index: this.DrawHelper.Vectors.length - 1 });
                        }
                    }
                }
            }

            else if (event.button == 2) { // right click
                this.Vectors = [];

                if (this.DrawHelper && !Utilities.Cursor3D.MovedMouse) {
                    let drawnGeometry = undefined;

                    if ((this.DrawType == "Point" || this.DrawType == "HeightPoint") && this.DrawHelper.Vectors.length > 0) {
                        let points = [];

                        for (let vector of this.DrawHelper.Vectors) {
                            points.push(new Point(vector));
                        }

                        drawnGeometry = points;
                    }
                    else if (this.DrawType == "Line" && this.DrawHelper.Vectors.length >= 2) {
                        drawnGeometry = new Line(this.DrawHelper.Vectors);
                    }
                    else if (this.DrawType == "Polygon" && this.DrawHelper.Vectors.length >= 3) {
                        drawnGeometry = new Polygon([this.DrawHelper.Vectors]);
                    }
                    
                    if (drawnGeometry) this.dispatchDrawEnd(drawnGeometry);
                    this.DrawHelper.clear();
                }
            }
        }
    }

    /**
     * Gets models clicked by the mouse on the parent source.
     * @private
     * @function
     */
    getMouseIntersect(event) {
        let mouseCoords = new THREE.Vector2(
            ( event.layerX / this.DomElement.getBoundingClientRect().width) * 2 - 1, // x
            -( event.layerY / this.DomElement.getBoundingClientRect().height ) * 2 + 1 // y
        );

        const raycastList = [];
        Utilities.PotreeViewer.scene.scene.traverse(c => { if (c.visible == true && c.type != 'AxesHelper' && c.userData.Type != 'Cursor3D'  && !c.userData.beingModified) { raycastList.push(c); } });

        this.RayCaster.setFromCamera(mouseCoords, Utilities.PotreeViewer.scene.getActiveCamera());

        let intersects = this.RayCaster.intersectObjects( raycastList, false );
        intersects.sort((first, second) => (first.distance > second.distance) ? 1 : -1)

        return intersects[0];
    }

    /**
     * Fires the vertexadded event.
     * @private
     * @function
     */
    dispatchVertexAdded(detail) {
        const customEvent = new CustomEvent('vertexadded', { detail: detail, source: this.ParentSource });

        this.ParentSource.EventNode.dispatchEvent(customEvent);
        this.dispatchEvent(customEvent);
    }

    /**
     * Removes the last added vertex.
     * @private
     * @function
     */
    undo(index) {
        if (this.Active) {
            if (this.ParentSource.Type == "GeometrySource") {
                this.Vectors.pop( Utilities.Cursor3D.Position );
                if (this.DrawHelper) this.DrawHelper.undo(index);

                this.ParentSource.updatePoints();
            }
            else if (this.ParentSource.Type == 'PotreeMeasurementSource') {
                if (this.DrawType == "HeightProfile" || this.DrawType == 'Line' || this.DrawType == 'Measurement' || this.DrawType == 'Angle' || this.DrawType == 'Point' || this.DrawType == 'Distance' || this.DrawType == 'Height' || this.DrawType == 'Circle' || this.DrawType == 'Area' || this.DrawType == 'Azimuth') {
                    if (!index && this.MeasurementBeingDrawn.points.length >= 1) {
                        this.MeasurementBeingDrawn.removeMarker(this.MeasurementBeingDrawn.points.length-1);
                    }
                    else {
                        this.MeasurementBeingDrawn.removeMarker(index);
                    }
                }
                if (this.DrawType == "Volume") {
                    this.ParentSource.removeVolume(this.MeasurementBeingDrawn);
                    this.MeasurementBeingDrawn = undefined;
                }
            }
        }
    }

    /**
     * Fires the drawend event.
     * @private
     * @function
     */
    dispatchDrawEnd(detail) {
        const customEvent = new CustomEvent('drawend', { detail: detail, source: this.ParentSource });

        this.ParentSource.EventNode.dispatchEvent(customEvent);
        this.dispatchEvent(customEvent);
    }
}