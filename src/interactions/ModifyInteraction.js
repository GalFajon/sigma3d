import { Interaction } from './Interaction.js';

import { Utilities } from '../utilities/Utilities.js';
import * as THREE from '../../libs/threejs/three.module.js';

/**
 * The interaction used for modifying the elements of a layer.
 * 
 * @param {Object} config - Configures the interaction.
 * @param {Sigma3D.Source} config.source - The source to which the interaction is bound.
 * 
 * @property {THREE.Raycaster} Raycaster - An instance of THREE.Raycaster used for getting mouse position.
 * @property {Object} SelectedVector - Describes the vector of a geometry that is currently being edited.
 * @property {number} SelectedVector.index - The index of the vector within the array of positions of the geometry.
 * @property {Array} SelectedVector.coordinates - An array of coordinates of the selected vector. ([x,y,z])
 * @property {number} SelectedVector.holeIndex - The index of the vector of a geometry that is being edited if said vector belongs to a polygon hole.
 * @property {THREE.Mesh} SelectedObject - The model of the measurement/geometry being edited.
 * @property {Sigma3D.PotreeMeasurement | Sigma3D.PotreeBoxVolume | Sigma3D.PotreeHeightProfile} SelectedMeasurement - The currently selected measurement of a PotreeMeasurementSource.
 * @property {string} Type - A string indicating the interaction type.
 * @property {boolean} MovedMouse - Indicates whether or not the user moved the mouse between pressing the mouse down and up. (true means the user dragged the mouse, false means the user clicked without moving)
 *
 * @fires ModifyInteraction#modifyend
 * @fires ModifyInteraction#modifystart
* 
 * @example
 * 			await Sigma3D.initialize();
 *			
 *			let pms = new Sigma3D.PotreeMeasurementSource({
 *				measurements: [
 *					new Sigma3D.PotreeMeasurement(
 *						[ [4,4,4], [5,5,5] ],
 *						{
 *							type:'Line'
 *						}
 *					),
 *					new Sigma3D.PotreeMeasurement(
 *						[ [3,3,3] ],
 * 						{
 *							type: 'Point'
 *						}
 * 					)
 *			],
 *				volumes: [
 *					new Sigma3D.PotreeBoxVolume([ [2,2,2] ], { scale: [1,1,1] })
 *				],
 *				heightProfiles: [
 *					new Sigma3D.PotreeHeightProfile( [ [0,0,0], [2,2,2] ], {width: 1} )
 *				]
 *			});
 *
 *			let modify = new Sigma3D.ModifyInteraction({ source:pms });
 *
 *          let map = new Sigma3D.Map({
 *               view: new Sigma3D.View({}),
 *
 *               layers: [
 *                   new Sigma3D.PotreeMeasurementLayer({
 *						source: pms
 *					}),
 *               ],
 *
 *				interactions: [
 *					modify
 *				]
 *           });
 *
 *           await Sigma3D.setMap(map);
 * 
 * @category Interaction
 * @class ModifyInteraction
 */

/**
 * @event ModifyInteraction#modifystart
 * @returns {Object} event.detail - Returns whatever measurement or geometry is going to be edited during the interaction.
 */

/**
 * @event ModifyInteraction#modifyend
 * @returns {Object} event.detail - Returns whatever measurement or geometry was edited during the interaction.
 */

export class ModifyInteraction extends Interaction {
    
    constructor(config) {
        super(config);

        this.RayCaster = Utilities.Raycaster;
        this.Button = 0;
        this.ClickRange = 0.5;

        if (config) {
            if (config.source) this.ParentSource = config.source;
            if (config.target) this.Target = config.target;
            if (config.button) this.Button = config.button;
            if (config.clickRange) this.ClickRange = config.clickRange;
        }

        this.SelectedVector = {
            index: undefined,
            coordinates: undefined,
            holeIndex: undefined
        };

        this.SelectedObject = undefined;
        this.SelectedMeasurement = undefined;
        this.Type = "ModifyInteraction";
        this.Active = true;
    }

    /**
     * Adds the event listeners.
     * @private
     * @function
     */
    addEventListeners() {
        if (this.ParentSource.Type == 'GeometrySource') {
            this.DomElement.addEventListener('pointerup', this.handleGeometrySourcePointerUp);
            this.DomElement.addEventListener('mousemove', this.handleGeometrySourceMouseMove);
        }
        else if (this.ParentSource.Type == "PotreeMeasurementSource") {
            this.ParentSource.addEventListener('added', (e) => { if (e.detail.volume) Utilities.PotreeViewer.inputHandler.blacklist.delete(e.detail.volume); })
            this.DomElement.addEventListener('pointerup',this.handlePotreeMeasurementSourcePointerUp);
            this.DomElement.addEventListener('mousemove', this.handlePotreeMeasurementSourceMouseMove);
        }
        else {
            console.warn("The assigned source " + this.ParentSource.Type + " is incompatible with the interaction " + this.Type + ".")
        }
    }

    /**
     * Initializes the interaction.
     * @private
     * @function
     */
    async initialize() {
        this.handleGeometrySourcePointerUp = this.handleGeometrySourcePointerUp.bind(this);
        this.handleGeometrySourceMouseMove = this.handleGeometrySourceMouseMove.bind(this);
        this.handlePotreeMeasurementSourcePointerUp = this.handlePotreeMeasurementSourcePointerUp.bind(this);
        this.handlePotreeMeasurementSourceMouseMove = this.handlePotreeMeasurementSourceMouseMove.bind(this);

        this.addEventListeners();
    }

    /**
     * Removes event handlers for when the interaction is taken off a map.
     * @private
     * @function
     */
    remove() {
        this.SelectedObject = undefined;
        this.SelectedMeasurement = undefined;
        this.SelectedVector = {
            index: undefined,
            coordinates: undefined,
            holeIndex: undefined
        };

        if (this.ParentSource.Type == "PotreeMeasurementSource") {
            for (let volume of this.ParentSource.Volumes) { Utilities.PotreeViewer.inputHandler.blacklist.add(volume); }
        }
    }

    /**
     * Determines whether or not the interaction uses its event listeners.
     * @private
     * @function
     */
    setActive(active) {
        if (active == true) { 
            this.Active = true;  

            if (this.ParentSource.Type == "PotreeMeasurementSource") {
                for (let volume of this.ParentSource.Volumes) { Utilities.PotreeViewer.inputHandler.blacklist.delete(volume); }
            }
        }
        else { 
            this.Active = false; 
            this.remove(); 
        }
    }

    /**
     * Handles the "pointerup" event for the corresponding source type.
     * @param {Object} event - The pointerup event.
     * @private
     * @function
     */
    handlePotreeMeasurementSourcePointerUp(event) {     
        if (this.Active == true) {
            this.handleGeometrySourceMouseMove();

            if (event.button == this.Button) {   
                if (Utilities.Cursor3D.MovedMouse == false) {
                    if (!this.SelectedMeasurement && !this.SelectedObject) {
                        let intersect = this.getMouseIntersect(event);

                        if (intersect || Utilities.Cursor3D.Snapped) {
                            if (intersect) {
                                this.SelectedObject = intersect.object;
                                this.SelectedMeasurement = intersect.object.parent;
                            }
                            else if(this.ParentSource.Measurements.includes(Utilities.Cursor3D.SnappedObject.parent) || this.ParentSource.Volumes.includes(Utilities.Cursor3D.SnappedObject) || this.ParentSource.HeightProfiles.includes(Utilities.Cursor3D.SnappedObject.parent)) {
                                this.SelectedObject = Utilities.Cursor3D.SnappedObject;

                                if (this.SelectedObject.parent && (this.SelectedObject.parent.Type == 'PotreeMeasurement' || this.SelectedObject.parent.Type == 'PotreeHeightProfile')) this.SelectedMeasurement = Utilities.Cursor3D.SnappedObject.parent;
                                else { this.SelectedMeasurement = Utilities.Cursor3D.SnappedObject; }
                            }

                            this.dispatchModifyStart();
                        }
                    }
                    else {
                        if (this.SelectedMeasurement.Type == "PotreeMeasurement" || this.SelectedMeasurement.Type == 'PotreeHeightProfile') {
                            let i = this.SelectedMeasurement.spheres.indexOf(this.SelectedObject);
                            
                            if (i !== -1) {
                                this.SelectedMeasurement.setPosition(i, new THREE.Vector3(Utilities.Cursor3D.Position[0], Utilities.Cursor3D.Position[1], Utilities.Cursor3D.Position[2]));
                            }

                            this.dispatchModifyEnd();
                            this.clearSelection();
                        }
                        else {
                            this.dispatchModifyEnd();
                            this.clearSelection();
                        }
                    }
                }
            }
        }
    }

    /**
     * Handles the "mousemove" event for the corresponding source type.
     * @private
     * @function
     */
    handlePotreeMeasurementSourceMouseMove() {
        if (this.Active) {
            if (this.SelectedMeasurement && this.SelectedObject.type == "Mesh") {
                if (this.SelectedMeasurement.Type == "PotreeMeasurement" || this.SelectedMeasurement.Type == 'PotreeHeightProfile') {
                    let i = this.SelectedMeasurement.spheres.indexOf(this.SelectedObject);
                    this.SelectedMeasurement.setPosition(i, new THREE.Vector3(Utilities.Cursor3D.Position[0], Utilities.Cursor3D.Position[1], Utilities.Cursor3D.Position[2]));
                }
                else 
                    this.clearSelection();
            }
        }
    }

    removeSelectedVertex() {
        if (this.SelectedObject && this.SelectedObject.Type == 'Line') {
            if (this.SelectedObject.Vectors.length > 2) {
                this.SelectedObject.Vectors.splice(this.SelectedVector.index, 1);
                this.SelectedObject.updateModel();

                this.dispatchModifyEnd();
                this.clearSelection();
            }
        }
        if (this.SelectedObject && this.SelectedObject.Type == 'Polygon') {
            if (this.SelectedObject.Vectors.length > 3) {
                this.SelectedObject.Vectors.splice(this.SelectedVector.index, 1);
                this.SelectedObject.updateModel();

                this.dispatchModifyEnd();
                this.clearSelection();
            }
        }
    }

    /**
     * Handles the "pointerup" event for the corresponding source type.
     * @param {Object} event - The pointerup event.
     * @private
     * @function
     */
    handleGeometrySourcePointerUp(event) { 
            if (this.Active) {
                this.handleGeometrySourceMouseMove();

                if (event.button == this.Button) {
                    if (Utilities.Cursor3D.MovedMouse == false) {
                        let intersect = this.getMouseIntersect(event);

                        if (this.SelectedObject && this.SelectedVector.coordinates) {    
                            this.dispatchModifyEnd();
                            this.clearSelection();
                        }
                        else if (intersect || Utilities.Cursor3D.Snapped || this.Target) {
                            if (this.Target) this.SelectedObject = this.Target;
                            else if (this.ParentSource.Geometries.includes(Utilities.Cursor3D.SnappedObject)) this.SelectedObject = Utilities.Cursor3D.SnappedObject;
                            else if (intersect && intersect.object && (intersect.object.userData && intersect.object.userData.isPolygonSurroundingLine)) this.SelectedObject = intersect.object.userData.parent;
                            else if (intersect) this.SelectedObject = intersect.object.userData;

                        if (this.SelectedObject) {                    
                            this.SelectedObject.beingModified = true;
                            
                            let cursorPos = new THREE.Vector3(...Utilities.Cursor3D.Position);

                            let nearestCoordsv = [0,0,0];
                            let nearestDistv = null;
                            let nearestIndexv = 0;

                            if (this.SelectedObject.Vectors) {


                                for (let i=0; i < this.SelectedObject.Vectors.length; i++) {
                                    let [x,y,z] = this.SelectedObject.Vectors[i];
                                    
                                    if (cursorPos.distanceTo( new THREE.Vector3(x,y,z)) < nearestDistv || nearestDistv === null) {
                                        nearestDistv = cursorPos.distanceTo( new THREE.Vector3(x,y,z));
                                        nearestCoordsv = [x,y,z];
                                        nearestIndexv = i;
                                    }
                                }

                                if (nearestDistv < this.ClickRange) this.SelectedVector = { coordinates: nearestCoordsv, index: nearestIndexv, holeIndex: undefined };
                            }

                            if (this.SelectedObject.Holes) {
                                for (let i=0; i < this.SelectedObject.Holes.length; i++) {
                                    let nearestCoords = [0,0,0];
                                    let nearestDist = null;
                                    let nearestIndex = 0;

                                    for (let j=0; j < this.SelectedObject.Holes[i].length; j++) {
                                        let [x,y,z] = this.SelectedObject.Holes[i][j];

                                        if (cursorPos.distanceTo( new THREE.Vector3(x,y,z)) < nearestDist || nearestDist === null) {
                                            nearestDist = cursorPos.distanceTo( new THREE.Vector3(x,y,z));
                                            nearestCoords = [x,y,z];
                                            nearestIndex = j;
                                        }
                                    }

                                    if (nearestDist < this.ClickRange && (!nearestDistv || nearestDist < nearestDistv)) this.SelectedVector = { coordinates: nearestCoords, index: nearestIndex, holeIndex: i };
                                }
                            }
                                
                            if (!this.SelectedVector.index && !this.SelectedVector.coordinates && !this.SelectedVector.holeIndex && (intersect || (Utilities.Cursor3D.Snapped && Utilities.Cursor3D.SnappedObject == this.SelectedObject))) {
                                    let point;

                                    if (intersect && intersect.point) point = intersect.point
                                    else point = new THREE.Vector3(...Utilities.Cursor3D.MousePosition);
                                    
                                    if ((this.SelectedObject.Type == "Line" || this.SelectedObject.Type == "Polygon") && this.SelectedObject.Vectors) {     
                                        for (let i=1; i < this.SelectedObject.Vectors.length; i++) {
                                            if (this.vectorIsOnLine(this.SelectedObject.Vectors[i-1],this.SelectedObject.Vectors[i],point.toArray())) {
                                                this.SelectedObject.Vectors.splice(i,0,point.toArray());
                                                this.SelectedObject.updateModel();
                                                
                                                this.SelectedVector = {
                                                    index: i,
                                                    coordinates: point.toArray(),
                                                    holeIndex: undefined
                                                }

                                                break;
                                            }
                                        }

                                        if (this.SelectedObject.Holes) {
                                            for (let i=0; i < this.SelectedObject.Holes.length; i++) {
                                                for (let j=1; j < this.SelectedObject.Holes[i].length; j++) {
                                                    if (this.vectorIsOnLine(this.SelectedObject.Holes[i][j-1],this.SelectedObject.Holes[i][j],point.toArray())) {
                                                        this.SelectedObject.Holes[i].splice(j,0,point.toArray());
                                                        this.SelectedObject.updateModel();
                                                        
                                                        this.SelectedVector = {
                                                            index: j,
                                                            coordinates: point.toArray(),
                                                            holeIndex: i
                                                        }
                    
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        if (this.SelectedObject.Type == "Polygon" && this.SelectedObject.Vectors.length > 1) {
                                            if (this.vectorIsOnLine(this.SelectedObject.Vectors[0],this.SelectedObject.Vectors[this.SelectedObject.Vectors.length - 1],point.toArray())) {
                                                this.SelectedObject.Vectors.splice(this.SelectedObject.Vectors.length,0,point.toArray());
                                                this.SelectedObject.updateModel();
                                                
                                                this.SelectedVector = {
                                                    index: this.SelectedObject.Vectors.length - 1,
                                                    coordinates: point.toArray(),
                                                    holeIndex: undefined
                                                }
                                            }
                                        }
                                    }
                                }

                                this.dispatchModifyStart();
                            }
                        }
                        else {
                            this.clearSelection();
                        }
                    }
                }
            }
        }
    

    /**
     * Checks if vector "c" is on a line determined by vector "a" and "b".
     * @private
     * @param {Array} a - The first vector of the line. [x,y,z]
     * @param {Array} b - The second vector of the line. [x,y,z]
     * @param {Array} c - The vector that may or may not be on the line. [x,y,z]
    * @function
     */
    vectorIsOnLine(a,b,c) {
        let curVec = new THREE.Vector3(...a);
        let prevVec = new THREE.Vector3(...b);
        let lineVec = new THREE.Vector3(...c);

        if (Math.floor(curVec.distanceTo(lineVec) + prevVec.distanceTo(lineVec)) == Math.floor(prevVec.distanceTo(curVec))) return true;
        else return false;
    }

    /**
     * Handles the "mousemove" event for the corresponding source type.
     * @private
     * @function
     */
    handleGeometrySourceMouseMove() {
        if (this.Active) {
            if (this.SelectedObject) this.SelectedObject.beingModified = true;

            if (this.SelectedVector.coordinates != undefined && this.SelectedVector.index != undefined ) {
                if (this.SelectedVector.holeIndex == undefined) {
                    this.SelectedVector.coordinates = Utilities.Cursor3D.Position;
                    this.SelectedObject.Vectors[this.SelectedVector.index] = this.SelectedVector.coordinates;
                }
                else {
                    this.SelectedVector.coordinates = Utilities.Cursor3D.Position;
                    this.SelectedObject.Holes[this.SelectedVector.holeIndex][this.SelectedVector.index] = this.SelectedVector.coordinates;
                }

                if (this.SelectedObject.Type !== 'Point' && this.SelectedObject.Type !== 'HeightPoint') this.SelectedObject.updateModel();
                else this.ParentSource.updatePoints();
            }
        }
    }

    /**
     * Clears the SelectedObject, SelectedMeasurement and SelectedVector properties.
     * @function
     */
    clearSelection() {
        if (this.SelectedVector) {
            this.SelectedVector = {
                coordinates: undefined,
                index: undefined,
                holeIndex: undefined
            }
        };

        if (this.SelectedObject) {
            this.SelectedObject.beingModified = false;
            this.SelectedObject = undefined;
        }

        if (this.SelectedMeasurement) this.SelectedMeasurement = undefined;
    }

    /**
     * Gets models clicked by the mouse on the parent source.
     * @private
     * @function
     */
    getMouseIntersect(event) {
        let intersects = [];
        let mouseCoords = new THREE.Vector2(
            ( event.layerX / this.DomElement.getBoundingClientRect().width) * 2 - 1, // x
            -( event.layerY / this.DomElement.getBoundingClientRect().height ) * 2 + 1 // y
        );

        this.RayCaster.setFromCamera(mouseCoords, Utilities.PotreeViewer.scene.getActiveCamera());

        if (this.ParentSource.Models) {
            intersects.push(...this.RayCaster.intersectObjects( this.ParentSource.Models, true ));
        }
        if (this.ParentSource.Type == "GeometrySource" && this.ParentSource.PointsCloud) {
            if (this.ParentSource.PointsCloud) for (const [key, value] of this.ParentSource.PointsCloud) {
                let pointIntersects = this.RayCaster.intersectObject(value, true);
                for (let intersect of pointIntersects) {
                    intersects.push({ object: { userData: this.ParentSource.Points.get(key)[intersect.index] }, point: intersect.point, distance: intersect.distance });
                }
            }
        }

        intersects.sort((first, second) => (first.distance > second.distance) ? 1 : -1)
        return intersects[0];
    }

    /**
     * Fires the modifystart event.
     * @private
     * @function
     */
    dispatchModifyStart() {
        const customEvent = new CustomEvent('modifystart', { 
            detail: {
                object: this.SelectedObject,
                measurement: this.SelectedMeasurement,
                editedVector: this.SelectedVector,
                source: this.ParentSource
            }
        });

        this.dispatchEvent(customEvent);
        this.ParentSource.EventNode.dispatchEvent(customEvent);
    }

    /**
     * Fires the modifyend event.
     * @private
     * @function
     */
    dispatchModifyEnd() {
        const customEvent = new CustomEvent('modifyend', { 
            detail: {
                object: this.SelectedObject,
                measurement: this.SelectedMeasurement,
                editedVector: this.SelectedVector,
                source: this.ParentSource
            }
        });

        this.dispatchEvent(customEvent);
        this.ParentSource.dispatchEvent(customEvent);
    }

}