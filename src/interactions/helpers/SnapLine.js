"use strict";
import * as THREE from '../../../libs/threejs/three.module.js';

import { Utilities } from '../../utilities/Utilities.js';
import { Geometry } from '../../geometries/Geometry.js';

import { SnapLineMaterial } from '../../utilities/Materials.js';

export class SnapLine extends Geometry {
    constructor(positions, referencedObject) {
        super();
        this.Vectors = positions;

        let threeVectors = this.convertPositionsToVectors( positions ); // turns [x,y,z] into a THREE.Vector3() instance
        let geometry = new THREE.BufferGeometry().setFromPoints( threeVectors ); // Every line has a unique geometry, meaning it cannot be stored in the Geometries.js file.

        this.model = new THREE.Line( geometry, SnapLineMaterial );
        this.ThreeScene =  Utilities.PotreeViewer.scene.scene;

        this.Type = "SnapLine"
        this.RefersTo= referencedObject;

        this.model.userData = this;
    }

    convertPositionsToVectors( positions ) {
        let vectors = [];

        for (let [x,y,z] of positions) {
            vectors.push( new THREE.Vector3(x,y,z) );
        }
        
        return vectors;
    }

    attachToScene() {
        this.ThreeScene.add(this.model);
    }

    removeFromScene() {
        this.ThreeScene.remove(this.model);
    }
}