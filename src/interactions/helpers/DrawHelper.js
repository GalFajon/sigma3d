import { Utilities } from '../../utilities/Utilities.js';

import * as THREE from '../../../libs/threejs/three.module.js'

import { Point } from '../../geometries/Point.js';
import { Line } from '../../geometries/Line.js';
import { Polygon } from '../../geometries/Polygon.js';

import { DrawHelperLineMaterial, DrawHelperMeshMaterial, DrawHelperPointMaterial } from '../../utilities/Materials.js';

export class DrawHelper {
    constructor(config) {
        this.Vectors = [];
        
        this.Points = [];
        this.PointsCloud = new THREE.Points(new THREE.BufferGeometry(), DrawHelperPointMaterial);

        this.Line = undefined;
        this.Polygon = undefined;

        this.ThreeScene = Utilities.overlayScene;

        if (config) {
            if (config.helperType) this.HelperType = config.helperType;
            if (config.scene) this.ThreeScene = config.scene;
        }

        this.PointsCloud.userData = this;
        this.Type = "DrawHelper";
    }

    addVector( vector ) {
        this.Vectors.push(vector);

        let point = new Point(vector,  {material: DrawHelperMeshMaterial});
        this.Points.push(point);

        this.updatePoints();

        if (this.HelperType == "Line" && this.Vectors.length > 1) {
            this.removeLine();

            let line = new Line(this.Vectors, {material: DrawHelperLineMaterial});

            this.ThreeScene.add(line.model);
            this.Line = line;
            this.Line.model.userData = this;
        }

        if (this.HelperType == "Polygon" && this.Vectors.length > 2) {
            this.removePolygon();

            let polygon = new Polygon( [ this.Vectors ], {material: DrawHelperMeshMaterial});

            if (!this.Polygon) {
                this.ThreeScene.add(polygon.model);
            }

            this.Polygon = polygon;
            this.Polygon.model.userData = this;
        }
    }

    updatePoints() {
        let root = this.Vectors[0];
        let flat = [];
        for (let vector of this.Vectors) flat.push( vector[0] - root[0], vector[1] - root[1], vector[2] - root[2] );

        this.ThreeScene.remove(this.PointsCloud);
        this.PointsCloud.geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( flat, 3 ) );
        this.PointsCloud.geometry.setDrawRange( 0, this.Vectors.length );
        this.PointsCloud.geometry.verticesNeedUpdate = true;
        this.PointsCloud.geometry.computeBoundingSphere();

        this.PointsCloud.position.set(...root);
        this.ThreeScene.add(this.PointsCloud);
    }

    undo(index) {
        if (!index) {
            this.Vectors.pop();
            this.Points.pop();
        }
        
        if (index) {
            this.Vectors.splice(index,1)
            this.Points.splice(index,1)[0];
        }

        if (this.Line) this.removeLine();
        if (this.Polygon) this.removePolygon();

        if (this.HelperType == "Line" && this.Vectors.length > 1) {
            let line = new Line(this.Vectors, {material: DrawHelperLineMaterial});
            this.ThreeScene.add(line.model);
            this.Line = line;
            this.Line.model.userData = this;
        }

        if (this.HelperType == "Polygon" && this.Vectors.length > 2) {
            let polygon = new Polygon( [ this.Vectors ], {material: DrawHelperMeshMaterial});
            if (!this.Polygon) this.ThreeScene.add(polygon.model);
            this.Polygon = polygon;
            this.Polygon.model.userData = this;
        }

        this.updatePoints();
    }

    clear() {
        this.removePolygon();
        this.removeLine();
        this.removePoints();

        this.Vectors = [];
    }

    removePolygon() {
        if (this.Polygon) {
            this.Polygon.model.geometry.dispose();
            this.ThreeScene.remove(this.Polygon.model);
            this.Polygon = undefined;
        }
    }

    removeLine() {
        if (this.Line) {
            this.Line.model.geometry.dispose();
            this.ThreeScene.remove(this.Line.model);
            this.Line = undefined;
        }
    }

    removePoints() {
        if (this.Points) {
            this.Points = [];
        }

        this.ThreeScene.remove(this.PointsCloud);
    }

    convertPositionsToVectors( positions ) {
        let vectors = [];

        for (let [x,y,z] of positions) {
            vectors.push( new THREE.Vector3(x,y,z) );
        }
        
        return vectors;
    }
}