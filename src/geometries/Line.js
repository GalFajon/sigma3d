import { Geometry } from './Geometry.js';
import * as THREE from '../../libs/threejs/three.module.js';

import {CustomMaterials, LineMaterial} from '../utilities/Materials.js'
import {LineGeometry} from '../utilities/Geometries.js';
import {Utilities} from '../utilities/Utilities.js';

/**
 * A geometry that displays a line across a series of positions.
 * 
 * @param {Array} positions - The positions between which the line should be drawn. ( [ [x1,y1,z1], [x2,y2,z2]... ] ).
 * 
 * @param {Object} config - Configures the Line object.
 * @param {THREE.Material} config.material - A custom Three.js material to be used by the line. 
 * 
 * @property {Array} Vectors - An array of vectors containing every position the geometry occupies.
 * @property {THREE.Line} model - The model used to represent the geometry on a Three.js scene. 
 * @property {string} Type - A string, indicating the type of the geometry.
 * @property {boolean} beingModified - Determines whether or not the geometry is being modified by a ModifyInteraction.
 * 
 * @example 
 * await Sigma3D.initialize();
 *
 * let geomSource = new Sigma3D.GeometrySource({
 *      geometries: [
 *		    new Sigma3D.Line([ [0,0,0], [-2,6,0] ])
 *		]
 *	});
 *			
 * let map = new Sigma3D.Map({
 *      view: new Sigma3D.View({}),
 *      layers: [
 *          new Sigma3D.GeometryLayer({
 *				source: geomSource
 *			})
 *        ]
 *  });
 * 
 * @category Geometry
 * @class
 */

export class Line extends Geometry {
    
    constructor( positions, config ) {    
        super();
        
        this.Vectors = positions;

        this.beingModified = false;

        this.createModel();

        if (config) {
            if (config.material) {
                this.model.material = config.material;
                this.CustomMaterial = config.material;
                if (CustomMaterials.indexOf(this.CustomMaterial) == -1) CustomMaterials.push(this.CustomMaterial);
            }
        }

        this.Type = "Line"
        this.model.userData = this;
    }

    /**
     * Converts an array of positions into a flat series of coordinates instances.
     * @param {Array} positions - An array of positions. ([ [x1,y1,z1], [x2,y2,z2]... ])
     * @private
     * @function
     */
     convertPositionsToFlatArray( positions ) {
        let flatArray = [];

        for (let [x,y,z] of positions) {
            flatArray.push(x,y,z);
        }
        
        return flatArray;
    }

    /**
     * Converts a flat array of coordinates into local positions relative to the origin of the line (first vector).
     * @param {Array} positions - An array of positions. ([ [x1,y1,z1], [x2,y2,z2]... ])
     * @private
     * @function
     */
    convertFlatPositionsToLocalPositions( flatPositions ) {
        let localPositions = [0,0,0];

        let initialPosition = new THREE.Vector3(flatPositions[0], flatPositions[1], flatPositions[2]);

        for (let i=3; i < flatPositions.length; i+=3) {
            let currentPosition = new THREE.Vector3(flatPositions[i], flatPositions[i+1], flatPositions[i+2]);
            currentPosition = currentPosition.clone().sub(initialPosition).toArray();

            localPositions.push(currentPosition[0], currentPosition[1], currentPosition[2]);
        }

        return localPositions;
    }

    /**
     * Initially creates the model of the line, similar in function to updateModel.
     * @private
     * @function
     */
    createModel() {
        let flatPositions = this.convertPositionsToFlatArray( this.Vectors ); // turns [x,y,z] into [x,y,z,x2,y2,z2...]
        let localPositions = this.convertFlatPositionsToLocalPositions(flatPositions);

        let geometry = new LineGeometry();

        geometry.setPositions(localPositions);

        let material = LineMaterial;

        if (this.CustomMaterial) {
            material = this.CustomMaterial;
        }

        this.model = new Utilities.Line2( geometry, material );
        this.model.computeLineDistances();
        this.model.scale.set( 1, 1, 1 );
        this.model.position.set(...this.Vectors[0]);
    }

    /**
     * Updates the model according to the Vectors property of the object.
     * @function
    */
    updateModel() {
        let flatPositions = this.convertPositionsToFlatArray( this.Vectors ); // turns [x,y,z] into [x,y,z,x2,y2,z2...]
        let localPositions = this.convertFlatPositionsToLocalPositions(flatPositions);

        let geometry = new LineGeometry();

        geometry.setPositions(localPositions);

        if (this.CustomMaterial) {
            this.model.material.dispose();
            this.model.material = this.CustomMaterial;
        }

        this.model.geometry.dispose();
        this.model.geometry = geometry;
     
        this.model.computeLineDistances();
        this.model.scale.set( 1, 1, 1 );
        this.model.position.set(...this.Vectors[0]);
    }

    setMaterial(material) {
        this.CustomMaterial = material;
        if (CustomMaterials.indexOf(this.CustomMaterial) == -1) CustomMaterials.push(this.CustomMaterial);
        
        this.model.material.dispose();
        this.model.material = this.CustomMaterial;
    }

}