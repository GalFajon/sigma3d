import { EventDispatcher } from '../utilities/EventDispatcher.js'

/**
 * The class all other geometry types inherit from. Not to be used by itself.
 * @property {Array} Vectors - An array of vectors containing every position the geometry occupies.
 * @category Geometry
 * @class
 */

export class Geometry extends EventDispatcher {
    construtor() {
        this.Vectors = []
    }
}