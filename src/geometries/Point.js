import { Geometry } from './Geometry.js';
import * as THREE from '../../libs/threejs/three.module.js';
import {PointMaterial} from '../utilities/Materials.js'

/**
 * A geometry that displays a point on certain coordinates.
 * 
 * @param {Array} position - The position of the point. ([x,y,z])
 * 
 * @param {Object} config - Configures the Point object.
 * @param {THREE.Material} config.material - A custom Three.js material to be used by the point.
 * @param {Sigma3D.Symbol} config.symbol - The Symbol to bind to the Point.
 * 
 * @property {Array} Vectors - An array of vectors containing every position the geometry occupies.
 * @property {THREE.Mesh} model - The model used to represent the geometry on a Three.js scene. 
 * @property {string} Type - A string, indicating the type of the geometry.
 * @property {boolean} beingModified - Determines whether or not the geometry is being modified by a ModifyInteraction.
 * 
 * @example 
 * await Sigma3D.initialize();
 *
 * let geomSource = new Sigma3D.GeometrySource({
 *      geometries: [
 *		    new Sigma3D.Point([0,0,0])
 *		]
 *	});
 *			
 * let map = new Sigma3D.Map({
 *      view: new Sigma3D.View({}),
 *      layers: [
 *          new Sigma3D.GeometryLayer({
 *				source: geomSource
 *			})
 *        ]
 *  });
 * 
 *	Sigma3D.setMap(map);
 * 
 * @category Geometry
 * @class
 */

class Point extends Geometry {
    constructor( [x, y, z], config ) {    
        super();
        this.Vectors = [ [x,y,z] ] 

        if (config && config.symbol) this.Symbol = config.symbol;

        this.beingModified = false;
        this.highlighted = false;
        
        this.Type = "Point";
    }
}

export { Point };

