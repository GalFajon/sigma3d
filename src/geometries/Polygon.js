import { Geometry } from './Geometry.js';
import * as THREE from '../../libs/threejs/three.module.js';
import {earcut} from '../../libs/earcut/earcut.js'
import {Utilities} from '../utilities/Utilities.js';

import {LineGeometry} from '../utilities/Geometries.js';

import {PolygonMaterial, CustomMaterials} from '../utilities/Materials.js'
import { PolygonSurroundingLineMaterial } from '../utilities/Materials.js';

import {Point} from '../geometries/Point.js';

/**
 * A geometry that displays a polygon (and its potential holes) on certain coordinates.
 * 
 * @param {Array} positions - The positions the polygon occupies, as well as the positions of its holes. ( [ [ [x1,y1,z1], [x2,y2,z2]... ], [ [hole1_x1, hole1_y1, hole1_z1]... ], [ [hole2_x1, hole2_y1, hole2_z1]... ] ])
 * 
 * @param {Object} config - Configures the Polygon object.
 * @param {THREE.Material} config.material - A custom Three.js material to be used by the point. 
 * 
 * @property {Array} Vectors - An array of vectors containing every position the geometry occupies.
 * @property {THREE.Mesh} model - The model used to represent the geometry on a Three.js scene. 
 * @property {string} Type - A string, indicating the type of the geometry.
 * @property {Array} surroundingLines - An array of lines that form the black outline of a polygon.
 * @property {boolean} beingModified - Determines whether or not the geometry is being modified by a ModifyInteraction.
 * 
 * @example 
 * await Sigma3D.initialize();
 *
 * let geomSource = new Sigma3D.GeometrySource({
 *      geometries: [
 *           new Sigma3D.Polygon(
 *               [
 *                   [ [0,0,0], [0,50,0], [50,50,0], [50,0,0] ],
 *                   [ [10,10,0], [10,20,0], [20,20,0], [20,10,0] ]
 *               ]
 *           )
 *		]
 * });
 *			
 * let map = new Sigma3D.Map({
 *      view: new Sigma3D.View({}),
 *      layers: [
 *          new Sigma3D.GeometryLayer({
 *				source: geomSource
 *			})
 *        ]
 * });
 * 
 * Sigma3D.setMap(map);
 * 
 * @category Geometry
 * @class
 */

class Polygon extends Geometry {

    // Positions is an array of coordinates. It is structured according to how polygons work in geoJSON.
    // The first set of coordinates is used to represent the polygon, while each subsequent array represents holes within the polygon.
    // [ [ [0,0], [3,3], [5,5] ], [ [1,1] [2,2], [3,3] ] ]

    constructor(positions, config) {
        super();

        this.Vectors = positions[0];
        this.Holes = this.getHoles(positions);

        let material = PolygonMaterial;

        if (config) {
            if (config.material) {
                material = config.material;
                this.CustomMaterial = config.material;
                if (CustomMaterials.indexOf(this.CustomMaterial) == -1) CustomMaterials.push(this.CustomMaterial);
            }
        }
        
        this.model = new THREE.Mesh(this.generateGeometry(positions), material);
        this.model.position.set(positions[0][0][0], positions[0][0][1], positions[0][0][2]);

        this.surroundingLines = this.generateSurroundingLines(positions);
        for (let surroundingLine of this.surroundingLines) this.model.add(surroundingLine);

        //if (config && config.generateCentroid == true) this.addCentroid(this.generateCentroid(positions))
        this.SurroundingLineVisible = true;
        this.beingModified = false;

        this.Type = "Polygon";
        this.model.userData = this;
    }

    /**
     * Gets the coordinates of every hole.
     * @param {Array} positions - An array of positions. ( [ [ [x1,y1,z1], [x2,y2,z2]... ], [ [hole1_x1, hole1_y1, hole1_z1]... ], [ [hole2_x1, hole2_y1, hole2_z1]... ] ])
     * 
     * @returns {Array} holes - The coordinates of every hole.
     * 
     * @private
     * @function
     */
    getHoles(positions) {
        let holes = [];

        for (let i=1; i < positions.length; i++) {
            holes.push(positions[i]);
        }

        return holes;
    }

    /**
     * Converts the position vectors of the polygon into local coordinates based on the first vector in the array.
     * @param {Array} positions - An array of positions. ( [ [ [x1,y1,z1], [x2,y2,z2]... ], [ [hole1_x1, hole1_y1, hole1_z1]... ], [ [hole2_x1, hole2_y1, hole2_z1]... ] ])
     * 
     * @returns {Array} localPositions - The local positions.
     * 
     * @private
     * @function
     */
    convertPositionsToLocalPositions( positions ) {
        let initialPosition = new THREE.Vector3(positions[0][0][0], positions[0][0][1], positions[0][0][2]);
        let localPositions = [];

        for (let i=0; i < positions.length; i++) {
            let positionSet = positions[i];
            
            localPositions.push([]);

            for (let j = 0; j < positionSet.length; j++) {
                let [x,y,z] = positionSet[j];
                let currentPosition = new THREE.Vector3(x,y,z);

                currentPosition = currentPosition.sub(initialPosition).toArray();

                localPositions[i].push(currentPosition)
            }
        }

        return localPositions
    }

    /*
    Below are methods refering to the scrapped "centroid" functionality of polygons. This might come in handy at a later time.

    addCentroid(centroid) {
        this.Centroid = centroid
    }

    removeCentroid() {
        this.Centroid = undefined;
    }

    generateCentroid(positions) {
        let turfPositions = positions;
        let totalAverageHeight = 0;

        for (let i = 0; i < positions.length; i++) {
            let averageHeight = 0;

            turfPositions[i].push(turfPositions[i][0]);
            
            for (let j=0; j < positions[i].length; j++) {
                averageHeight += positions[i][j][2];
            }

            averageHeight /= positions[i].length;
            totalAverageHeight += averageHeight;
        }

        totalAverageHeight /= positions.length;
        totalAverageHeight += 1;

        let centroidCoords = Utilities.turf.centroid(Utilities.turf.polygon(turfPositions)).geometry.coordinates;
        centroidCoords.push(totalAverageHeight);
        
        return new Point(centroidCoords, {material: PolygonCentroidMaterial})
    }
    */

    /**
     * Generates the THREE.Geometry used by the model of the polygon.
     * @param {Array} positions - An array of positions. ( [ [ [x1,y1,z1], [x2,y2,z2]... ], [ [hole1_x1, hole1_y1, hole1_z1]... ], [ [hole2_x1, hole2_y1, hole2_z1]... ] ])
     * 
     * @returns {THREE.Geometry} geometry - The geometry of the polygon.
     * 
     * @private
     * @function
     */
    generateGeometry( positions ) {
        let localPositions = this.convertPositionsToLocalPositions(positions);

        if (positions[0].length < 4 || this.Holes.length > 0) {
            let data = earcut.flatten(localPositions);
            let triangles = earcut(data.vertices, data.holes, data.dimensions);

            let geometry = new THREE.Geometry();

            for (let i=0; i < data.vertices.length; i += 3) {
                geometry.vertices.push( new THREE.Vector3(data.vertices[i], data.vertices[i + 1], data.vertices[i +2]) )
            }

            for (let i=0; i < triangles.length; i+=3) {
                geometry.faces.push( new THREE.Face3(triangles[i], triangles[i+1], triangles[i+2]) )
            }

            geometry.computeFaceNormals();
            
            return geometry;
        }
        else {
            let vectors = [];

            for (let ring of localPositions) {
                for (let coords of ring) vectors.push(new THREE.Vector3(...coords));
            }

            return new Utilities.ConvexGeometry(vectors);
        }
    }

    /**
     * Generates the THREE.Geometry used by the model of the polygon.
     * @param {Array} positions - An array of positions. ( [ [ [x1,y1,z1], [x2,y2,z2]... ], [ [hole1_x1, hole1_y1, hole1_z1]... ], [ [hole2_x1, hole2_y1, hole2_z1]... ] ])
     * 
     * @returns {THREE.Geometry} geometry - The geometry of the polygon.
     * 
     * @private
     * @function
     */
    generateSurroundingLines(positions) {
        let surroundingLines = [];
        let localPositions = this.convertPositionsToLocalPositions(positions);

        for (let positionSet of localPositions) {
            let flatPositions = [];

            for (let [x,y,z] of positionSet) flatPositions.push(x,y,z);
            flatPositions.push(positionSet[0][0], positionSet[0][1], positionSet[0][2]);

            let geometry = new LineGeometry();
            geometry.setPositions(flatPositions);
    
            let material = PolygonSurroundingLineMaterial;
    
            let model = new Utilities.Line2( geometry, material );
            model.computeLineDistances();
            model.scale.set( 1, 1, 1 );

            model.userData.isPolygonSurroundingLine = true;
            model.userData.parent = this;
    
            surroundingLines.push(model);
        }

        return surroundingLines;
    }

    /**
     * Updates the model of the polygon to show/hide its surrounding line.
     * @function
     */
    toggleSurroundingLine() {
        this.SurroundingLineVisible = !this.SurroundingLineVisible;
        this.updateSurroundingLineVisibility();
    }

    /**
     * Updates the visibility of the surrounding line.
     * @function
     */
    updateSurroundingLineVisibility() {
        if (this.SurroundingLineVisible == true) for (let surroundingLine of this.surroundingLines) surroundingLine.visible = true;
        else for (let surroundingLine of this.surroundingLines) surroundingLine.visible = false;
    }

    /**
     * Updates the model of the polygon to reflect its Vectors array.
     * @private
     * @function
     */
    updateModel() {
        let geometry = this.generateGeometry( [this.Vectors, ...this.Holes] );
        
        this.model.geometry.dispose();
        this.model.geometry = geometry;
        this.model.position.set(this.Vectors[0][0], this.Vectors[0][1], this.Vectors[0][2]);

        for (let surroundingLine of this.surroundingLines) surroundingLine.geometry.dispose();
        for (let surroundingLine of this.surroundingLines) this.model.remove(surroundingLine);

        this.surroundingLines = this.generateSurroundingLines([this.Vectors, ...this.Holes]);
        
        for (let surroundingLine of this.surroundingLines) this.model.add(surroundingLine);
        this.updateSurroundingLineVisibility();
    }

    setMaterial(material) {
        this.CustomMaterial = material;
        if (CustomMaterials.indexOf(this.CustomMaterial) == -1) CustomMaterials.push(this.CustomMaterial);
        
        this.model.material.dispose();
        this.model.material = this.CustomMaterial;
    }
}

export {Polygon}
