import { Geometry } from './Geometry.js';
import * as THREE from '../../libs/threejs/three.module.js';
import { TextGeometry } from '../utilities/Geometries.js'
import {HeightPointMaterial} from '../utilities/Materials.js'
import { Utilities } from '../utilities/Utilities.js';

/**
 * A geometry that displays a point and its height.
 * 
 * @param {Array} position - The position at which to place the HeightPoint ([x,y,z]).
 * 
 * @param {Object} config - Configures the HeightPoint object.
 * @param {THREE.Material} config.material - A custom Three.js material to be used by the point. 
 * 
 * @property {Array} Vectors - An array of vectors containing every position the geometry occupies.
 * @property {THREE.Mesh} model - The model used to represent the geometry on a Three.js scene. 
 * @property {THREE.Mesh} labelModel - The model used for the label that indicates the geometry height on a Three.js scene.
 * @property {string} Type - A string, indicating the type of the geometry.
 * @property {boolean} beingModified - Determines whether or not the geometry is being modified by a ModifyInteraction.
 * 
 * @example 
 * await Sigma3D.initialize();
 *
 * let geomSource = new Sigma3D.GeometrySource({
 *      geometries: [
 *		    new Sigma3D.HeightPoint([0,0,0])
 *		]
 *	});
 *			
 * let map = new Sigma3D.Map({
 *      view: new Sigma3D.View({}),
 *      layers: [
 *          new Sigma3D.GeometryLayer({
 *				source: geomSource
 *			})
 *        ]
 *  });
 * 
 *	Sigma3D.setMap(map);
 * 
 * @category Geometry
 * @class
 */

export class HeightPoint extends Geometry {
    constructor( [x, y, z], config ) { 
        super();
        this.Vectors = [ [x,y,z] ];

        this.beingModified = false;
        this.Type = "HeightPoint"
    }

    /**
     * Updates the Vectors property of the HeightPoint after it has been moved.
     * @function
     */
    updateVectors() {
        this.Vectors = [ [this.model.position.x, this.model.position.y, this.model.position.z] ];
    }

    /**
     * Updates the model of the HeightPoint to reflect its Vectors property.
     * @function
     */
    updateModel() {
        
    }
}