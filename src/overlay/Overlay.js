import { CSS2DObject } from "../../libs/threejs/utils/CSS2DRenderer.js";
import { Utilities } from "../utilities/Utilities.js";

/**
 * A class that allows for the display of a 2D DOM element on a set of coordinates in 3D space.
 * 
 * @param {Array} position - Determines the position of the overlay. ([x,y,z])
 * @param {Object} config - Configures the overlay.
 * @param {DOMElement} config.domElement -The DOM element to be displayed at the coordinates.
 * 
 * @property {string} Type - Indicates the object type ("Overlay").
 * @property {Array} Vectors - An array of coordinates of the Overlay. ([ [x,y,z] ])
 * @property {THREE.Scene} ThreeScene - The THREE.js scene the Overlay is bound to.
 * @property {THREE.CSS2DObject} model - The CSS2DObject instance representing the overlay.
 * 
 * @example
 *  // <div id="overlay_text" style="color:green; background-color:aqua;">My first overlay</div>
 *
 *	// <div id="overlay_text_2" style="color:red; background-color:black;" onclick="alert('working')">
 *	//	    <h1>My second overlay.</h1>
 *	//	    <button>A button.</button>
 *	// </div>
 *  
 * 	await Sigma3D.initialize();
 *
 *   let geomSource = new Sigma3D.GeometrySource({
 *       geometries: [
 *           new Sigma3D.Point([0,0,0]),
 *           new Sigma3D.Point([1,1,1])
 *       ]
 *   });
 *   
 *   let map = new Sigma3D.Map({
 *       view: new Sigma3D.View({}),
 *
 *       layers: [
 *           new Sigma3D.NexusLayer({
 *               source: geomSource
 *           })
 *       ],
 *
 *       overlays: [
 *           new Sigma3D.Overlay([0,0,0], { domElement: document.getElementById('overlay_text') }),
 *       ]
 *   });
 *
 *   await Sigma3D.setMap(map);
 *
 *   const secondOverlay = new Sigma3D.Overlay([1,1,1], { domElement: document.getElementById('overlay_text_2')})
 *   map.addOverlay(secondOverlay);
 * 
 * @category Overlay
 * @class
 */
export class Overlay
{
    constructor([x, y, z], config) {
        this.Type = "Overlay";
        this.Vectors = [ [x,y,z] ];

        if (config.domElement) {
            this.model = new CSS2DObject(config.domElement);
            this.model.position.set(x,y,z);
            this.DomElement = config.domElement;
            this.model.userData = this;
        }
        
        if (config.visibilityDistance) this.VisibilityDistance = config.visibilityDistance;
        if (config.clickable == false) this.DomElement.style.pointerEvents = 'none';

        this.ThreeScene = Utilities.PotreeViewer.scene.scene;
    }

    /**
     * Attaches the overlay to the THREE.Scene instance.
     * @private
     * @function
     */
    attachToScene() {
        this.ThreeScene.add(this.model);
    }

    /**
     * Removes the overlay from the THREE.Scene instance.
     * @private
     * @function
     */
    removeFromScene() {
        this.ThreeScene.remove(this.model);
    }

    /**
     * Updates the array of vectors in accordance to the overlay position.
     * @private
     * @function
     */
    updateVectors() {
        this.Vectors = [ [this.model.position.x, this.model.position.y, this.model.position.z] ];
    }

    /**
     * Updates the overlay model in accordance to the vector array.
     * @private
     * @function
     */
    updateModel() {
        this.model.position.set(this.Vectors[0][0], this.Vectors[0][1], this.Vectors[0][2]);
    }

    /**
     * Updates the position of the overlay.
     * @private
     * @function
     */
    setPosition([x,y,z]) {
        this.Vectors = [ [x, y, z] ];
        this.model.position.set(x, y, z);
    }
}