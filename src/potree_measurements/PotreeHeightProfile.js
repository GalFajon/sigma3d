import { Utilities } from "../utilities/Utilities.js";
import * as THREE from '../../libs/threejs/three.module.js';

/**
 * A wrapper for the Potree height profile class. (http://potree.org/potree/examples/elevation_profile.html)
 * 
 * @param {Array} positions - The positions of the measurement markers (1 or more, [[x,y,z],[x2,y2,z2]]).
 * 
 * @param {Object} config - Configures the measurement.
 * @param {string} config.width - Sets the width of the height profile.
 * 
 * @property {string} Type - Indicates the type of the object ("PotreeHeightProfile").
 * @example
 *  await Sigma3D.initialize();
 *   
 *  let pms = new Sigma3D.PotreeMeasurementSource({
 *      heightProfiles: [
 *          new Sigma3D.PotreeHeightProfile( [ [0,0,0], [2,2,2] ], {width: 1} )
 *      ]
 *  });
 *
 *  let map = new Sigma3D.Map({
 *      view: new Sigma3D.View({}),
 *      layers: [
 *          new Sigma3D.PotreeMeasurementLayer({
 *              source: pms
 *           }),
 *       ]
 *   });
 *
 *   await Sigma3D.setMap(map);
 * 
 * @category PotreeMeasurement
 * @class PotreeHeightProfile
 *
 */

export class PotreeHeightProfile extends Utilities.Potree.Profile {
    constructor(positions, config) {
        super();

        for (let [x,y,z] of positions) {
            this.addMarker( new THREE.Vector3(x,y,z) );
        }

        if(this.points.length <= 1){
            let camera = Utilities.PotreeViewer.scene.getActiveCamera();
            let distance = camera.position.distanceTo(this.points[0]);
            let clientSize = Utilities.PotreeViewer.renderer.getSize(new THREE.Vector2());
            let pr = Utilities.Potree.Utils.projectedRadius(1, camera, distance, clientSize.width, clientSize.height);
            let width = (10 / pr);

            this.setWidth(width);
        }

        if (config) {
            if (config.width) this.setWidth(config.width);
        }

        this.Type = "PotreeHeightProfile";
    }
}