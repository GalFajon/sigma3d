import { Utilities } from "../utilities/Utilities.js";
import * as THREE from '../../libs/threejs/three.module.js';

import { PotreeLineMaterial } from "../utilities/Materials.js";
import {LineGeometry} from '../utilities/Geometries.js';

/**
 * A wrapper for the Potree Measure class. (http://potree.org/potree/examples/measurements.html)
 * 
 * @param {Array} positions - The positions of the measurement markers (1 or more, [[x,y,z],[x2,y2,z2]]).
 * 
 * @param {Object} config - Configures the measurement.
 * @param {boolean} config.showDistances - Shows the distance between measurement markers as a line.
 * @param {boolean} config.showCoordinates - Displays the coordinates of measurement markers.
 * @param {boolean} config.showArea - Displays the size of the area surrounded by the measurement markers.
 * @param {boolean} config.closed - Indicates whether or not the measurement is closed (a line is drawn from the last measurement to the first).
 * @param {boolean} config.showAngles - Shows the angles betwwen the markers.
 * @param {boolean} config.showCircle - Displays a circle with a radius between two markers.
 * @param {boolean} config.showHeight - Displays the height between two points.
 * @param {boolean} config.showEdges - Disables/enables the display of edges on a measurement.
 * @param {boolean} config.showAzimuth - Shows the azimuth angle between three markers.
 * @param {boolean} config.maxMarkers - Limits the maximum amount of markers allowed.
 *  
 * @param {string} config.type - Allows a choice between several preset measurements ("Angle", "Point", "Distance", "Height", "Circle", "Area", "Azimuth").
 * 
 * @property {string} Type - Indicates the type of the object ("PotreeMeasurement").
 * 
 * @example 
 *  await Sigma3D.initialize();
 *   
 *  let pms = new Sigma3D.PotreeMeasurementSource({
 *      measurements: [
 *          new Sigma3D.PotreeMeasurement(
 *              [ [4,4,4], [5,5,5] ],
 *              {
 *                  type: 'Azimuth'
 *              }
 *          ),
 *          new Sigma3D.PotreeMeasurement(
 *              [ [3,3,3] ],
 *              {
 *                  type: 'Point'
 *              }
 *          )
 *      ]
 *  });
 *
 *  let map = new Sigma3D.Map({
 *      view: new Sigma3D.View({}),
 *      layers: [
 *          new Sigma3D.PotreeMeasurementLayer({
 *              source: pms
 *          }),
 *      ]
 *  });
 *
 *  await Sigma3D.setMap(map);
 * 
 * @category PotreeMeasurement
 * @class PotreeMeasurement
 *
 */

export class PotreeMeasurement extends Utilities.Potree.Measure {
    constructor(positions, config) {
        super();

        if (positions) {
            for (let position of positions) {
                this.addMarker( new THREE.Vector3(position[0], position[1], position[2]));
            }
        }

        if (config) {
            if (config.type) {
                if (config.type == 'Angle') {
                    this._showDistances = false;
                    this._showAngles = true;
                    this._showArea = false;
                    this._closed = true;
                    this._maxMarkers = 3;
                    this._name = 'Angle';
                }

                if (config.type == 'Point') {
                    this._showDistances = false;
                    this._showAngles = false;
                    this._showCoordinates = true;
                    this._showArea = false;
                    this._closed = true;
                    this._maxMarkers = 1;
                    this._name = 'Point';
                }

                if (config.type == 'Distance') {
                    this._showDistances = true,
                    this._showArea = false,
                    this._closed = false,
                    this._name = 'Distance'
                }

                if (config.type == 'Height') {
                    this._showDistances = false;
                    this._showHeight = true;
                    this._showArea = false;
                    this._closed = false;
                    this._maxMarkers = 2;
                    this._name = 'Height';

                }

                if (config.type == 'Circle') {                 
                    this._showDistances = false;
                    this._showHeight = false;
                    this._showArea = false;
                    this._showCircle = true;
                    this._showEdges = false;
                    this._closed = false;
                    this._maxMarkers = 3;
                    this._name = 'Circle';
                }
                 
                if (config.type == 'Area') {
                    this._showDistances = true;
                    this._showArea = true;
                    this._closed = true;
                    this._name = 'Area';
                }

                if (config.type == 'Azimuth') {
                    this._showDistances = false;
                    this._showHeight = false;
                    this._showArea = false;
                    this._showCircle = false;
                    this._showEdges = false;
                    this._showAzimuth = true;
                    this._closed = false;
                    this._maxMarkers = 2;
                    this._name = 'Azimuth';
                }
            }

            if (config.showDistances != undefined) this._showDistances = config.showDistances;
            if (config.showCoordinates != undefined) this._showCoordinates = config.showCoordinates;
            if (config.showArea != undefined) this._showArea = config.showArea;
            if (config.closed != undefined) this._closed = config.closed;
            if (config.showAngles != undefined) this._showAngles = config.showAngles;
            if (config.showCircle != undefined) this._showCircle = config.showCircle;
            if (config.showHeight != undefined) this._showHeight = config.showHeight;
            if (config.showEdges != undefined) this._showEdges = config.showEdges;
            if (config.showAzimuth != undefined) this._showAzimuth = config.showAzimuth;
            if (config.maxMarkers != undefined) this._maxMarkers = config.maxMarkers
            this._showPointName = false;
        }

        this.Type = "PotreeMeasurement";
    }

    /**
     * Adds a new marker at the given coordinates.
     * @param {Array} point - An array containing the coordinates [x,y,z]
     * @function
     */
     addMarker (point) {
        if (point.x != null) {
            point = {position: point};
        }else if(point instanceof Array){
            point = {position: new Vector3(...point)};
        }
        this.points.push(point);

        // sphere
        let sphere = new THREE.Mesh(this.sphereGeometry, this.createSphereMaterial());

        this.add(sphere);
        this.spheres.push(sphere);

        { // edges
            let lineGeometry = new LineGeometry();
            lineGeometry.setPositions( [
                    0, 0, 0,
                    0, 0, 0,
            ]);

            let edge = new Utilities.Line2(lineGeometry, PotreeLineMaterial);
            edge.visible = true;

            this.add(edge);
            this.edges.push(edge);
        }

        { // edge labels
            let edgeLabel = new Utilities.Potree.TextSprite();
            edgeLabel.setBorderColor({r: 0, g: 0, b: 0, a: 1.0});
            edgeLabel.setBackgroundColor({r: 0, g: 0, b: 0, a: 1.0});
            edgeLabel.material.depthTest = false;
            edgeLabel.visible = false;
            edgeLabel.fontsize = 16;
            this.edgeLabels.push(edgeLabel);
            this.add(edgeLabel);
        }

        { // angle labels
            let angleLabel = new Utilities.Potree.TextSprite();
            angleLabel.setBorderColor({r: 0, g: 0, b: 0, a: 1.0});
            angleLabel.setBackgroundColor({r: 0, g: 0, b: 0, a: 1.0});
            angleLabel.fontsize = 16;
            angleLabel.material.depthTest = false;
            angleLabel.material.opacity = 1;
            angleLabel.visible = false;
            this.angleLabels.push(angleLabel);
            this.add(angleLabel);
        }

        { // coordinate labels
            let coordinateLabel = new Utilities.Potree.TextSprite();
            coordinateLabel.setBorderColor({r: 0, g: 0, b: 0, a: 1.0});
            coordinateLabel.setBackgroundColor({r: 0, g: 0, b: 0, a: 1.0});
            coordinateLabel.fontsize = 16;
            coordinateLabel.material.depthTest = false;
            coordinateLabel.material.opacity = 1;
            coordinateLabel.visible = false;
            this.coordinateLabels.push(coordinateLabel);
            this.add(coordinateLabel);
        }

        // GAL -> Event listeners are now named so that we can remove them.

        { // Event Listeners
            /*sphere.drag = (e) => {
                let I = Utils.getMousePointCloudIntersection(
                    e.drag.end, 
                    e.viewer.scene.getActiveCamera(), 
                    e.viewer, 
                    e.viewer.scene.pointclouds,
                    {pickClipped: true});

                if (I) {
                    let i = this.spheres.indexOf(e.drag.object);
                    if (i !== -1) {
                        let point = this.points[i];
                        
                        // loop through current keys and cleanup ones that will be orphaned
                        for (let key of Object.keys(point)) {
                            if (!I.point[key]) {
                                delete point[key];
                            }
                        }

                        for (let key of Object.keys(I.point).filter(e => e !== 'position')) {
                            point[key] = I.point[key];
                        }

                        this.setPosition(i, I.location);
                    }
                }
            };

            sphere.drop = e => {
                let i = this.spheres.indexOf(e.drag.object);
                if (i !== -1) {
                    this.dispatchEvent({
                        'type': 'marker_dropped',
                        'measurement': this,
                        'index': i
                    });
                }
            };

            sphere.mouseover = (e) => e.object.material.emissive.setHex(0x888888);
            sphere.mouseleave = (e) => e.object.material.emissive.setHex(0x000000);

            sphere.addEventListener('drag', sphere.drag);
            sphere.addEventListener('drop', sphere.drop);
            sphere.addEventListener('mouseover', sphere.mouseover);
            sphere.addEventListener('mouseleave', sphere.mouseleave);*/
        }

        let event = {
            type: 'marker_added',
            measurement: this,
            sphere: sphere
        };
        this.dispatchEvent(event);

        this.setMarker(this.points.length - 1, point);
    };

    removeMarker(index) {
        this.points.splice(index, 1);

        this.remove(this.spheres[index]);

        let edgeIndex = (index === 0) ? 0 : (index - 1);
 
        if (edgeIndex == 0 && this.edges.length > 1) {
            this.edges[1].visible = false;
            this.edgeLabels[1].visible = false;
        }
        
        this.remove(this.edges[edgeIndex]);
        this.edges.splice(edgeIndex, 1);

        this.remove(this.edgeLabels[edgeIndex]);
        this.edgeLabels.splice(edgeIndex, 1);
        this.coordinateLabels.splice(index, 1);

        this.remove(this.angleLabels[index]);
        this.angleLabels.splice(index, 1);

        this.spheres.splice(index, 1);

        this.update();

        this.dispatchEvent({type: 'marker_removed', measurement: this});
    };
} 