import { Utilities } from "../utilities/Utilities.js";

/**
 * A wrapper for the Potree height profile class. (http://potree.org/potree/examples/elevation_profile.html)
 * 
 * @param {Array} position - The position of the box volume markers ([ [x,y,z] ]).
 * 
 * @param {Object} config - Configures the measurement.
 * @param {string} config.scale - Sets the width of the box volume.
 * @param {boolean} config.visible - Sets the visibility of the volume.
 * @param {boolean} config.showVolumeLabel - Determines whether or not the volume label should be shown.
 * 
 * @property {string} Type - Indicates the type of the object ("PotreeBoxVolume").
 * @example
 *   await Sigma3D.initialize();
 *
 *  let pms = new Sigma3D.PotreeMeasurementSource({
 *      volumes: [
 *          new Sigma3D.PotreeBoxVolume([ [2,2,2] ], { scale: [1,1,1] })
 *      ]
 *  });
 *
 *  let map = new Sigma3D.Map({
 *      view: new Sigma3D.View({}),
 *      layers: [
 *          new Sigma3D.PotreeMeasurementLayer({
 *              source: pms
 *          }),
 *      ]
 *  });
 *
 *  await Sigma3D.setMap(map);
 * 
 * @category PotreeMeasurement
 * @class PotreeBoxVolume
 *
 */

export class PotreeBoxVolume extends Utilities.Potree.BoxVolume {
    constructor( [ [x, y, z] ], config) {
        super();
        this.position.set(x,y,z);

        this.visible = true;
        this.showVolumeLabel = true;

        if (config) {
            if (config.scale) this.scale.set(config.scale[0], config.scale[1], config.scale[2]);
            if (config.visible) this.visible = config.visible;
            if (config.showVolumeLabel) this.showVolumeLabel = config.showVolumeLabel
        }

        this.Type = "PotreeBoxVolume";
        this.update();
    }
}