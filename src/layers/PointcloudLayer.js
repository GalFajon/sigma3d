import { Layer } from './Layer.js'
import { Utilities } from '../utilities/Utilities.js'

/**
 * Indicates a layer that is bound to a PointcloudSource.
 * 
 * @param {Object} config - Configures the layer.
 * @param {Sigma3D.Source} config.source - The source the layer is bound to.
 * 
 * @property {string} Type - Indicates the layer type.
 * @category Layer
 * @class
 */
class PointcloudLayer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "PointcloudLayer";
        this.Source = config.source;
    }
}

export { PointcloudLayer }