import { Layer } from './Layer.js'

/**
 * Indicates a layer that is bound to an OrientedImageSource.
 * 
 * @param {Object} config - Configures the layer.
 * @param {Sigma3D.Source} config.source - The source the layer is bound to.
 * 
 * @property {string} Type - Indicates the layer type.
 * @category Layer
 * @class
 */
export class OrientedImageLayer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "OrientedImageLayer";
        this.Source = config.source;
    }

    /**
     * Hides the layer.
     * @function
     */
    hide() {
        this.visible = false;

        for (let imageSet of this.Source.ImageSets) {
            for (let image of imageSet.images) {
                if (!image.focused && !image.group) {
                    image.line.visible = false;
                    image.mesh.visible = false;
                    image.clickable = false;
                }
                else if(image.group && !image.focused) {
                    image.line.visible = image.group.visible;
                    image.mesh.visible = image.group.visible;
                    image.clickable = image.group.visible;
                }
            }
        }
    }
    
    /**
     * Shows the layer.
     * @function
     */
    show() {
        this.visible = true;

        for (let imageSet of this.Source.ImageSets) {
            for (let image of imageSet.images) {
                if (!image.group) {
                    image.line.visible = true;
                    image.mesh.visible = true;
                    image.clickable = true;
                }
                else if (image.group) {
                    image.line.visible = image.group.visible;
                    image.mesh.visible = image.group.visible;
                    image.clickable = image.group.visible;
                }
            }
        }
    }

    hideAll() {
        for (let group in this.Source.Groups) this.Source.Groups[group].visible = false;
        this.hide();
    }

    showAll() {
        for (let group in this.Source.Groups) this.Source.Groups[group].visible = true;
        this.show();
    }
}