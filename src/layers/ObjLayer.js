import { Layer } from './Layer.js'

export class ObjLayer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "ObjLayer";
        this.Source = config.source;
    }

}