import { Layer } from './Layer.js'

class OverlayLayer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "OverlayLayer";
        this.Source = config.source;
    }

    /**
     * Toggles whether or not the layer is visible.
     * @function
     */
    toggle() {
        if (this.visible) this.hide();
        else this.show();
    }
    
    /**
     * Hides the layer.
     * @function
     */
    hide() {
        this.visible = false;
        this.Source.UseVisibilityDistance = false;

        if (this.Source.Models !== undefined && this.Source.Models !== null) {
            for (let Model of this.Source.Models) Model.visible = false;
        }
    }

    /**
     * Shows the layer.
     * @function
     */
    show() {
        this.visible = true;
        this.Source.UseVisibilityDistance = true;

        if (this.Source.Models !== undefined && this.Source.Models !== null) {
            for (let Model of this.Source.Models) {
                if (!this.Source.FocusedImage || Model == this.Source.FocusedImage) Model.visible = true;
            }
        }
    }
}

export { OverlayLayer }