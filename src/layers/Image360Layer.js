import { Layer } from './Layer.js'

/**
 * Indicates a layer that is bound to an Image360Source.
 * 
 * @param {Object} config - Configures the layer.
 * @param {Sigma3D.Source} config.source - The source the layer is bound to.
 * 
 * @property {string} Type - Indicates the layer type.
 * @category Layer
 * @class
 */
export class Image360Layer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "Image360Layer";   
        this.Source = config.source;

        this.selectingEnabled = true;
    }

    hide() {
        this.visible = false;

        if (this.Source.Models !== undefined && this.Source.Models !== null) {
            for (let Model of this.Source.Models) Model.visible = false;
        }

        if (this.selectingEnabled) for (let imageSet of this.Source.ImageSets) imageSet.selectingEnabled = false;
        this.selectingEnabled = false;
    }

    show() {
        this.visible = true;

        if (this.Source.Models !== undefined && this.Source.Models !== null) {
            for (let Model of this.Source.Models) {
                if (!this.Source.FocusedImage || Model == this.Source.FocusedImage) Model.visible = true;
            }
        }

        if (!this.selectingEnabled) for (let imageSet of this.Source.ImageSets) imageSet.selectingEnabled = true;
        this.selectingEnabled = true;
    }
}