import { Layer } from './Layer.js'

export class PlyLayer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "PlyLayer";
        this.Source = config.source;
    }
}