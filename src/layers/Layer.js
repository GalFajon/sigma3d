/**
 * The class all other layer types inherit from. Not to be used by itself.
 * 
 * @param {Object} config - Configures the layer.
 * @param {boolean} config.visible - Determines whether or not the layer is visible.
 * 
 * @property {boolean} visible - Indicates whether or not the layer is currently visible.
 * @property {Array} Models - An array of models contained by the layer.
 * @category Layer
 * @class
 */

class Layer {
    constructor( config ) {
        if (config.visible == true) {
            this.visible = config.visible;
            this.show();
        }
        else if( config.visible == false) {
            this.visible = config.visible;
            this.hide();
        }
        else 
            this.visible = true;
    }

    /**
     * Toggles whether or not the layer is visible.
     * @function
     */
    toggle() {
        if (this.visible)
            this.hide();
        else
            this.show();
    }

    /**
     * Hides the layer.
     * @function
     */
    hide() {
        this.visible = false;

        if (this.Source.Models !== undefined && this.Source.Models !== null) {
            for (let Model of this.Source.Models) Model.visible = false;
        }
    }

    /**
     * Shows the layer.
     * @function
     */
    show() {
        this.visible = true;

        if (this.Source.Models !== undefined && this.Source.Models !== null) {
            for (let Model of this.Source.Models) {
                if (!this.Source.FocusedImage || Model == this.Source.FocusedImage) Model.visible = true;
            }
        }
    }

}

export { Layer }