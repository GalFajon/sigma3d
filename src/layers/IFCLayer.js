import { Layer } from './Layer.js'

/**
 * Indicates a layer that is bound to an IFCSource.
 * 
 * @param {Object} config - Configures the layer.
 * @param {Sigma3D.Source} config.source - The source the layer is bound to.
 * 
 * @property {string} Type - Indicates the layer type.
 * @category Layer
 * @class
 */

export class IFCLayer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "IFCLayer";
        this.Source = config.source;
    }

        /**
         * Hides the layer.
         * @function
         */
        hide() {
            this.visible = false;

            if (this.Source.Models !== undefined && this.Source.Models !== null) for (let Model of this.Source.Models) Model.visible = false;
            if (this.Source.Outlines !== undefined && this.Source.Outlines !== null) for (let Outline of this.Source.Outlines) Outline.visible = false;
        }
    
        /**
         * Shows the layer.
         * @function
         */
        show() {
            this.visible = true;

            if (this.Source.Models !== undefined && this.Source.Models !== null) for (let Model of this.Source.Models) Model.visible = true;
            if (this.Source.Outlines !== undefined && this.Source.Outlines !== null) for (let Outline of this.Source.Outlines) Outline.visible = true;
        }
}