import { Layer } from './Layer.js'

/**
 * Indicates a layer that is bound to a GeometrySource.
 * 
 * @param {Object} config - Configures the layer.
 * @param {Sigma3D.Source} config.source - The source the layer is bound to.
 * 
 * @property {string} Type - Indicates the layer type.
 * @category Layer
 * @class
 */

export class GeometryLayer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "GeometryLayer";
        this.Source = config.source;
    }

    /**
     * Toggles the lines that surround polygons.
     * @function
     */
    togglePolygonSurroundingLines() {
        this.Source.togglePolygonSurroundingLines();
    }

    /**
     * Hides the layer.
     * @function
     */
    hide() {
        this.visible = false;

        for (const [key, value] of this.Source.PointsCloud) value.visible = false;

        if (this.Source.Models !== undefined && this.Source.Models !== null) {
            for (let Model of this.Source.Models) Model.visible = false;
        }
    }

    /**
     * Shows the layer.
     * @function
     */
    show() {
        this.visible = true;

        for (const [key, value] of this.Source.PointsCloud) value.visible = true;

        if (this.Source.Models !== undefined && this.Source.Models !== null) {
            for (let Model of this.Source.Models) {
                if (!this.Source.FocusedImage || Model == this.Source.FocusedImage) Model.visible = true;
            }
        }
    }
}