import { Layer } from './Layer.js'

/**
 * Indicates a layer that is bound to a PotreeMeasurementSource.
 * 
 * @param {Object} config - Configures the layer.
 * @param {Sigma3D.Source} config.source - The source the layer is bound to.
 * 
 * @property {string} Type - Indicates the layer type.
 * @category Layer
 * @class
 */
class PotreeMeasurementLayer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "PotreeMeasurementLayer";
        this.Source = config.source;
    }
}

export { PotreeMeasurementLayer }