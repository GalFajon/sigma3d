export class EventDispatcher {
    constructor() {
        this.EventNode = document.createElement('div');
    }

    addEventListener(name, callback) {
        this.EventNode.addEventListener(name, callback);
    }

    removeEventListener(name, callback) {
        this.EventNode.removeEventListener(name, callback);
    }

    dispatchEvent(event) {
        this.EventNode.dispatchEvent(event);
    }
}