import * as THREE from '../../libs/threejs/three.module.js'

import {LineGeometry} from '../../libs/threejs/fatlines/LineGeometry.js'

// -- Geometry geometries --
//export const PointGeometry = new THREE.BufferGeometry();
//export const HeightPointGeometry = new THREE.BufferGeometry();

export const SymbolGeometry = new THREE.PlaneGeometry();

export {LineGeometry};

export const SnapPointGeometry = new THREE.BoxGeometry( 1, 1, 1 );

// -- Text (such as that used on HeightPoint labels) --
export const TextGeometry = new THREE.PlaneBufferGeometry(0.5,0.2);

// -- Cursor3D --