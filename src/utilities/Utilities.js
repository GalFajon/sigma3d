import { IFCLoader } from '../../libs/threejs/ifc/IFCLoader.js';
import { CSS2DRenderer } from '../../libs/threejs/utils/CSS2DRenderer.js';

// Custom controls for Potree. The default for all maps unless otherwise specified.
import { PLYLoader } from '../../libs/ply/PLYLoader.js';
import { noCloudControls } from '../../libs/potree/noCloudControls.js';
import { Cursor3D } from './Cursor3D.js';
import { Line2 } from '../../libs/threejs/fatlines/Line2.js';
import { OBJLoader } from '../../libs/threejs/obj/OBJLoader.js';
import { MTLLoader } from '../../libs/threejs/obj/MTLLoader.js';
import { ConvexGeometry } from '../../libs/threejs/convex/ConvexGeometry.js'
import * as THREE from '../../libs/threejs/three.module.js';

/**
 * A global variable (Sigma3D.Utilities) containing several tools used by the library.
 *
 * @property {IFCLoader} IFCLoader - A loader used for adding ".ifc" files to a THREE scene.
 * @property {GeoTIFF} GeoTIFF - A library used for processing GeoTIFF images.
 * @property {CSS2DRenderer} CSS2DRenderer - Used for rendering Overlays.
 * @property {Potree} Potree - Library, used for displaying pointclouds and configuring the scene.
 * @property {turf} turf - Library, used for geospatial calculations.
 * @property {PotreeViewer} PotreeViewer - The viewer class of Potree. Contains things like the THREE.js camera being used, the active controls of the scene, etc.
 * @property {noCloudControls} noCloudControls - A modified version of Potrees "ground controls" class.
 * @property {Line2} Line2 - Used for drawing thicker lines in THREE.js.
 * @category Terrain
 * @class
 */
let Utilities = {
    // used for loading IFC models
    IFCLoader: new IFCLoader(),
    PLYLoader: new PLYLoader(),
    OBJLoader: new OBJLoader(),
    MTLLoader: new MTLLoader(),

    // used for terrain loading.
    GeoTIFF: GeoTIFF,

    // Used for appending DOM elements to the scene.
    CSS2DRenderer: new CSS2DRenderer(),

    // Potree is already globally declared
    Potree: Potree,
    turf: turf,

    PotreeViewer: new Potree.Viewer(document.getElementById('potree_render_area'), { noDragAndDrop: true }),
    noCloudControls: noCloudControls,

    // fatlines
    ConvexGeometry: ConvexGeometry,
    Line2: Line2,
    textureLoader: new THREE.TextureLoader(),

    overlayScene: new THREE.Scene(),

    // raycaster
    Raycaster: new THREE.Raycaster(),
    Nexus: window.Nexus,
}

Utilities.initialize = function initialize() {
    return new Promise((resolve, reject) => {
        // Allows raycasting sprites
        Utilities.Raycaster.camera = Utilities.PotreeViewer.scene.getActiveCamera();
        Utilities.Raycaster.params.Points.threshold = 0.1;
        Utilities.Raycaster.params.Line.threshold = 0.1;

        // 3D cursor, used for interactions
        Utilities.Cursor3D = new Cursor3D({
            potreeViewer: Utilities.PotreeViewer,
            domElement: Utilities.PotreeViewer.renderer.domElement,
            potree: Utilities.Potree,
            raycaster: Utilities.Raycaster
        }),

            // Utility configuration.
            Utilities.IFCLoader.ifcManager.setWasmPath('./');

        // Configure the Potree viewer
        Utilities.PotreeViewer.setEDLEnabled(true);
        Utilities.PotreeViewer.setFOV(60);
        Utilities.PotreeViewer.setPointBudget(500_000);
        Utilities.PotreeViewer.loadSettingsFromURL();

        Utilities.PotreeViewer.setBackground('none');
        Utilities.PotreeViewer.renderer.domElement.style.backgroundColor = '#ededed';

        Utilities.PotreeViewer.noCloudControls = new Utilities.noCloudControls(Utilities.PotreeViewer);
        Utilities.PotreeViewer.setControls(Utilities.PotreeViewer.noCloudControls);
        Utilities.Controls = Utilities.PotreeViewer.noCloudControls;

        Utilities.PotreeViewer.loadGUI(() => {
            Utilities.PotreeViewer.setLanguage('en');
            //$("#menu_tools").next().show();
            //$("#menu_scene").next().show();
            //Utilities.PotreeViewer.toggleSidebar();

            if (document.getElementById('potree_quick_buttons') != undefined) {
                document.getElementById('potree_quick_buttons').style.display = 'none';
            }

            let container = document.getElementById('potree_render_area').getBoundingClientRect();
            Utilities.PotreeViewer.renderer.setSize(container.width, container.height);

            // CSS2DRenderer
            Utilities.CSS2DRenderer.setSize(container.width, container.height);
            Utilities.PotreeViewer.renderer.domElement.parentElement.appendChild(Utilities.CSS2DRenderer.domElement);

            // Potree is written in such a way that it often assumes "viewer" is a global variable.
            // I'll try to fix it as I go along, but just in case:
            window.viewer = Utilities.PotreeViewer;
            resolve();
        });
    });
}

export { Utilities }