export class ImageGenerator {
    static generateImage(source) {
        return new Promise( async (resolve, reject) => {
            try {
              let response = await fetch(source).then( (response) => {
                if (response.ok) {
                  let img = new Image();
                  img.crossOrigin = "anonymous";
                  img.src = source;
                  img.onload = () => resolve(img);
                }
                else resolve(undefined);
              })
              .catch( (error) => { console.log(error); resolve(undefined); } );
            }
            catch (error) { console.log(error); resolve(undefined);}
        })
    }
}