import * as THREE from '../../libs/threejs/three.module.js'

import { LineMaterial as LineMaterialClass } from '../../libs/threejs/fatlines/LineMaterial.js'

const lineResolution = new THREE.Vector2( 1000, 1000);

// -- Geometry Materials --
export const PointMaterial = new THREE.PointsMaterial( { 
    color: '#C41E3A', 
    size: 0.1, 
    sizeAttenuation: true,
});

export const DrawHelperPointMaterial = new THREE.PointsMaterial( { color: '#0000FF', size: 0.1, depthTest: false, depthWrite: false } );
export const PointSelectionMaterial = new THREE.PointsMaterial( { 
    color: '#99ff99', 
    size: 0.2,
});

export const HeightPointMaterial = new THREE.PointsMaterial({ 
    color: '#f0a37a', 
    size: 0.15,
});
export let CustomMaterials = [];

export const SymbolMaterialCache = new Map();
SymbolMaterialCache.set('default', PointMaterial);
SymbolMaterialCache.set('highlighted', PointSelectionMaterial);
SymbolMaterialCache.set('height', HeightPointMaterial);

export const LineMaterial = new LineMaterialClass( {
    color: 0xff0000,
    linewidth: 3, // in pixels
    resolution:  new THREE.Vector2(1000, 1000),
    dashed: false,
    depthTest: true,
    depthWrite: true
} );

export const PotreeLineMaterial = new LineMaterialClass( {
    color: 0xff0000,
    linewidth: 3, // in pixels
    resolution:  new THREE.Vector2(1000, 1000),
    dashed: false,
    depthTest: true,
    depthWrite: true
} );

export const PolygonSurroundingLineMaterial = new LineMaterialClass({
    color: 'black',
    linewidth: 1, // in pixels
    resolution:  new THREE.Vector2(1000, 1000),
    dashed: false,
    depthTest: true,
    depthWrite: true
})
//export const LineMaterial = new LineMaterialClass( { color: 'red', linewidth: 5, vertexColors: false, resolution: lineResolution,dashed: false, alphaToCoverage: true });

export const PolygonMaterial = new THREE.MeshBasicMaterial( {color: 'purple', side: THREE.DoubleSide } );
export const PolygonCentroidMaterial = new THREE.MeshBasicMaterial({color: 'white'});

// -- Interaction materials --
export const MeshSelectionMaterial = new THREE.MeshBasicMaterial( {color: 'lightgreen', side: THREE.DoubleSide, transparent: false } );
export const LineSelectionMaterial = new LineMaterialClass( { color: 'lightgreen', linewidth: 5, vertexColors: false, resolution: lineResolution, dashed: false, alphaToCoverage: true });

// Selected IFC Subset
export const IFCSelectionMaterial = new THREE.MeshPhongMaterial( { color: 'lightgreen', transparent: true, opacity: 0.9 } );

// -- DrawHelper materials --
export const DrawHelperMeshMaterial = new THREE.MeshBasicMaterial( {color: 'blue', transparent: false } );
export const DrawHelperLineMaterial = new LineMaterialClass( { color: 'blue', linewidth: 5, vertexColors: false, resolution: lineResolution, dashed: false, alphaToCoverage: true });

// -- Cursor3D material --
function generateSpriteTexture(color) {
    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext("2d");
    
    canvas.width = 64;
    canvas.height = 64;
    canvas.style.width = 64 + "px";
    canvas.style.height = 64 + "px";
    
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    ctx.strokeStyle = 'black';
    if (color) ctx.fillStyle = color;
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.arc( canvas.width / 2, canvas.height / 2, canvas.width / 2 - 3, 0, 2 * Math.PI, false);
    if (color) ctx.fill();
    ctx.stroke();
    
    let texture = new THREE.Texture( canvas );
    texture.needsUpdate = true;
    
    canvas.remove();
    return texture;
}

export const Cursor3DMaterial = new THREE.SpriteMaterial( { map: generateSpriteTexture('black'), depthTest: false, depthWrite: false, transparent:true } );
export const Cursor3DSnapMaterial = new THREE.MeshBasicMaterial( { map: generateSpriteTexture(undefined), depthTest: false, depthWrite: false, transparent:true } );
// -- SnapPoint material --
export const SnapPointMaterial = new THREE.PointsMaterial( { color: '#282828', size: 0.1, transparent:true, opacity: 0 } );
export const SnapLineMaterial = new THREE.LineBasicMaterial( {color: 'yellow', transparent:true, opacity:0 } );

// -- IFCOutline --
export const IFCOutlineMaterial = new THREE.LineBasicMaterial( { color: 0x000000 } );