import {Cursor3DMaterial, Cursor3DSnapMaterial} from './Materials.js';

import { CSS2DObject } from "../../libs/threejs/utils/CSS2DRenderer.js";

import * as THREE from '../../libs/threejs/three.module.js';
import { Utilities } from './Utilities.js';

export class Cursor3D {
    
    constructor(config) {
        
        this.MousePosition = [0,0,0]; // independent of snapping
        this.Position = [0,0,0] // [x,y,z]
        this.Type = 'Cursor3D';

        if (config) {
            if (config.potreeViewer) this.PotreeViewer = config.potreeViewer
            if (config.potree) this.Potree = config.potree;
            if (config.domElement) this.DomElement = config.domElement;
            if (config.raycaster) this.RayCaster = config.raycaster;
        }

        this.UpdateOwnPosition = true;
        
        this.MovedMouse = false;
        this.Snapped = false;
        this.SnappedObject = undefined;
        this.model = new THREE.Sprite(Cursor3DMaterial)
        
        this.cursorHelperElement = this.generateCursorHelper();
        this.heightHelperElement = this.generateHeightHelper();

        this.cursorHelperModel = new CSS2DObject(this.cursorHelperElement);
        this.heightHelperModel = new CSS2DObject(this.heightHelperElement);
        this.model.userData = this;

        this.SnapDistance = 1;
        this.pointSnapDistance = 0.3;
        this.snapInteractions = []

        this.Map = undefined;

        this.showHeightHelper();
    }

    attachToScene(scene) {
        scene.add(this.model);
        scene.add(this.cursorHelperModel);
        scene.add(this.heightHelperModel);
    }

    initializeEvents(map) {
        this.Map = map;

        this.DomElement.addEventListener('mousemove', (event) => {
            this.updatePosition(event);
            this.MovedMouse = true;
        })

        this.DomElement.addEventListener('pointerdown', (event) => {
            this.updateHeight(event);
            this.MovedMouse = false;
        })
    }

    generateCursorHelper() {
        let dot = document.createElement('span');

        dot.style.height = '5px';
        dot.style.width = '5px';
        dot.style.backgroundColor = 'white';
        dot.style.borderRadius = '50%';
        dot.style.display = 'inline-block';
        dot.style.pointerEvents = 'none';
        dot.style.border = '0.5px solid black';
        return dot;
    }

    generateHeightHelper() {        
        let paragraph = document.createElement('p');

        paragraph.innerText = this.model.position.z;
        paragraph.style.color = 'white';
        paragraph.style.textShadow = '-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black';
        paragraph.style.pointerEvents = 'none';
        paragraph.style.paddingTop = '50px';
        paragraph.style.paddingLeft = '50px';
        
        return paragraph;
    }

    updateModelPosition() {
        this.model.position.set(this.Position[0], this.Position[1], this.Position[2]);
        this.heightHelperElement.innerText = 'z: ' + Math.round(this.Position[2] * 100) / 100;
        this.cursorHelperModel.position.set(this.Position[0], this.Position[1], this.Position[2]);
        this.heightHelperModel.position.set(this.Position[0], this.Position[1], this.Position[2]);
    }

    updatePosition(e) {
        try {
            let map = this.Map;
            let mouse = new THREE.Vector2(
                (e.layerX / this.DomElement.getBoundingClientRect().width) * 2 - 1, // x
                -(e.layerY / this.DomElement.getBoundingClientRect().height ) * 2 + 1 // y
            );

            let snapRaycastList = [];
            let snapPointRaycastList = []
            let snapPointClouds = [];
            let snapPointList = []

            for (let interaction of map.Interactions) {
                if (interaction.Type == "SnapInteraction") {
                    if (interaction.Active == true) {
                        if (interaction.Target == undefined || !interaction.Target.beingModified) {
                            snapPointList.push(interaction.SnapPoints);
                            snapPointRaycastList.push(interaction.SnapPointCloud);
                            snapRaycastList.push(...interaction.SnapLines.map( line => line.model ));
                            for (let source of interaction.ParentSources) if (source.Type == 'PointcloudSource') snapPointClouds.push(...source.Pointclouds);
                        }
                    }
                }
            }

            let snappedToPoint = false;
            let snappedToLine = false;
            let snappedToPtcld = false;

            for (let i=0; i < snapPointRaycastList.length; i++) {
                if (snapPointList[i].length > 0) {
                    let pointIntersects = this.getMouseIntersect(mouse, [ snapPointRaycastList[i] ]);

                    if(pointIntersects && pointIntersects[0]) {
                        let p = new THREE.Vector3(...snapPointList[i][pointIntersects[0].index].Coordinates);

                        if (pointIntersects[0].index !== undefined && p.distanceTo(pointIntersects[0].point) < this.pointSnapDistance) {
                            this.snapTo( snapPointList[i][pointIntersects[0].index].Coordinates, snapPointList[i][pointIntersects[0].index].RefersTo);
                            snappedToPoint = true;
                            break;
                        }
                    }
                }
            }

            if (!snappedToPoint) {
                let intersected = this.getMouseIntersect(mouse,snapRaycastList);

                if (intersected && intersected[0]) {
                    if (intersected[0].object.userData.Type == 'SnapLine') {
                        this.snapTo([intersected[0].point.x, intersected[0].point.y, intersected[0].point.z], intersected[0].object.userData.RefersTo);
                        snappedToLine = true;
                    }
                }
            }

            if (!snappedToPoint && !snappedToLine) {
                let pointcloudIntersection = Utilities.Potree.Utils.getMousePointCloudIntersection(
                    Utilities.PotreeViewer.inputHandler.mouse, 
                    Utilities.PotreeViewer.scene.getActiveCamera(), 
                    Utilities.PotreeViewer, 
                    snapPointClouds
                );
        
                if (pointcloudIntersection) {
                    this.snapTo([pointcloudIntersection.location.x, pointcloudIntersection.location.y, pointcloudIntersection.location.z]);
                    snappedToPtcld = true;
                }
            }

            if (!snappedToPoint && !snappedToPtcld && !snappedToLine) this.unsnap();

            if (!this.Snapped) {
                let vector = new THREE.Vector3(mouse.x,mouse.y,0.5);
                vector.unproject( this.PotreeViewer.scene.getActiveCamera().clone() );
                vector.sub( this.PotreeViewer.scene.getActiveCamera().clone().position ).normalize();
                
                let targetZ = this.Position[2];
        
                let distance = (targetZ - this.PotreeViewer.scene.getActiveCamera().clone().position.z) / vector.z;
                let position = (this.PotreeViewer.scene.getActiveCamera().clone().position).add( vector.multiplyScalar( distance ) );

                this.MousePosition = [position.x, position.y, position.z];
                this.Position = [position.x, position.y, position.z];                 
                this.updateModelPosition();
            }
        }
        catch (err) { console.log(err) }
    }

    updateHeight(e) {
        let map = this.Map;
        let mouse = new THREE.Vector2(
            (e.layerX / this.DomElement.getBoundingClientRect().width) * 2 - 1, // x
            -(e.layerY / this.DomElement.getBoundingClientRect().height ) * 2 + 1 // y
        );

        this.RayCaster.setFromCamera( mouse, this.PotreeViewer.scene.getActiveCamera() );
        
        try {
            if (!this.Snapped) {
                let I = undefined;

                if (Utilities.PotreeViewer.scene.pointclouds.length > 0) {
                    I = Potree.Utils.getMousePointCloudIntersection(
                        e, 
                        Utilities.PotreeViewer.scene.getActiveCamera(), 
                        Utilities.PotreeViewer, 
                        Utilities.PotreeViewer.scene.pointclouds, 
                        {pickClipped: false}
                    );
                }

                const raycastList = [];

                for (let source of map.Sources) {
                    if ((source.Type == "IFCSource" || source.Type == "ObjSource" || source.Type == "PlySource" || source.Type == "NexusSource" || source.Type == "TerrainSource" || source.Type == "WMSTerrainSource") && source.Models) {
                        for (let model of source.Models) raycastList.push(model);
                    }
                }

                let intersected = this.RayCaster.intersectObjects( raycastList, true );

                let vector = new THREE.Vector3(mouse.x,mouse.y,0.5);
                vector.unproject( this.PotreeViewer.scene.getActiveCamera().clone() );
                vector.sub( this.PotreeViewer.scene.getActiveCamera().clone().position ).normalize();
                
                let targetZ;

                if (intersected && intersected[0] && intersected[0].point.z !== 0) targetZ = intersected[0].point.z;
                else if (I) targetZ = I.location.z;
                else targetZ = this.Position[2];

                let distance = (targetZ - this.PotreeViewer.scene.getActiveCamera().clone().position.z) / vector.z;
                let position = (this.PotreeViewer.scene.getActiveCamera().clone().position).add( vector.multiplyScalar( distance ) );

                this.MousePosition = [position.x, position.y, position.z];
                this.Position = [position.x, position.y, position.z];                 
                this.updateModelPosition();
            }
        }
        catch (err) {}
    }

    getMouseIntersect(mouse,list) {
        this.RayCaster.setFromCamera( mouse, this.PotreeViewer.scene.getActiveCamera() );

        let intersected = this.RayCaster.intersectObjects(list, false);
        return intersected;
    }

    snapTo([x,y,z], object) {
        this.UpdateOwnPosition = false;
        
        this.Snapped = true;
        this.SnappedObject = object;

        this.Position = [x,y,z];
        this.MousePosition = [x,y,z];
        this.updateModelPosition();

        this.model.material = Cursor3DSnapMaterial;
    }

    unsnap() {
        this.Snapped = false;
        this.UpdateOwnPosition = true;        
        this.SnappedObject = undefined;
        this.model.material = Cursor3DMaterial;
    }

    showHeightHelper() {
        this.heightHelperModel.visible = true;
    }  

    hideHeightHelper() {
        this.heightHelperModel.visible = false;
    }
}