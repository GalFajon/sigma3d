import * as THREE from '../libs/threejs/three.module.js';
import { Utilities } from './utilities/Utilities.js';
import { EventDispatcher } from './utilities/EventDispatcher.js';

import { SymbolMaterialCache } from './utilities/Materials.js';

/**
 * Describes a map the user may view using the library.
 * 
 * @param {Object} config - Configures the view.
 * @param {Array} config.layers - Contains layers used by the map.
 * @param {Array} config.interactions - Contains interactions used by the map.
 * @param {Object} config.view - Sets the view to be used by the map.
 * 
 * @property {Array} Layers - Contains every layer in use by the map. 
 * @property {Array} Interactions - Contains every interaction in use by the map. 
 * @property {Array} Sources - Contains all sources used by layers on the map.
 * @property {THREE.AmbientLight} Light - An instance of THREE.AmbientLight used to cast light on models.
 * 
 * @category Viewer
 * 
 * @example
 *   await Sigma3D.initialize();
 *
 *   const geomSource = new Sigma3D.GeometrySource({
 *       geometries: [
 *           new Sigma3D.Line([ [0,0,0], [-2,6,0] ]),
 *           new Sigma3D.Polygon(
 *               [
 *                   [ [0,0,0], [0,50,0], [50,50,0], [50,0,0] ],
 *                   [ [10,10,0], [10,20,0], [20,20,0], [20,10,0] ]
 *               ]
 *           )
 *       ]
 *   });
 *   
 *   const map = new Sigma3D.Map({
 *       view: new Sigma3D.View({}),
 *
 *       layers: [
 *           new Sigma3D.GeometryLayer({
 *               source: geomSource
 *           })
 *       ]
 *   });
 *
 *
 *   Sigma3D.setMap(map);
 * 
 * @class
 */

class Map extends EventDispatcher {
    constructor( config ) {
        super();

        this.Layers = [];
        this.Interactions = [];

        if (config) {
            if (config.layers) this.Layers = config.layers;
            if (config.interactions) this.Interactions = config.interactions;
            if (config.view) this.CurrentView = config.view;
            if (config.style) this.Style = config.style;
            else this.Style = { PointMinScale: 0.03, PointMaxScale: 1 };
        }

        this.Sources = [];
        this.Light = new THREE.AmbientLight();
        
        this.overlayScene = Utilities.overlayScene;

        this.everyFrame();

        window.addEventListener('resize', (e) => {
            let container = document.getElementById('potree_render_area').getBoundingClientRect();
            Utilities.PotreeViewer.renderer.setSize( container.width, container.height );    
            Utilities.CSS2DRenderer.setSize( container.width, container.height );
        })
    }

    /**
     * Initializes the layers, sources, interactions and overlays of the map as well as the 3D cursor.
     * @private
     * @function
     */
    async initialize() {       
        this.CurrentView.zoomToPosition( [0,0,0] );

        await this.initializeLayers(this.Layers);
        await this.initializeInteractions(this.Interactions);

        const customEvent = new CustomEvent('loaded', { map: this });
        this.dispatchEvent(customEvent);

        this.initializeCursor3D();
    }

    enableDepthTestingOnGeometries() {
        this.testingGeometryDepth = true;
        for (let Source of this.Sources) {
            if (Source.Type == "GeometrySource") {
                for (const [key, value] of Source.PointsCloud) {
                    Utilities.PotreeViewer.scene.scene.add(value);
                }
                for (let model of Source.Models) {
                    Utilities.PotreeViewer.scene.scene.add(model);
                }
                Source.ThreeScene = Utilities.PotreeViewer.scene.scene;
            }
        }
    }

    disableDepthTestingOnGeometries() {        
        this.testingGeometryDepth = false;
        for (let Source of this.Sources) {
            if (Source.Type == "GeometrySource") {
                for (const [key, value] of Source.PointsCloud) {
                    Utilities.PotreeViewer.scene.scene.remove(value);
                    this.overlayScene.add(value);
                }
                for (let model of Source.Models) {
                    Utilities.PotreeViewer.scene.scene.remove(model);
                    this.overlayScene.add(model);
                }
                Source.ThreeScene = this.overlayScene;
            }
        }
    }

    /**
     * Initializes the 3D cursor.
     * @private
     * @function
     */
    initializeCursor3D() {
        Utilities.Cursor3D.initializeEvents(this);
        Utilities.Cursor3D.attachToScene(this.overlayScene);
    }

    /**
     * Initializes layers.
     * @param {Array} layers - An array of layers.
     * @private
     * @function
     */
    async initializeLayers(layers) {
        if (layers) {
            for (let layer of layers) {
                await this.addLayer(layer, false)
            }
        }

        Utilities.PotreeViewer.scene.scene.add( this.Light );
    }

    /**
     * Initializes interactions.
     * @param {Array} interactions - An array of interactions.
     * @private
     * @function
     */
    async initializeInteractions(interactions) {
        if (interactions) {
            for (let interaction of interactions) {await this.addInteraction(interaction, false)}
        }
    }

    /**
     * Adds a layer to the map.
     * @param {Sigma3D.Layer} layer - A layer to be added to the map.
     * @param {boolean} addToArray - Determines whether or not the layer is added to the Layers array. True by default.
     * @function
     */
    async addLayer( layer, addToArray = true) {
        await layer.Source.load();

        this.Sources.push(layer.Source);

        if (addToArray) this.Layers.push(layer);
    }

    /**
     * Remove a layer from the map.
     * @param {Sigma3D.Layer} layer - A layer to be removed from the map.
     * @function
     */
    removeLayer( layer ) {
        this.Layers.splice( this.Layers.indexOf(layer), 1);

        layer.Source.removeFromScene();
    }

    /**
     * Adds an interaction to the map.
     * @param {Sigma3D.Interaction} interaction - An interaction to be added to the map.
     * @param {boolean} addToArray - Determines whether or not the interaction is added to the Interactions array. True by default.
     * @function
     */
    async addInteraction( interaction, addToArray = true ) {
        await interaction.initialize();

        if (addToArray) this.Interactions.push(interaction);
    }

    /**
     * Removes an interaction from the map.
     * @param {Sigma3D.Interaction} interaction - An interaction to be removed from the map.
     * @function
     */
    /*removeInteraction( interaction ) {
        if (this.Interactions.indexOf(interaction) != -1) {
            interaction.remove();
            this.Interactions.splice(this.Interactions.indexOf(interaction), 1);
        }
    }*/

    /**
     * Is executed every frame.
     * @private
     * @function
     */
    everyFrame() {
        if (this.CurrentView) {
            this.CurrentView.update();
            this.updateModels();
            this.updateTerrain();        
        }

        Utilities.PotreeViewer.renderer.clearDepth();
        Utilities.PotreeViewer.renderer.render( this.overlayScene, Utilities.PotreeViewer.scene.getActiveCamera() );
        Utilities.CSS2DRenderer.render( Utilities.PotreeViewer.scene.scene, Utilities.PotreeViewer.scene.getActiveCamera() );
        Utilities.CSS2DRenderer.render( this.overlayScene, Utilities.PotreeViewer.scene.getActiveCamera() );

        requestAnimationFrame(this.everyFrame.bind(this));

        for (let layer of this.Layers) {
            if (layer.visible == false) layer.hide();
            else layer.show();
        }
    }

    /**
     * Updates terrain geometry on WMSTerrain and Terrain sources.
     * @private
     * @function
     */
    updateTerrain() {
        for (let source of this.Sources) {
            if (source.Type == 'TerrainSource') source.update(this.CurrentView.Position);
            if (source.Type == "WMSTerrainSource") source.update();
        }
    }

    /**
     * Scales models found in layer sources, interactions, as well as the 3D cursor, based on their distance from the camera.
     * @private
     * @function
     */
    updateModels() {
        if (Utilities.Cursor3D) {
            let distance = Utilities.Cursor3D.model.position.distanceTo(Utilities.PotreeViewer.scene.view.position);
            let pr = Potree.Utils.projectedRadius(1, Utilities.PotreeViewer.scene.getActiveCamera(), distance, Utilities.PotreeViewer.clientwidth, Utilities.PotreeViewer.renderer.domElement.clientHeight);
            let scale = 10 / pr;

            if (scale > 3) scale = 2;

            if (Utilities.Cursor3D.Snapped) scale *= 3;

            Utilities.Cursor3D.model.scale.set(scale, scale, scale);
        }

        for (let source of this.Sources) {
            if (source.Type == 'OverlaySource' && source.UseVisibilityDistance) {
                for (let overlay of source.Overlays) {
                    if (overlay.model.position.distanceTo(Utilities.PotreeViewer.scene.view.position) > overlay.VisibilityDistance) overlay.model.visible = false;
                    else overlay.model.visible = true;
                }
            }

            if (source.Type == 'GeometrySource') {
                let distance = Utilities.PotreeViewer.scene.view.position.z - Utilities.Cursor3D.model.position.z;
                let pr = Potree.Utils.projectedRadius(1, Utilities.PotreeViewer.scene.getActiveCamera(), distance, Utilities.PotreeViewer.clientwidth, Utilities.PotreeViewer.renderer.domElement.clientHeight);
                let scale = 10 / pr;
                
                if (scale < this.Style.PointMinScale) scale = this.Style.PointMinScale;
                if (scale > this.Style.PointMaxScale) scale = this.Style.PointMaxScale;
                Utilities.Raycaster.params.Points.threshold = scale;
                
                for (let material of SymbolMaterialCache.values()) {
                    if (!material.userData.originalSize) material.userData.originalSize = material.size;
                    material.size = scale * material.userData.originalSize;
                }
            }
        }   
    }

    /**
     * Returns a THREE.Vector3() that determines the coordinates the camera is turned towards.
     * @private
     * @function
     */
    getWhereCameraIsLooking() {
        Utilities.Raycaster.setFromCamera( new THREE.Vector2(0,0), Utilities.PotreeViewer.scene.getActiveCamera() );

        let intersected = Utilities.Raycaster.intersectObjects(  Utilities.PotreeViewer.scene.scene.children, false );
        intersected = intersected.filter( intersection => intersection.object.type != 'AxesHelper' && intersection.object.userData.Type != 'Cursor3D' && !intersection.object.userData.beingModified);
        intersected = intersected.sort((intersection1, intersection2) => (intersection1.distance > intersection2.distance) ? 1 : -1);
        
        let vector = new THREE.Vector3();
        vector.set(0, 0, 0.5);
        vector.unproject( Utilities.PotreeViewer.scene.getActiveCamera().clone() );
        vector.sub( Utilities.PotreeViewer.scene.getActiveCamera().clone().position ).normalize();
        
        let targetZ;

        if (intersected && intersected[0] && intersected[0].point.z !== 0) {
            targetZ = intersected[0].point.z;
        }
        else {
            targetZ = 0;
        }

        let distance = (targetZ - Utilities.PotreeViewer.scene.getActiveCamera().clone().position.z) / vector.z;
        let position = (Utilities.PotreeViewer.scene.getActiveCamera().clone().position).add( vector.multiplyScalar( distance ) );

        return position
    }
}

export { Map }