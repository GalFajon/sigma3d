import { Map } from './Map.js';
import { View } from './View.js';

import { Utilities } from './utilities/Utilities.js'
import { EventDispatcher } from './utilities/EventDispatcher.js';
import * as Geometries from './utilities/Geometries.js'
import * as Materials from './utilities/Materials.js'

import { PointcloudLayer } from './layers/PointcloudLayer.js';
import { PotreeMeasurementLayer } from './layers/PotreeMeasurementLayer.js';
import { IFCLayer } from './layers/IFCLayer.js';
import { GeometryLayer } from './layers/GeometryLayer.js';
import { Image360Layer } from './layers/Image360Layer.js';
import { NexusLayer } from './layers/NexusLayer.js';
import { OrientedImageLayer } from './layers/OrientedImageLayer.js';
import { TerrainLayer } from './layers/TerrainLayer.js';
import { PlyLayer } from './layers/PlyLayer.js';
import { ObjLayer } from './layers/ObjLayer.js';

import { SelectInteraction } from './interactions/SelectInteraction.js';
import { DrawInteraction } from './interactions/DrawInteraction.js';
import { SnapInteraction } from './interactions/SnapInteraction.js';
import { ModifyInteraction } from './interactions/ModifyInteraction.js';

import { GeometrySource } from './sources/GeometrySource.js';
import { IFCSource } from './sources/IFCSource.js';
import { PointcloudSource } from './sources/PointcloudSource.js';
import { Image360Source } from './sources/Image360Source.js';
import { NexusSource } from './sources/NexusSource.js';
import { PotreeMeasurementSource } from './sources/PotreeMeasurementSource.js';
import { OrientedImageSource } from './sources/OrientedImageSource.js';
import { TerrainSource } from './sources/TerrainSource.js';
import { PlySource } from './sources/PlySource.js';
import { WMSTerrainSource } from './sources/WMSTerrainSource.js';
import { OverlaySource } from './sources/OverlaySource.js';
import { ObjSource } from './sources/ObjSource.js';

import { Point } from './geometries/Point.js';
import { HeightPoint } from './geometries/HeightPoint.js';
import { Line } from './geometries/Line.js';
import { Polygon } from './geometries/Polygon.js';

import { PotreeMeasurement } from './potree_measurements/PotreeMeasurement.js';
import { PotreeBoxVolume } from './potree_measurements/PotreeBoxVolume.js';
import { PotreeHeightProfile } from './potree_measurements/PotreeHeightProfile.js';

import { Cursor3D } from './utilities/Cursor3D.js';

import { Overlay } from './overlay/Overlay.js';

import * as THREE from '../libs/threejs/three.module.js';
import { OverlayLayer } from './layers/OverlayLayer.js';

/**
 * The root class of the library that contains references to every other class. Sets itself as a global property of the "window" object.
 * @category Sigma3D
 * @example
 *
 * await Sigma3D.initialize();
 * Sigma3D.setMap(map);
 *
 * @class
 */

class Sigma3D extends EventDispatcher {

    constructor() {
        super();

        /**
         * @property {Sigma3D.Map} CurrentMap - Contains the current map being displayed by the library, if one is set.
         */
        this.CurrentMap = undefined;

        this.View = View;
        this.Map = Map;

        // Layers
        this.PointcloudLayer = PointcloudLayer;
        this.IFCLayer = IFCLayer;
        this.GeometryLayer = GeometryLayer;
        this.Image360Layer = Image360Layer;
        this.NexusLayer = NexusLayer;
        this.PotreeMeasurementLayer = PotreeMeasurementLayer;
        this.OrientedImageLayer = OrientedImageLayer;
        this.TerrainLayer = TerrainLayer;
        this.OverlayLayer = OverlayLayer;
        this.ObjLayer = ObjLayer;
        this.PlyLayer = PlyLayer;

        // interactions
        this.SelectInteraction = SelectInteraction;
        this.DrawInteraction = DrawInteraction;
        this.SnapInteraction = SnapInteraction;
        this.ModifyInteraction = ModifyInteraction;

        // sources
        this.GeometrySource = GeometrySource;
        this.IFCSource = IFCSource;
        this.PointcloudSource = PointcloudSource;
        this.Image360Source = Image360Source;
        this.NexusSource = NexusSource;
        this.PotreeMeasurementSource = PotreeMeasurementSource;
        this.OrientedImageSource = OrientedImageSource;
        this.TerrainSource = TerrainSource;
        this.PlySource = PlySource;
        this.WMSTerrainSource = WMSTerrainSource;
        this.OverlaySource = OverlaySource;
        this.ObjSource = ObjSource;

        // utilities
        this.Utilities = Utilities;

        this.Geometries = Geometries;
        this.Materials = Materials;

        // geometries
        this.Point = Point;
        this.HeightPoint = HeightPoint;
        this.Line = Line;
        this.Polygon = Polygon;
        this.Symbol = Symbol;

        // potree measurement
        this.PotreeMeasurement = PotreeMeasurement;
        this.PotreeBoxVolume = PotreeBoxVolume;
        this.PotreeHeightProfile = PotreeHeightProfile;

        // controls
        this.DeviceOrientationControls = Potree.DeviceOrientationControls;
        this.EarthControls = Potree.EarthControls;
        this.VRControls = Potree.VRControls;
        this.OrbitControls = Potree.OrbitControls;
        this.noCloudControls = Utilities.noCloudControls;

        // 3d cursor
        this.Cursor3D = Cursor3D;

        // overlay
        this.Overlay = Overlay;

        // focused image
        this.TestingDepth = true;
    }

    /**
     * Initializes the utilities used by the class.
     * @private
     * @function
     */
    async initialize() {
        await this.Utilities.initialize();

        Utilities.PotreeViewer.addEventListener('oriented_image_focused', (e) => {
            this.dispatchEvent(new CustomEvent('oriented-image-focused', { detail: e.detail }));
            this.disableDepthTestingOnGeometries(true);

            for (let imageSet of Utilities.PotreeViewer.scene.orientedImages) imageSet.images.forEach((image) => image.focused = false);

            e.detail.image.focused = true;
            e.detail.image.line.visible = true;
            e.detail.image.mesh.visible = true;
            e.detail.image.clickable = true;
        });

        Utilities.PotreeViewer.addEventListener('oriented_image_unfocused', (e) => {
            this.dispatchEvent(new CustomEvent('oriented-image-unfocused', { detail: e.detail }));
            if (this.TestingDepth) this.enableDepthTestingOnGeometries();

            for (let imageSet of Utilities.PotreeViewer.scene.orientedImages) imageSet.images.forEach((image) => image.focused = false);
        });

        Utilities.PotreeViewer.addEventListener('360image_focused', (e) => {
            for (let source of this.CurrentMap.Sources) {
                if (source.Type == "Image360Source") {
                    for (let imageSet of source.ImageSets) {
                        imageSet.selectingEnabled = false;

                        for (let image of imageSet.images) {
                            if (image !== e.detail.image) {
                                image.mesh.visible = false;
                            }
                        }
                    }
                }
            }

            this.dispatchEvent(new CustomEvent('360-image-focused'));
        });

        Utilities.PotreeViewer.addEventListener('360image_unfocused', (e) => {
            for (let source of this.CurrentMap.Sources) {
                if (source.Type == "Image360Source") {
                    for (let imageSet of source.ImageSets) {
                        imageSet.selectingEnabled = true;
                        for (let image of imageSet.images) image.mesh.visible = true;
                    }
                }
            }

            this.dispatchEvent(new CustomEvent('360-image-unfocused'));
        });
    }

    /**
     * Sets the CurrentMap property and initializes whatever map was passed to the function.
     * @param {Sigma3D.Map} newMap - An instance of Sigma3D.Map
     * @function
     */
    async setMap(newMap) {
        this.CurrentMap = newMap;
        await this.CurrentMap.initialize();
    }

    async setWasmPath(path) {
        await Utilities.IFCLoader.ifcManager.setWasmPath(path);
    }

    focusOrientedImage(image) {
        for (let imageSet of Utilities.PotreeViewer.scene.orientedImages) imageSet.images.forEach((image) => image.focused = false);
        Utilities.Potree.debug.moveToImage(image);

        image.focused = true;
        image.line.visible = true;
        image.mesh.visible = true;
        image.clickable = true;
    }

    findOrientedImage(coords, dir, angleFilter) {
        let images = [];

        for (let source of this.CurrentMap.Sources) {
            if (source.Type == "OrientedImageSource") {
                let image = source.getImageAt(coords, dir);
                if (image != undefined) images.push(image);
            }
        }

        let min = Infinity;
        let minImage = images[0];
        let cur = new THREE.Vector3(...Utilities.Cursor3D.Position);

        for (let image of images) {
            if (image.position.distanceTo(cur) < min) {
                min = image.position.distanceTo(cur);
                minImage = image;
            }
        }

        return minImage;
    }

    rotateOrientedImage(x, y) {
        if (this.Utilities.PotreeViewer.orientedImageControls) {
            const fovY = this.Utilities.PotreeViewer.getFOV();
            const top = Math.tan((fovY / 2) * (Math.PI / 180));
            this.Utilities.PotreeViewer.orientedImageControls.shear[1] += y * top;
            this.Utilities.PotreeViewer.orientedImageControls.shear[0] += x * top;
        }
    }

    /**
     * Centers the current view relative to the bounding boxes of the current maps sources.
     * @function
     */
    centerView(layers = undefined) {
        let bbox = new THREE.Box3();
        let updated = false;

        if (layers) {
            for (let layer of layers) {
                bbox.union(layer.Source.getBbox());
                updated = true;
            }
        }
        else if (this.CurrentMap.Layers) {
            for (let layer of this.CurrentMap.Layers) {
                bbox.union(layer.Source.getBbox());
                updated = true;
            }
        }

        if (updated) this.CurrentMap.CurrentView.center(bbox);
    }

    /**
     * Enables depth testing (the geometries can be obscured by other objects in the scene).
     * @private
     * @function
     */
    enableDepthTestingOnGeometries() {
        this.TestingDepth = true;
        this.CurrentMap.enableDepthTestingOnGeometries();
    }

    /**
     * Disables depth testing (the geometries always appears to be at the front).
     * @private
     * @function
     */
    disableDepthTestingOnGeometries(internal) {
        if (!internal) this.TestingDepth = false;
        this.CurrentMap.disableDepthTestingOnGeometries();
    }

    /**
     * Opens a window for analyzing and exporting a PotreeHeightProfile, belonging to a PotreeMeasurementSource.
     * @function
     */
    openHeightProfileWindow(color = 'black') {
        document.getElementById('profileCanvasContainer').style.backgroundColor = color;
        Utilities.PotreeViewer.profileWindow.show();
    }

    /**
     * Closes a window for analyzing and exporting a PotreeHeightProfile, belonging to a PotreeMeasurementSource.
     * @function
     */
    closeHeightProfileWindow() {
        Utilities.PotreeViewer.profileWindow.hide();
    }

    /**
     * Sets the PotreeHeightProfile currently being edited.
     * @param {PotreeHeightProfile} profile - The PotreeHeightProfile currently being edited.
     * @function
     */
    setEditedProfile(profile) {
        Utilities.PotreeViewer.profileWindowController.setProfile(profile);
    }

    /**
     * Sets the settings used by the viewer for displaying PointcloudSources. Settings apply to all pointclouds.
     * @param {Object} settings - Described below.
     *
     * @param {Object} settings.edl - Configures settings of the EDL shader.
     * @param {boolean} settings.edl.enabled - Determines whether or not the shader should be enabled.
     * @param {number} settings.edl.opacity - Determines how visible the shader is.
     * @param {number} settings.edl.radius - Determines the thickness of the shader.
     * @param {number} settings.edl.strength - Determines how dark the shader should be.
     *
     *
     * @param {number} settings.FOV - Configures viewer FOV.
     *
     * @param {number} settings.pointBudget - Determines how many points should be displayed at a given time (has a strong impact on performance).
     * @param {string} settings.background - Determines the background used by the viewer ("black", "white", "skybox"...).
     *
     * @function
     */
    setPointcloudDisplaySettings(settings) {
        if (settings.edl) {
            if (settings.edl.enabled !== undefined) this.Utilities.PotreeViewer.setEDLEnabled(settings.edl.enabled);
            if (settings.edl.opacity) this.Utilities.PotreeViewer.setEDLOpacity(settings.edl.opacity);
            if (settings.edl.radius) this.Utilities.PotreeViewer.setEDLRadius(settings.edl.radius);
            if (settings.edl.strength) this.Utilities.PotreeViewer.setEDLStrength(settings.edl.strength);
        }

        if (settings.FOV) this.Utilities.PotreeViewer.setFOV(settings.FOV);
        if (settings.pointBudget) this.Utilities.PotreeViewer.setPointBudget(settings.pointBudget);
        if (settings.background) this.Utilities.PotreeViewer.setBackground(settings.background);
    }

    /**
     * Gets a list of classifications (point categories) used by the pointclouds, loaded into the viewer. (e.g. "ground", "walls", etc.)
     * @returns {Object} Sigma3D.Utilities.PotreeViewer.classifications - An object whose properties are the indexes of classifications, which contain the associated data. (e.g. { "0": { "name":"ground" } }... )
     * @function
     */
    getPointcloudClassifications() {
        return this.Utilities.PotreeViewer.classifications;
    }

    /**
     * Sets the visibility of a pointcloud classification by name.
     * @param {string} classificationName - The name of the classification as returned by the getPointcloudClassifications() method.
     * @param {boolean} visibility - What to set the visibility of the classification to.
     * @function
     */
    setPointcloudClassificationVisibility(classificationName, visibility) {
        let classifications = this.getPointcloudClassifications();

        for (let classificationIndex in classifications) {
            if (classifications[classificationIndex].name == classificationName) {
                this.Utilities.PotreeViewer.setClassificationVisibility(classificationIndex, visibility);
            }
        }
    }

    addPointcloudClassification(id, name) {
        let classifications = Utilities.PotreeViewer.classifications;
        classifications[id] = { visible: true, name: name, color: [Math.random(), Math.random(), Math.random(), Math.random()] },
            Utilities.PotreeViewer.setClassifications(classifications);
    }
}

export { Sigma3D };