const path = require('path');

module.exports = {
  entry: './src/Sigma3D.js',
  output: {
    filename: 'Sigma3D.js',
    path: path.resolve(__dirname, 'build'),
    library: 'Sigma3D',
    libraryTarget: 'window',
    publicPath: '',
    umdNamedDefine: true 
  },
  module: {
    rules: [
      {
        test: /\.png$/,
        type: 'asset/resource'
     }
    ],
  }
};