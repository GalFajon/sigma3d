/* LAYER ADDITION WIDGET */
const IfcLayerAdditionButton = document.getElementById('IFC-submit');
const NexusLayerAdditionButton = document.getElementById('Nexus-submit')
const PointcloudLayerAdditionButton = document.getElementById('Pointcloud-submit')
const Image360LayerAdditionButton = document.getElementById('Image360-submit')
const PotreeMeasurementLayerAdditionButton = document.getElementById('PotreeMeasurement-submit');
const GeometryLayerAdditionButton = document.getElementById('Geometry-submit')

IfcLayerAdditionButton.addEventListener('click', async (e) => {
    const input = document.getElementById('IFC-url');
    const ifcSource = new Sigma3D.IFCSource({ urls: [input.value] });
    await map.addLayer(new Sigma3D.IFCLayer({source: ifcSource}));

    updateLayerDisplay()
});

NexusLayerAdditionButton.addEventListener('click', async (e) => {
    const input = document.getElementById('Nexus-url');
    const nexusSource = new Sigma3D.NexusSource({ urls: [input.value] });
    await map.addLayer(new Sigma3D.NexusLayer({source: nexusSource}));

    updateLayerDisplay()
});

PointcloudLayerAdditionButton.addEventListener('click', async (e) => {
    const input = document.getElementById('Pointcloud-url');
    const pointcloudSource = new Sigma3D.PointcloudSource({ urls: [input.value] });
    await map.addLayer(new Sigma3D.PointcloudLayer({source: pointcloudSource}));

    updateLayerDisplay()
});

Image360LayerAdditionButton.addEventListener('click', async (e) => {
    const input = document.getElementById('Image360-url');
    const image360Source = new Sigma3D.Image360Source({ urls: [input.value] });
    await map.addLayer(new Sigma3D.Image360Layer({source: image360Source}));

    updateLayerDisplay()
});

PotreeMeasurementLayerAdditionButton.addEventListener('click', async (e) => {
    const PotreeMeasurementSource = new Sigma3D.PotreeMeasurementSource();
    await map.addLayer(new Sigma3D.PotreeMeasurementLayer({source: PotreeMeasurementSource}));

    updateLayerDisplay()
});

GeometryLayerAdditionButton.addEventListener('click', async (e) => {
    const GeometrySource = new Sigma3D.GeometrySource();
    await map.addLayer(new Sigma3D.GeometryLayer({source: GeometrySource}));

    updateLayerDisplay()
});

/* LAYER DISPLAY */
let layerDisplay = document.getElementById('layerDisplay');

function updateLayerDisplay() {
    while (layerDisplay.firstChild) {
        layerDisplay.removeChild(layerDisplay.firstChild);
    }

    for (let layer of map.Layers) {
        let item = document.createElement('li');
        let index = map.Layers.indexOf(layer);
        item.classList = 'list-group-item';
        item.innerText = '#'+index+' '+layer.Type;
        item.value = index;
        layerDisplay.appendChild(item);
    }
}

function generateDrawButton() {}
function generateModifyButton() {}
function generateSnapButton() {}
function generateSelectButton() {}